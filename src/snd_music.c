#include "quakedef.h"

#ifdef HAVE_MPG123
/*
 * mp3 support code block
 * ===========================================================================
 * MP3 Audio Support
 * From Robert 'Heffo' Heffernan
 * ===========================================================================
 */

#  define TEMP_BUFFER_SIZE 0x400000    //1mb temp buffer for MP3

/*
 * MP3 New - Allocate & initialise a new mp3_t structure 
 */
mp3_t        *MP3_New (char *filename) {
    mp3_t        *mp3;
    byte          stackbuf[1 * 1024];  // avoid dirtying the cache heap

    mp3 = Z_Malloc (sizeof (mp3_t));
    if (!mp3) {
        Con_Printf ("Couln't allocate memory\n");
        return NULL;
    }

    /*
     * Load the MP3 file via the filesystem for compatibility
     * * set mp3->mp3data pointer memory location of loaded file
     * * set mp3->mp3size value to size of file 
     */
    //    mp3->mp3size = FS_LoadFile(filename, (void **)&mp3->mp3data);
    mp3->mp3data = Com_LoadStackFile (filename, stackbuf, sizeof (stackbuf));
    if (!mp3->mp3data) {
        Con_Warnf ("Couldn't load %s\n", filename);
        Z_Free (mp3);
        return NULL;
    }

    return mp3;
}

/*
 * MP3_Free - Release an mp3_t structure, and free all memory used by it 
 */
void MP3_Free (mp3_t * mp3) {
    Con_Printf ("freeing mp3 struct\n");

    if (!mp3) {
        Con_DPrintf ("null mp3 struct\n");
        return;
    }
    //    if (mp3->mp3data)                  /* mp3 still loaded */
    //        Q_free (mp3->mp3data);
    //        FS_FreeFile(mp3->mp3data); /* free mp3 */
    if (mp3->rawdata)                  /* raw audio left */
        Q_free (mp3->rawdata);
    Z_Free (mp3);
}

/*
 * ExtendRawBuffer - Allocates & extends a buffer for the raw decompressed audio data 
 */
qboolean ExtendRawBuffer (mp3_t * mp3, int size) {
    byte         *newbuf;
    Con_Printf ("extend raw buffer, size=%d\n", size);

    if (!mp3) {
        return false;
    }

    if (!mp3->rawdata) {
        mp3->rawdata = Q_malloc (size);
        if (!mp3->rawdata) {
            return false;
        }
        mp3->rawsize = size;
        return true;
    }

    newbuf = Q_malloc (mp3->rawsize + size);
    if (!newbuf) {
        Con_Printf ("Not enough memory\n");
        return false;
    }

    memcpy (newbuf, mp3->rawdata, mp3->rawsize);
    mp3->rawsize += size;
    Q_free (mp3->rawdata);
    mp3->rawdata = newbuf;

    return true;
}

qboolean MP3_Process (mp3_t * mp3) {
    mpg123_handle *mh;
    byte          sbuff[8192];
    byte         *tbuff, *tb = NULL;
    int           ret;
    size_t        size, tbuffused = 0;

    int           i = 0;
    Con_Printf ("processing mp3\n");

    if (!mp3)
        return false;
    if (!mp3->mp3data)
        return false;

    // allocate for large temp decode buffer 
    tbuff = Q_malloc (TEMP_BUFFER_SIZE);
    if (!tbuff)
        return false;

    // Initialize MP3 Decoder 
    mpg123_init ();
    mh = mpg123_new (NULL, &ret);
    if (!mh) {
        Con_Printf ("Failed to initialize mpg123 library\n");
        return false;
    }

    mpg123_param (mh, MPG123_RESYNC_LIMIT, -1, 0);
    mpg123_param (mh, MPG123_VERBOSE, developer.value, 0);
    mpg123_open_feed (mh);

    while (ret != MPG123_NEW_FORMAT) {
        // Decode the 1st frame of MP3 data, store in buffer 
        ret = mpg123_decode (mh, mp3->mp3data, TEMP_BUFFER_SIZE, tbuff, TEMP_BUFFER_SIZE, &size);
        Con_Printf ("first while i=%i, size=%i\n", i++, size);

        if (ret == MPG123_NEW_FORMAT) {
            // Copy the MP3s format details (samplerate # chans) 
            mpg123_getformat (mh, (long *) &mp3->samplerate, (int *) &mp3->channels, &mp3->encoding);
            Con_DPrintf ("rate:%d, channels:%d, encoding:%d\n", mp3->samplerate, mp3->channels, mp3->encoding);
        }

        /*
         * Set a pointer to the start of the temporary buffer
         * and then offset it by the number of bytes decoded.
         * Also set the amount of the temp buffer that has been used. 
         */
        tb = tbuff + size;
        tbuffused = size;
    }

    /*
     * ret = mpg123_format (mh, mp3->samplerate, MPG123_MONO, mp3->encoding);
     * if (ret == MPG123_ERR)
     * Con_DPrintf("Failed to force mono channel\n");
     * ret = ~MPG123_ERR;
     */

    // Start a loop that ends when the MP3 decoder fails 
    while (ret != MPG123_ERR && ret != MPG123_NEED_MORE) {

        // Decode the next frame into smaller temp 
        ret = mpg123_decode (mh, NULL, 0, sbuff, 8192, &size);
        //        Con_Printf ("second while i=%i, size=%i\n", i++, size);

        if (tbuffused + size >= TEMP_BUFFER_SIZE) {
            if (!ExtendRawBuffer (mp3, tbuffused)) {
                // error shutdown MP3 decoder 
                Con_Printf ("bug i=%i\n", i++);
                Q_free (tbuff);
                return false;
            }

            memcpy (mp3->rawdata + mp3->rawpos, tbuff, tbuffused);
            tbuffused = 0;
            tb = tbuff;
            mp3->rawpos = mp3->rawsize;
        }

        memcpy (tb, sbuff, size);
        tb += size;
        tbuffused += size;
    }

    // done decoding, flush buffer into sample buffer 
    if (!ExtendRawBuffer (mp3, tbuffused)) {
        Con_DPrintf ("Failed to extend sample buffer\n");
        Q_free (tbuff);
        return false;
    }
    // CPY lrg temp buff into extended sample buff 
    memcpy (mp3->rawdata + mp3->rawpos, tbuff, tbuffused);

    // calculate samples based on size of decompressed MP3 / 2 (16b) 
    mp3->samples = (mp3->rawsize / 2) / mp3->channels;

    Q_free (tbuff);
    Con_DPrintf ("sample=%i\n", mp3->samples);
    return true;
}

/*
 * S_LoadMP3Sound - A modified version of S_LoadSound but with MP3 support 
 */
sfxcache_t   *S_LoadMP3Sound (sfx_t * s) {
    char          namebuffer[MAX_QPATH];
    mp3_t        *mp3;
    float         stepscale;
    sfxcache_t   *sc;
    char         *name;

    name = s->name;
    if (name[0] == '#') {
        strcpy (namebuffer, &name[1]);
    } else {
        Q_snprintfz (namebuffer, sizeof (namebuffer), "music/%s", name);
        Com_DefaultExtension (namebuffer, ".mp3");
    }

    mp3 = MP3_New (namebuffer);
    if (!mp3) {
        Con_Warnf ("Couldn't load %s\n", namebuffer);
        return NULL;
    }

    if (!MP3_Process (mp3)) {
        Con_Printf ("Error while processing %s\n", namebuffer);
        MP3_Free (mp3);
        return NULL;
    }

    /*
     * if (mp3->channels == 2) {
     * Con_Printf ("%s is a stero sample, not supported\n", s->name);
     * MP3_Free (mp3);
     * return NULL;
     * }
     */

    // Align samplerate of MP3 and Quake 
    stepscale = (float) mp3->samplerate / shm->speed;

    /*
     * Allocate a piece of zone memory to store the decompressed
     * * & scaled MP3 aswell as it's attatched sfxcache_t structure 
     */
    //    sc = s->cache = Z_Malloc ((mp3->rawsize /stepscale) + sizeof(sfxcache_t));
    sc = Cache_Alloc (&s->cache, (mp3->rawsize / stepscale) + sizeof (sfxcache_t), s->name);

    if (!sc) {
        // Couldn't allocate the zone 
        Con_DPrintf ("Memory allocation error\n");
        MP3_Free (mp3);
        return NULL;
    }
    // Copy a few details of MP3 into the sfxcache_t structure 
    sc->length = mp3->samples;
    sc->loopstart = -1;
    sc->speed = mp3->samplerate;
    sc->width = 2;
    sc->stereo = 1;                    //mp3->channels;

    // Resample the decompressed MP3 to match Dominion's samplerate 
    ResampleSfx (s, sc->speed, sc->width, mp3->rawdata);

    // Free the mp3_t structure as it's no longer needed
    MP3_Free (mp3);

    //Return sfxcache_t structure for the decoded & processed MP3 
    return sc;
}
#endif                                 /* HAVE_MPG123 */
