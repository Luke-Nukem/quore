/*
 * Copyright (C) 2002-2003 A Nourai
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation; either version 2 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.
 *
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 */
// vid_common_gl.c -- Common code for vid_wgl.c and vid_glx.c

#include "quakedef.h"

const char   *gl_vendor;
const char   *gl_renderer;
const char   *gl_version;
const char   *gl_extensions;

float         gldepthmin, gldepthmax;
qboolean      gl_allow_ztrick = true;
qboolean      gl_nopolysmooth = false;
qboolean      gl_softwarerenderer = false;

int           vid_highhunkmark;

float         vid_gamma = 1.0;
byte          vid_gamma_table[256];

unsigned      d_8to24table[256];

byte          color_white[4] = { 255, 255, 255, 0 };
byte          color_black[4] = { 0, 0, 0, 0 };

cvar_t        vid_vsync = { "vid_vsync", "0", CVAR_NONE, OnChange_vid_vsync };

unsigned int  pboIds[MAX_PBO];
unsigned int  vboIds[MAX_VBO];

int           gl_samplebuffers = 1;
int           gl_samples = 2;
int           gl_textureunits = 1;

static extension_t extensionlist[] = {
    {"GL_ARB_multitexture", &gl_ext.multitex},
    {"GL_ARB_multisample", &gl_ext.multisample},
    {"GL_ARB_pixel_buffer_object", &gl_ext.pixel_bufferobject},
    {"GL_ARB_texture_compression", &gl_ext.tex_compression},
    {"GL_ARB_texture_env_add", &gl_ext.tex_envadd},
    {"GL_ARB_texture_non_power_of_two", &gl_ext.tex_nonpoweroftwo},
    {"GL_ARB_vertex_buffer_object", &gl_ext.vertex_bufferobject},
    {"GL_EXT_blend_color", &gl_ext.blend_color},
    {"GL_EXT_blend_minmax", &gl_ext.blend_minmax},
    {"GL_EXT_blend_subtract", &gl_ext.blend_substract},
    {"GL_EXT_framebuffer_object", &gl_ext.frame_bufferobject},
    {"GL_EXT_texture_env_combine", &gl_ext.tex_envcombine},
    {"GL_EXT_texture_filter_anisotropic", &gl_ext.tex_filteranisotropic},
    {"GL_SGIS_generate_mipmap", &gl_ext.generate_mipmap}
};

#define NUM_EXTENSIONS (sizeof(extensionlist) / sizeof(extension_t))

qboolean CheckExtension (const char *extension) {
    char         *where, *terminator;
    const char   *start;

    if (!gl_extensions && !(gl_extensions = (char *) glGetString (GL_EXTENSIONS)))      // lxndr: cast added
        return false;

    if (!extension || *extension == 0 || strchr (extension, ' '))
        return false;

    for (start = gl_extensions; (where = strstr (start, extension)); start = terminator) {
        terminator = where + strlen (extension);
        if ((where == start || *(where - 1) == ' ') && (*terminator == 0 || *terminator == ' '))
            return true;
    }

    return false;
}

void InitExtensions (void) {
    int           i;

    Con_DPrintf ("Extensions found:\n");
    for (i = 0; i < NUM_EXTENSIONS; i++) {
        if (CheckExtension (extensionlist[i].extension)) {
            Con_DPrintf ("  %s\n", extensionlist[i].extension);
            *extensionlist[i].found = true;
        } else {
            *extensionlist[i].found = false;
        }
    }

    qglActiveTexture = (void *) qglGetProcAddress ("glActiveTextureARB");
    qglBindBuffer = (void *) qglGetProcAddress ("glBindBufferARB");
    qglBlendEquation = qglGetProcAddress ("glBlendEquationEXT");
    qglBlendColor = qglGetProcAddress ("glBlendColorEXT");
    qglBufferData = (void *) qglGetProcAddress ("glBufferDataARB");
    qglBufferSubData = (void *) qglGetProcAddress ("glBufferSubDataARB");
    qglClientActiveTexture = (void *) qglGetProcAddress ("glClientActiveTextureARB");
    qglDeleteBuffers = (void *) qglGetProcAddress ("glDeleteBuffersARB");
    qglGenBuffers = (void *) qglGetProcAddress ("glGenBuffersARB");
    qglGetBufferParameteriv = (void *) qglGetProcAddress ("glGetBufferParameterivARB");
    qglMapBuffer = (void *) qglGetProcAddress ("glMapBufferARB");
    qglMultiTexCoord2f = (void *) qglGetProcAddress ("glMultiTexCoord2fARB");
    qglMultiDrawArrays = (void *) qglGetProcAddress ("glMultiDrawArraysEXT");
    qglUnmapBuffer = (void *) qglGetProcAddress ("glUnmapBufferARB");

    if (gl_ext.blend_minmax && gl_ext.blend_substract)
        if (!qglBlendEquation)
            gl_ext.blend_minmax = gl_ext.blend_substract = false;
    if (gl_ext.blend_color)
        if (!qglBlendColor)
            gl_ext.blend_color = false;
    if (gl_ext.frame_bufferobject)
        if (!qglGenerateMipmap)
            gl_ext.frame_bufferobject = false;
    if (gl_ext.multisample) {
        glGetIntegerv (GL_SAMPLE_BUFFERS_ARB, &gl_samplebuffers);
        glGetIntegerv (GL_SAMPLES_ARB, &gl_samples);
        if (gl_samples > 1) {
            Con_DPrintf ("  Enabled %i samples on hardware\n", gl_samples);
            glEnable (GL_MULTISAMPLE_ARB);
        }
    }
    if (gl_ext.multitex) {
        if (Com_CheckParm ("-nomtex") || !qglMultiTexCoord2f || !qglActiveTexture || strstr (gl_renderer, "Savage")) {
            gl_ext.multitex = false;
            gl_textureunits = 1;
        }

        glGetIntegerv (GL_MAX_TEXTURE_UNITS_ARB, &gl_textureunits);
        gl_textureunits = min (gl_textureunits, 4);

        if (Com_CheckParm ("-maxtmu2") || !strcmp (gl_vendor, "ATI Technologies Inc."))
            gl_textureunits = min (gl_textureunits, 2);

        if (gl_textureunits < 2) {
            gl_ext.multitex = false;
            gl_textureunits = 1;
        } else {
            Con_DPrintf ("Enabled %i texture units on hardware\n", gl_textureunits);
        }
    }
    if (gl_ext.pixel_bufferobject) {
        if (qglBindBuffer && qglBufferData && qglDeleteBuffers && qglGenBuffers && qglMapBuffer && qglUnmapBuffer) {
            qglGenBuffers (MAX_PBO, pboIds);
            for (i = 0; i < MAX_PBO; i++) {
                if (i < 2) {
                    qglBindBuffer (GL_PIXEL_PACK_BUFFER_ARB, pboIds[i]);
                    qglBufferData (GL_PIXEL_PACK_BUFFER_ARB, vid.width * vid.height * 3, 0, GL_STREAM_READ_ARB);
                    qglBindBuffer (GL_PIXEL_PACK_BUFFER_ARB, 0);
                } else {
                    qglBindBuffer (GL_PIXEL_UNPACK_BUFFER_ARB, pboIds[i]);
                    qglBufferData (GL_PIXEL_UNPACK_BUFFER_ARB, 1000000, 0, GL_DYNAMIC_DRAW_ARB);
                    qglBindBuffer (GL_PIXEL_UNPACK_BUFFER_ARB, 0);
                }
            }
        } else {
            gl_ext.pixel_bufferobject = false;
        }
    }
    if (gl_ext.vertex_bufferobject) {
        if (qglClientActiveTexture && qglBindBuffer && qglBufferData && qglBufferSubData && qglDeleteBuffers && qglGenBuffers && qglMapBuffer
            && qglMultiDrawArrays && qglUnmapBuffer) {
            qglGenBuffers (MAX_VBO, vboIds);
            for (i = 0; i < MAX_VBO; i++) {
                qglBindBuffer (GL_ARRAY_BUFFER_ARB, vboIds[i]);
                qglBufferData (GL_ARRAY_BUFFER_ARB, MAX_MAP_VERTS * 4, 0, GL_STREAM_DRAW_ARB);
                qglBindBuffer (GL_ARRAY_BUFFER_ARB, 0);
            }
        } else {
            gl_ext.vertex_bufferobject = false;
        }
    }
}

void GL_Info_f (void) {
    int           i;

    Con_Printf ("Extensions found:\n");
    for (i = 0; i < NUM_EXTENSIONS; i++)
        if (CheckExtension (extensionlist[i].extension))
            Con_Printf ("  %s\n", extensionlist[i].extension);
}

/*
 * GL_Init
 */
void GL_Init (void) {
    gl_vendor = (char *) glGetString (GL_VENDOR);
    Con_Printf ("GL_VENDOR: %s\n", gl_vendor);
    gl_renderer = (char *) glGetString (GL_RENDERER);
    Con_Printf ("GL_RENDERER: %s\n", gl_renderer);
    gl_version = (char *) glGetString (GL_VERSION);
    Con_Printf ("GL_VERSION: %s\n", gl_version);
    gl_extensions = (char *) glGetString (GL_EXTENSIONS);
    if (Com_CheckParm ("-gl_ext"))
        Con_Printf ("GL_EXTENSIONS: %s\n", gl_extensions);

    Cmd_AddCommand ("glinfo", GL_Info_f);

// lxndr: FIXME
/*    if (!strstr (gl_renderer, "Mesa")) {
        Con_Warnf ("Software renderer detected\n");
        gl_softwarerenderer = true;
    }
    if (!Q_strcasecmp (gl_renderer, "GeForce FX 5200/AGP/SSE2")) // lxndr: for GL_POLYGON_SMOOTH
*/ gl_nopolysmooth = true;

    InitExtensions ();

    glAlphaFunc (GL_GREATER, 0);
    glBlendFunc (GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    glCullFace (GL_FRONT);
    glFrontFace (GL_CCW);
    glPolygonOffset (0.05, 25.0);      // lxndr: zfighting fix from ezquake
    glPolygonMode (GL_FRONT_AND_BACK, GL_FILL);
    glShadeModel (GL_FLAT);

    glTexEnvf (GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_REPLACE);
    glTexParameterf (GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
    glTexParameterf (GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameterf (GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
    glTexParameterf (GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);

    glHint (GL_FOG_HINT, GL_NICEST);
    if (gl_ext.generate_mipmap) {
        glHint (GL_GENERATE_MIPMAP_HINT_SGIS, GL_NICEST);
//        glTexParameteri (GL_TEXTURE_2D, GL_TEXTURE_MAX_LEVEL_SGIS, 100);
    }
}

void GL_Shutdown (void) {
    R_ClearLevels ();
    R_ClearDecals ();

    if (gl_ext.pixel_bufferobject)
        qglDeleteBuffers (MAX_PBO, pboIds);
    if (gl_ext.vertex_bufferobject)
        qglDeleteBuffers (MAX_VBO, vboIds);
}

void Check_Gamma (unsigned char *pal) {
    int           i;
    float         inf;
    unsigned char palette[768];

#ifdef DEBUG
    Con_Printf ("Check_Gamma: vid_gamma=%f\n", vid_gamma);
#endif
    if ((i = Com_CheckParm ("-gamma")) && Com_IsParmData (i + 1))
        vid_gamma = bound (0.2, Q_atof (com_argv[i + 1]), 2);   // lxndr: was 1
    else
        vid_gamma = 1;

    //    Cvar_SetDefault (&v_gamma, vid_gamma); // lxndr: no thanks

    if (vid_gamma != 1) {
        for (i = 0; i < 256; i++) {
            inf = min (255 * pow ((i + 0.5) / 255.5, vid_gamma) + 0.5, 255);
            vid_gamma_table[i] = inf;
        }
    } else {
        for (i = 0; i < 256; i++)
            vid_gamma_table[i] = i;
    }

    for (i = 0; i < 768; i++)
        palette[i] = vid_gamma_table[pal[i]];

    memcpy (pal, palette, sizeof (palette));
}

void VID_SetPalette (unsigned char *palette) {
    int           i;
    byte         *pal;
    unsigned      r, g, b, *table;

    // 8 8 8 encoding
    pal = palette;
    table = d_8to24table;
    for (i = 0; i < 256; i++) {
        r = pal[0];
        g = pal[1];
        b = pal[2];
        pal += 3;
        *table++ = (255 << 24) + (r << 0) + (g << 8) + (b << 16);
    }
    d_8to24table[255] = 0;             // 255 is transparent
}
