/*
 * Copyright (C) 1996-1997 Id Software, Inc.
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation; either version 2 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.
 *
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 */
// view.c -- player eye positioning

#include "quakedef.h"
#include <time.h>                      // cl_clock

/*
 *
 * The view is allowed to move slightly from it's true position for bobbing,
 * but if it exceeds 8 pixels linear distance (spherical, not box), the list of
 * entities sent from the server may not include everything in the pvs,
 * especially when crossing a water boudnary.
 *
 */

#ifndef GLQUAKE
cvar_t        lcd_x = { "lcd_x", "0" };
cvar_t        lcd_yaw = { "lcd_yaw", "0" };
#endif

cvar_t        cl_rollangle = { "cl_rollangle", "5.0" };

cvar_t        v_bob = { "v_bob", "0.02" };

cvar_t        v_bobcycle = { "v_bobcycle", "0.6" };
cvar_t        v_bobcyclefine = { "v_bobcyclefine", "0.05" };
cvar_t        v_bobcyclehealth = { "v_bobcyclehealth", "0.5" };

cvar_t        v_bobpitch = { "v_bobpitch", "0.5" };
cvar_t        v_bobroll = { "v_bobroll", "0.5" };
cvar_t        v_bobyaw = { "v_bobyaw", "0.5" };
cvar_t        v_bobz = { "v_bobz", "0.25" };
cvar_t        v_bobpitchshift = { "v_bobpitchshift", "1" };
cvar_t        v_bobrollshift = { "v_bobrollshift", "1" };
cvar_t        v_bobyawshift = { "v_bobyawshift", "0.5" };

cvar_t        v_bobslowscale = { "v_bobslowscale", "0.25" };
cvar_t        v_bobslowthreshold = { "v_bobslowthreshold", "125" };

cvar_t        v_gunbob = { "v_gunbob", "1" };

cvar_t        v_gunbobpitch = { "v_gunbobpitch", "1" };
cvar_t        v_gunbobroll = { "v_gunbobroll", "0.25" };
cvar_t        v_gunbobyaw = { "v_gunbobyaw", "1" };
cvar_t        v_gunbobx = { "v_gunbobx", "1" };
cvar_t        v_gunboby = { "v_gunboby", "1." };
cvar_t        v_gunbobz = { "v_gunbobz", "0.25" };
cvar_t        v_gunbobpitchshift = { "v_gunbobpitchshift", "0.25" };
cvar_t        v_gunbobrollshift = { "v_gunbobrollshift", "1.5" };
cvar_t        v_gunbobyawshift = { "v_gunbobyawshift", "1" };
cvar_t        v_gunbobxshift = { "v_gunbobxshift", "0" };
cvar_t        v_gunbobyshift = { "v_gunbobyshift", "1.5" };
cvar_t        v_gunbobzshift = { "v_gunbobzshift", "1.5" };

cvar_t        v_guninertia = { "v_guninertia", "1" };
cvar_t        v_gunxoffset = { "v_gunxoffset", "0" };
cvar_t        v_gunyoffset = { "v_gunyoffset", "2" };
cvar_t        v_gunzoffset = { "v_gunzoffset", "0" };
cvar_t        v_gunzoffsetzoom = { "v_gunzoffsetzoom", "10" };

cvar_t        v_kick = { "v_kick", "1" };
cvar_t        v_kicktime = { "v_kicktime", "0.5" };
cvar_t        v_kickroll = { "v_kickroll", "0.6" };
cvar_t        v_kickpitch = { "v_kickpitch", "0.6" };

cvar_t        v_iyaw_cycle = { "v_iyaw_cycle", "2" };
cvar_t        v_iroll_cycle = { "v_iroll_cycle", "0.5" };
cvar_t        v_ipitch_cycle = { "v_ipitch_cycle", "1" };
cvar_t        v_iyaw_level = { "v_iyaw_level", "0.3" };
cvar_t        v_iroll_level = { "v_iroll_level", "0.1" };
cvar_t        v_ipitch_level = { "v_ipitch_level", "0.3" };

cvar_t        v_idlescale = { "v_idlescale", "1" };
cvar_t        v_noisescale = { "v_noisescale", "1" };

cvar_t        v_fovcorrection = { "v_fovcorrection", "0.5" };
cvar_t        v_ratiocorrection = { "v_ratiocorrection", "1" };

cvar_t        crosshair = { "crosshair", "0" };
cvar_t        crosshaircolor = { "crosshaircolor", "79" };
cvar_t        crosshairsize = { "crosshairsize", "1" };

cvar_t        v_cshiftbonus = { "v_cshiftbonus", "1" };
cvar_t        v_cshiftcontent = { "v_cshiftcontent", "1" };
cvar_t        v_cshiftdamage = { "v_cshiftdamage", "1" };
cvar_t        v_cshiftpent = { "v_cshiftpent", "0.2" };
cvar_t        v_cshiftquad = { "v_cshiftquad", "0.2" };
cvar_t        v_cshiftring = { "v_cshiftring", "0.2" };
cvar_t        v_cshiftsuit = { "v_cshiftsuit", "0.2" };

float         cl_punchangle, cl_ideal_punchangle;

static float  airscale;
static float  attackscale;
static float  movescale;
static float  upscale;

float         healthscale;
float         slowscale;
float         speedcycle;
float         speedscale;
float         zoomscale;

float         ratiocorrection;

float         v_dmg_time, v_dmg_roll, v_dmg_pitch;
float         zoom = 0;

float         v_fpshist[FPS_HIST_SIZE];
float         v_fpsmean;

vec3_t        v_ofs;
vec3_t        v_ofsgun;

/*
 * V_CalcFPS
 */
void V_CalcFPS (void) {
    int           i;
    float         sum = 0;
    static double lastrealtime;
    extern int    fps_count;

    if (realtime - lastrealtime > 0.1) {
        for (i = FPS_HIST_SIZE; i > 0; --i) {
            v_fpshist[i] = v_fpshist[i - 1];
            sum += v_fpshist[i];
        }
        v_fpshist[0] = fps_count / (realtime - lastrealtime);
        v_fpsmean = (sum + v_fpshist[0]) / (float) FPS_HIST_SIZE;
        fps_count = 0;
        lastrealtime = realtime;
    }
}

/*
 *
 * PALETTE FLASHES
 *
 *
 */

cshift_t      cshift_empty = { {130, 80, 50}, 0 };
cshift_t      cshift_water = { {26, 16, 10}, 192 };
cshift_t      cshift_slime = { {0, 25, 5}, 192 };
cshift_t      cshift_lava = { {255, 80, 0}, 224 };

#ifdef	GLQUAKE
cvar_t        v_cshiftpercent = { "v_cshiftpercent", "100" };
cvar_t        v_gamma = { "gl_gamma", "1.25" };
cvar_t        v_contrast = { "gl_contrast", "1" };
float         v_blend[4];              // rgba 0.0 - 1.0
unsigned short ramps[3][256];
#else
byte          gammatable[256];         // palette is sent through this
byte          current_pal[768];        // Tonik: used for screenshots
cvar_t        v_gamma = { "gamma", "1" };
cvar_t        v_contrast = { "contrast", "1" };
#endif

#ifndef GLQUAKE
void BuildGammaTable (float g, float c) {
    int           i, inf;

    g = bound (0.3, g, 3);
    c = bound (1, c, 3);

    if (g == 1 && c == 1) {
        for (i = 0; i < 256; i++)
            gammatable[i] = i;
        return;
    }

    for (i = 0; i < 256; i++) {
        inf = 255 * pow ((i + 0.5) / 255.5 * c, g) + 0.5;
        gammatable[i] = bound (0, inf, 255);
    }
}

/*
 * V_CheckGamma
 */
qboolean V_CheckGamma (void) {
    static float  old_gamma, old_contrast;

    if (v_gamma.value == old_gamma && v_contrast.value == old_contrast)
        return false;

    old_gamma = v_gamma.value;
    old_contrast = v_contrast.value;

    BuildGammaTable (v_gamma.value, v_contrast.value);
    vid.recalc_refdef = 1;             // force a surface cache flush

    return true;
}
#endif                                 // GLQUAKE

float         damagetime = 0, damagecount;

/*
 * V_ParseDamage
 */
void V_ParseDamage (void) {
    int           i, armor, blood;
    float         side, fraction;
    vec3_t        from, forward, right, up;
    entity_t     *ent;

    armor = MSG_ReadByte ();
    blood = MSG_ReadByte ();
    for (i = 0; i < 3; i++)
        from[i] = MSG_ReadCoord ();

    damagecount = blood * 0.5 + armor * 0.5;
    if (damagecount < 10)
        damagecount = 10;
    damagetime = cl.time;              // joe: for Q3 on-screen damage splash

    cl.faceanimtime = cl.time + 0.2;   // put sbar face into pain frame

    cl.cshifts[CSHIFT_DAMAGE].percent += 3 * damagecount;
    cl.cshifts[CSHIFT_DAMAGE].percent = bound (0, cl.cshifts[CSHIFT_DAMAGE].percent, 150);

    fraction = bound (0, v_cshiftdamage.value, 1);
    cl.cshifts[CSHIFT_DAMAGE].percent *= fraction;

    if (armor > blood) {
        cl.cshifts[CSHIFT_DAMAGE].destcolor[0] = 200;
        cl.cshifts[CSHIFT_DAMAGE].destcolor[1] = 100;
        cl.cshifts[CSHIFT_DAMAGE].destcolor[2] = 100;
    } else if (armor) {
        cl.cshifts[CSHIFT_DAMAGE].destcolor[0] = 200;   // lxndr: was 220
        cl.cshifts[CSHIFT_DAMAGE].destcolor[1] = 50;
        cl.cshifts[CSHIFT_DAMAGE].destcolor[2] = 50;
    } else {
        cl.cshifts[CSHIFT_DAMAGE].destcolor[0] = 200;   // lxndr: was 255
        cl.cshifts[CSHIFT_DAMAGE].destcolor[1] = 0;
        cl.cshifts[CSHIFT_DAMAGE].destcolor[2] = 0;
    }

    // calculate view angle kicks
    ent = &cl_entities[cl.viewentity];

    VectorSubtract (from, ent->origin, from);
    VectorNormalize (from);

    AngleVectors (ent->angles, forward, right, up);

    side = DotProduct (from, right);
    v_dmg_roll = damagecount * side * v_kickroll.value * healthscale;

    side = DotProduct (from, forward);
    v_dmg_pitch = damagecount * side * v_kickpitch.value * healthscale;

    v_dmg_time = v_kicktime.value * healthscale;
}

/*
 * V_cshift_f
 */
void V_cshift_f (void) {
    cshift_empty.destcolor[0] = atoi (Cmd_Argv (1));
    cshift_empty.destcolor[1] = atoi (Cmd_Argv (2));
    cshift_empty.destcolor[2] = atoi (Cmd_Argv (3));
    cshift_empty.percent = atoi (Cmd_Argv (4));
}

/*
 * V_BonusFlash_f
 *
 * When you run over an item, the server sends this command
 */
void V_BonusFlash_f (void) {
    if (!v_cshiftbonus.value)
        return;

    cl.cshifts[CSHIFT_BONUS].destcolor[0] = 215;
    cl.cshifts[CSHIFT_BONUS].destcolor[1] = 186;
    cl.cshifts[CSHIFT_BONUS].destcolor[2] = 69;
    cl.cshifts[CSHIFT_BONUS].percent = 50;
}

/*
 * V_CalcPowerupCshift
 */
void V_CalcPowerupCshift (void) {
    float         fraction = 0;

    if (cl.items & IT_QUAD) {
        cl.cshifts[CSHIFT_POWERUP].destcolor[0] = 0;
        cl.cshifts[CSHIFT_POWERUP].destcolor[1] = 0;
        cl.cshifts[CSHIFT_POWERUP].destcolor[2] = 255;
        fraction = bound (0, v_cshiftquad.value, 1);
    } else if (cl.items & IT_SUIT) {
        cl.cshifts[CSHIFT_POWERUP].destcolor[0] = 0;
        cl.cshifts[CSHIFT_POWERUP].destcolor[1] = 255;
        cl.cshifts[CSHIFT_POWERUP].destcolor[2] = 0;
        fraction = bound (0, v_cshiftsuit.value, 1);
    } else if (cl.items & IT_INVISIBILITY) {
        cl.cshifts[CSHIFT_POWERUP].destcolor[0] = 255;
        cl.cshifts[CSHIFT_POWERUP].destcolor[1] = 255;
        cl.cshifts[CSHIFT_POWERUP].destcolor[2] = 255;
        fraction = bound (0, v_cshiftring.value, 1);
    } else if (cl.items & IT_INVULNERABILITY) {
        cl.cshifts[CSHIFT_POWERUP].destcolor[0] = 255;
        cl.cshifts[CSHIFT_POWERUP].destcolor[1] = 255;
        cl.cshifts[CSHIFT_POWERUP].destcolor[2] = 0;
        fraction = bound (0, v_cshiftpent.value, 1);
    }
    cl.cshifts[CSHIFT_POWERUP].percent = 100 * fraction;
}

/*
 * V_CalcBlend
 */
#ifdef	GLQUAKE
void V_CalcBlend (void) {
    int           j;
    float         r, g, b, a, a2;

    if (!gl_polyblend.value)
        return;

    r = g = b = a = 0;

    if (cls.state != ca_connected) {
        cl.cshifts[CSHIFT_CONTENTS] = cshift_empty;
        cl.cshifts[CSHIFT_POWERUP].percent = 0;
    } else {
        V_CalcPowerupCshift ();
    }

    // drop the damage value
#  ifdef DEVEL
    if (sv_healthimpact.value)
        j = (100 - cl.stats[STAT_HEALTH]) / (5 - skill.value);
    else
#  endif
        j = 0;
    cl.cshifts[CSHIFT_DAMAGE].percent -= host_frametime * 150.0 / (skill.value + 2) * 2;        // lxndr: added skill scale
    if (cl.cshifts[CSHIFT_DAMAGE].percent <= j)
        cl.cshifts[CSHIFT_DAMAGE].percent = j;

    // drop the bonus value
    cl.cshifts[CSHIFT_BONUS].percent -= host_frametime * 100;
    if (cl.cshifts[CSHIFT_BONUS].percent <= 0)
        cl.cshifts[CSHIFT_BONUS].percent = 0;

    for (j = 0; j < NUM_CSHIFTS; j++) {
        if (!v_cshiftpercent.value)
            continue;

        a2 = ((cl.cshifts[j].percent * v_cshiftpercent.value) / 100.0) / 255.0;

        if (!a2)
            continue;

        a = a + a2 * (1 - a);
        a2 /= a;

        r = r * (1 - a2) + cl.cshifts[j].destcolor[0] * a2;
        g = g * (1 - a2) + cl.cshifts[j].destcolor[1] * a2;
        b = b * (1 - a2) + cl.cshifts[j].destcolor[2] * a2;
    }

    v_blend[0] = r / 255.0;
    v_blend[1] = g / 255.0;
    v_blend[2] = b / 255.0;
    v_blend[3] = bound (0, a, 1);
}

void V_AddLightBlend (float r, float g, float b, float a) {
    if (!gl_polyblend.value || !v_cshiftpercent.value)
        return;

    a *= v_cshiftpercent.value / 100.0;

    v_blend[3] = v_blend[3] + a * (1 - v_blend[3]);

    if (!v_blend[3])
        return;

    a /= v_blend[3];

    v_blend[0] = v_blend[0] * (1 - a) + r * a;
    v_blend[1] = v_blend[1] * (1 - a) + g * a;
    v_blend[2] = v_blend[2] * (1 - a) + b * a;
}
#endif

/*
 * V_KeepConstantLight
 *
 * Keep content light near unchanged by the contrast value
 */
void V_KeepConstantLight (void) {
    cl.cshifts[CSHIFT_CONTENTS].destcolor[0] /= (v_contrast.value);
    cl.cshifts[CSHIFT_CONTENTS].destcolor[1] /= (v_contrast.value);
    cl.cshifts[CSHIFT_CONTENTS].destcolor[2] /= (v_contrast.value);
}

/*
 * V_SetContentsColor
 *
 * Underwater, lava, etc each has a color shift
 */
void V_SetContentsColor (int contents) {
    if (!v_cshiftcontent.value) {
        cl.cshifts[CSHIFT_CONTENTS] = cshift_empty;
        return;
    }

    switch (contents) {
        case CONTENTS_EMPTY:
            cl.cshifts[CSHIFT_CONTENTS] = cshift_empty;
            break;
        case CONTENTS_LAVA:
            cl.cshifts[CSHIFT_CONTENTS] = cshift_lava;
            V_KeepConstantLight ();
            break;
        case CONTENTS_SOLID:
        case CONTENTS_SLIME:
            cl.cshifts[CSHIFT_CONTENTS] = cshift_slime;
            V_KeepConstantLight ();
            break;
        default:
            cl.cshifts[CSHIFT_CONTENTS] = cshift_water;
            V_KeepConstantLight ();
    }

    cl.cshifts[CSHIFT_CONTENTS].percent *= v_cshiftcontent.value;

#ifdef GLQUAKE
    V_CalcBlend ();
#endif
}

/*
 *
 * VIEW RENDERING
 *
 *
 */

cvar_t        v_centermove = { "v_centermove", "0.15" };
cvar_t        v_centerspeed = { "v_centerspeed", "500" };

void Force_CenterView_f (void) {
    cl.viewangles[PITCH] = 0;
}

void V_StartPitchDrift (void) {
    if (cl.laststop == cl.time)
        return;                        // something else is keeping it from drifting

    if (cl.nodrift || !cl.pitchvel) {
        cl.pitchvel = v_centerspeed.value;
        cl.nodrift = false;
        cl.driftmove = 0;
    }
}

void V_StopPitchDrift (void) {
    cl.laststop = cl.time;
    cl.nodrift = true;
    cl.pitchvel = 0;
}

/*
 * V_DriftPitch
 *
 * Moves the client pitch angle towards cl.idealpitch sent by the server.
 *
 * If the user is adjusting pitch manually, either with lookup/lookdown, mlook
 * and mouse, or klook and keyboard, pitch drifting is constantly stopped.
 *
 * Drifting is enabled when the center view key is hit, mlook is released and
 * lookspring is non 0, or when
 */
void V_DriftPitch (void) {
    float         delta, move;

    if (!cl.onground || cls.demoplayback) {
        cl.driftmove = 0;
        cl.pitchvel = 0;
        return;
    }
    // don't count small mouse motion
    if (cl.nodrift) {
        if (fabs (cl.cmd.forwardmove) < cl_speedforward.value)
            cl.driftmove = 0;
        else
            cl.driftmove += host_frametime;

        if (cl.driftmove > v_centermove.value)
            V_StartPitchDrift ();
        return;
    }

    delta = cl.idealpitch - cl.viewangles[PITCH];

    if (!delta) {
        cl.pitchvel = 0;
        return;
    }

    move = host_frametime * cl.pitchvel;
    cl.pitchvel += host_frametime * v_centerspeed.value;

//    Con_Printf ("move: %f (%f)\n", move, host_frametime);

    if (delta > 0) {
        if (move > delta) {
            cl.pitchvel = 0;
            move = delta;
        }
        cl.viewangles[PITCH] += move;
    } else if (delta < 0) {
        if (move > -delta) {
            cl.pitchvel = 0;
            move = -delta;
        }
        cl.viewangles[PITCH] -= move;
    }
}

void V_DropPunchAngle (void) {
    if (cl_ideal_punchangle < cl_punchangle) {
        if (cl_ideal_punchangle >= -2) // small kick
            cl_punchangle -= 20 * host_frametime;
        else                           // big kick
            cl_punchangle -= 40 * host_frametime;

        if (cl_punchangle < cl_ideal_punchangle) {
            cl_punchangle = cl_ideal_punchangle;
            cl_ideal_punchangle = 0;
        }
    } else {
        cl_punchangle += 8 * host_frametime;
        if (cl_punchangle > 0)
            cl_punchangle = 0;
    }
}

// 

float V_CalcAngleDelta (float a) {
    a = anglemod (a);
    if (a > 180)
        a -= 360;
    return a;
}

/*
 * V_CalcHealthScale
 * value between 1 (100% health) and 2 (0% health)
 */
void V_CalcHealthScale (void) {
    if (sv_healthimpact.value)
        healthscale = (100.0 - bound (0, cl.stats[STAT_HEALTH], 100)) / 100.0 / (4.0 / (skill.value + 1.0)) + 1;
    else
        healthscale = 1;
}

/*
 * V_CalcSpeedCycle
 * used to adjut player and weapon movement following player speed
 */
void V_CalcSpeedCycle (void) {
    static float  delta = 0, oldspeed = 0;
    float         speed, cycle;

    cycle = 2 * M_PI * cl.time;
    speed = logf (speedscale * powf (healthscale, v_bobcyclehealth.value) * v_bobcyclefine.value + 1) * (v_bobcycle.value * 1.666); // lxndr: 1.666 to keep default v_bobcycle value unchanged
    delta += cycle * (oldspeed - speed);
    oldspeed = speed;
    speedcycle = cycle * speed + delta;
}

/*
 * V_CalcSpeedScale
 */
void V_CalcSpeedScale (void) {
    speedscale = sqrt (cl.velocity[0] * cl.velocity[0] + cl.velocity[1] * cl.velocity[1] + cl.velocity[2] * cl.velocity[2]);
}

/*
 * V_CalcViewScale
 */
void V_CalcViewScale (void) {

    // smooths jumps
    if (cl.onground)
        airscale += 5.0 * host_frametime;
    else
        airscale -= 2.0 * host_frametime;
    airscale = bound (0, airscale, 1);

    // smooths shooting transition
    if (in_attack.state & 1)
        attackscale -= 10.0 / healthscale * host_frametime;
    else
        attackscale += 2.5 / healthscale * host_frametime;
    attackscale = bound (0, attackscale, 1);

    // smooths zoom transition
    if (cl.stats[STAT_ACTIVEWEAPON] == IT_AXE)
        zoomscale = 1;
    else
        zoomscale = ((20 - 5 * healthscale) - zoom * 0.8) / (20 - 5 * healthscale);

    // slow
    slowscale = powf (speedscale / v_bobslowthreshold.value, 1.0 - speedscale / 50.0) * v_bobslowscale.value + 1.0;

    // up
    upscale = bound (-5, cl.velocity[2] / (20.0 + speedscale) * healthscale, 5);

    // move
    movescale = (movescale + fabs (upscale) + logf (speedscale * (v_bob.value * 50) / 200 + 1.0) * healthscale) / 2.0; // lxndr: 50 to keep default v_bob value unchanged
    movescale *= airscale;
}

static vec3_t right;
/*
 * V_CalcRoll
 *
 * Used by view and sv_user
 */
float V_CalcRoll (vec3_t angles, vec3_t velocity) {
    float         sign, side;

    AngleVectors (angles, NULL, right, NULL);
    side = DotProduct (velocity, right);
    sign = side < 0 ? -1 : 1;
    side = fabs (side);

    side = (side < cl_rollspeed.value) ? side * cl_rollangle.value / cl_rollspeed.value : cl_rollangle.value;

    return side * sign;
}

/*
 * V_BoundOffsets
 */
void V_BoundOffsets (void) {
    entity_t     *ent;

    ent = &cl_entities[cl.viewentity];

    // absolutely bound refresh reletive to entity clipping hull so the view can never be inside a solid wall
    r_refdef.vieworg[0] = max (r_refdef.vieworg[0], ent->origin[0] - 14);
    r_refdef.vieworg[0] = min (r_refdef.vieworg[0], ent->origin[0] + 14);
    r_refdef.vieworg[1] = max (r_refdef.vieworg[1], ent->origin[1] - 14);
    r_refdef.vieworg[1] = min (r_refdef.vieworg[1], ent->origin[1] + 14);
    r_refdef.vieworg[2] = max (r_refdef.vieworg[2], ent->origin[2] - 22);
    r_refdef.vieworg[2] = min (r_refdef.vieworg[2], ent->origin[2] + 30);
}

/*
 * V_AddBob
 */
float V_AddBob (void) {
    float         bob, cycle, bobcycle;

    if (v_bobcycle.value <= 0)
        return 0;

    bob = 0;
    if (game.legacy) {
        bobcycle = v_bobcycle.value;
        if (in_zoom.state & 1)
            bobcycle *= 1.2;

        cycle = (cl.time - (int) (cl.time / bobcycle) * bobcycle) / bobcycle;
        if (cycle < 0.5)
            cycle = M_PI * cycle / 0.5;
        else
            cycle = M_PI + M_PI * (cycle - 0.5) / 0.5;

        // bob is proportional to velocity in the xy plane
        bob = sqrt (cl.velocity[0] * cl.velocity[0] + cl.velocity[1] * cl.velocity[1]) * v_bob.value;
        bob = bob * 0.3 + bob * 0.7 * Q_sin (cycle);
        bob = bound (-7, bob, 4);

        r_refdef.vieworg[2] += bob;
    } else {
        bob = airscale * healthscale * slowscale * (v_bob.value * 50); // lxndr: 50 to keep default v_bob value unchanged

        r_refdef.vieworg[2] += Q_sin (speedcycle) * 2.5 * bob * v_bobz.value;
        r_refdef.viewangles[PITCH] += 5.0 * movescale * v_bobpitch.value;
        r_refdef.viewangles[PITCH] += (atan (Q_sin (speedcycle - v_bobpitchshift.value * M_PI) - 1.5 + healthscale) + 1.0) * bob * v_bobpitch.value;
        r_refdef.viewangles[ROLL] -= Q_sin ((speedcycle - (1.5 + v_bobrollshift.value) * M_PI) / 2.0) * bob * v_bobroll.value;
        r_refdef.viewangles[YAW] += Q_sin ((speedcycle - (1.5 + v_bobyawshift.value) * M_PI) / 2.0) * bob * v_bobyaw.value;
    }

    return bob; // lxndr: for legacy
}

/*
 * V_AddIdle
 *
 * Idle swaying
 */
void V_AddIdle (void) {
    float         iscale;

    iscale = v_idlescale.value * healthscale * (2.0 - zoomscale);

    r_refdef.viewangles[ROLL] += iscale * Q_sin (cl.time * v_iroll_cycle.value) * v_iroll_level.value;
    r_refdef.viewangles[PITCH] += iscale * Q_sin (cl.time * v_ipitch_cycle.value) * v_ipitch_level.value;
    r_refdef.viewangles[YAW] += iscale * Q_sin (cl.time * v_iyaw_cycle.value) * v_iyaw_level.value;
}

/*
 * V_AddKick
 */
void V_AddKick (void) {
    vec3_t        forward;

    if (v_dmg_time > 0) {
        r_refdef.viewangles[ROLL] += v_dmg_time / v_kicktime.value * v_dmg_roll;
        r_refdef.viewangles[PITCH] += v_dmg_time / v_kicktime.value * v_dmg_pitch;
        v_dmg_time -= host_frametime;
    }

    if (v_kick.value == 1) {
        VectorAdd (r_refdef.viewangles, cl.punchangle, r_refdef.viewangles);
    } else if (v_kick.value == 2) {
        // add kick offset
        AngleVectors (r_refdef.viewangles, forward, NULL, NULL);
        VectorMA (r_refdef.vieworg, cl_punchangle, forward, r_refdef.vieworg);

        // add kick angle
        r_refdef.viewangles[PITCH] += cl_punchangle * 0.8;      // lxndr: was 0.5
    }
}

/*
 * V_AddNoise
 */
void V_AddNoise (void) {
    float        nscale;

    nscale = v_noisescale.value * (healthscale - 1.0) * powf (healthscale, 5) / 100.0 * (1.0 + movescale);

    r_refdef.viewangles[YAW] -= Q_cos (Q_tan (cl.time * cl.time)) * nscale;
    r_refdef.viewangles[PITCH] -= Q_sin (Q_tan (cl.time * cl.time)) * nscale;
    if (cl.stats[STAT_HEALTH] > 0 && cls.state == ca_connected)
        r_refdef.vieworg[2] -= Q_cos (2 * cl.time) * (healthscale - 1) * 0.2;     // lxndr: simulate breath
}

/*
 * V_AddRoll
 *
 * Roll is induced by movement and damage
 */
void V_AddRoll (void) {
    float         side;

    side = V_CalcRoll (cl_entities[cl.viewentity].angles, cl.velocity);
    r_refdef.viewangles[ROLL] += side;

    r_refdef.viewangles[ROLL] += zoom / (20 - 5 * healthscale) * v_gunyoffset.value;

#ifdef DEVEL // lxndr: have to find how to take the weapon position as the effective origin of shots
    r_refdef.vieworg[0] += zoom / (20 - 5 * healthscale) * v_gunyoffset.value;
    r_refdef.vieworg[2] -= zoom / (20 - 5 * healthscale);
#endif
}

/*
 * V_CalcFov
 */
float V_CalcFov (float fov_x, float width, float height) {
    float         a, x;

    fov_x = bound (1, fov_x, 179);     // lxndr: bound added

    x = width / Q_tan (fov_x / 360 * M_PI);

    a = Q_atan (height / x);

    a = a * 360 / M_PI;

    return a;
}

/*
 * V_CalcInvFov // lxndr: not used
 */
float V_CalcInvFov (float fov_y, float width, float height) {
    float         a, x, fov_x;

    a = fov_y * M_PI / 360;

    x = height / Q_tan (a);

    fov_x = Q_atan (width / x) * (360 / M_PI);

    return fov_x;
}

/*
 * V_CalcRatioCorrection
 * there might be a cleaner way by playing with fov calculation within R_SetupGL or the like
 * or by checking if V_CalcFov functions have the right width and height arguments
 */
void V_CalcRatioCorrection (void) {
    if (v_ratiocorrection.value == 2)
        ratiocorrection = (240 - (float) r_refdef.vrect.height / (float) r_refdef.vrect.width * 320) / 20.0;    // the 0.1 factor is guessed
    else
        ratiocorrection = 0;
}

/*
 * V_CalcZoom
 */
void V_CalcZoom () {
    extern cvar_t scr_fov;
    float         limit;

    if ((in_zoom.state & 1) && cl.stats[STAT_HEALTH] > 0 /*&& damagecount == 10 */ ) {  // zoom on
        limit = 20 - 5 * healthscale;
        if (zoom < limit) {
            zoom += 25 / (healthscale * healthscale) * host_frametime;  // zoom speed
            zoom = min (zoom, limit);
        }
    } else if (zoom > 0) {             // zoom off, was zooming
        zoom -= 50 / healthscale * host_frametime;
        zoom = max (zoom, 0);
    } else {
        zoom = 0;
    }

    r_refdef.fov_x = scr_fov.value - zoom;

    if (game.quore && cl.stats[STAT_HEALTH] > 0 && cls.state == ca_connected) {
        if (v_noisescale.value)
            r_refdef.fov_x -= (healthscale - 1) * Q_cos (2 * cl.time);    // noise
//        if (cl.time > 0 && cl.time < 2.5)
//            r_refdef.fov_x -= (2.5 - cl.time) * (2.5 - cl.time) * 2.5;    // unzoom
    }

    r_refdef.fov_y = V_CalcFov (r_refdef.fov_x, r_refdef.vrect.width, r_refdef.vrect.height);
}

/*
 * V_CalcWeaponOffset
 */
void V_CalcWeaponOffset (void) {
    static float  ofsx, ofsy, ofsz;
    float         limit;

    if (in_zoom.state & 1 && cl.stats[STAT_HEALTH] > 0 && cl.stats[STAT_ACTIVEWEAPON] != IT_AXE) {       // zoom on
        limit = -2.5;
        if (ofsx > limit) {
            ofsx -= 2.5 / healthscale * host_frametime;
            ofsx = max (ofsx, limit);
        }
        limit = fabs (v_gunyoffset.value) / 5.0;
        if (ofsy > limit) {
            ofsy -= 2 * ofsy / healthscale * host_frametime;
            ofsy = max (ofsy, limit);
        }
        limit = v_gunzoffsetzoom.value - ratiocorrection;
        if (ofsz < limit) {
            ofsz += 45 / (1 + healthscale) * host_frametime;
            ofsz = min (ofsz, limit);
        }
    } else if (zoom > 0) {             // zoom off, was zooming
        ofsx += 20 / healthscale * host_frametime;
        ofsx = min (ofsx, v_gunxoffset.value);
        ofsy += (5 + powf (v_gunyoffset.value / 1.5, 2)) / healthscale * host_frametime;
        ofsy = min (ofsy, fabs (v_gunyoffset.value));
        ofsz -= 100 / (1 + healthscale) * host_frametime;
        ofsz = max (ofsz, v_gunzoffset.value);
    } else {
        ofsx = v_gunxoffset.value;
        ofsy = fabs (v_gunyoffset.value);
        ofsz = v_gunzoffset.value;
    }

    v_ofs[0] = ofsx;
    v_ofs[1] = v_gunyoffset.value > 0 && ofsy > 0 ? ofsy : -ofsy;
    v_ofs[2] = ofsz;
}

/*
 * V_CalcWeaponAngle
 */
void V_CalcWeaponAngle (vec3_t viewangles) {
    float         yaw, pitch, move, bob, gunscale, iscale, nscale;
    static float  oldyaw = 0, oldpitch = 0;

    yaw = r_refdef.viewangles[YAW];
    yaw = V_CalcAngleDelta (yaw - viewangles[YAW]);// * v_guninertia.value;
    pitch = r_refdef.viewangles[PITCH] * 1.1;  // lxndr: more amplitude
    pitch = V_CalcAngleDelta (pitch - viewangles[PITCH]);// * v_guninertia.value;

    move = host_frametime * 20;
    if (yaw > oldyaw) {
        if (oldyaw + move < yaw)
            yaw = oldyaw + move;
    } else {
        if (oldyaw - move > yaw)
            yaw = oldyaw - move;
    }
    if (pitch > oldpitch) {
        if (oldpitch + move < pitch)
            pitch = oldpitch + move;
    } else {
        if (oldpitch - move > pitch)
            pitch = oldpitch - move;
    }
    oldyaw = yaw;
    oldpitch = pitch;

    cl.viewent.angles[YAW] = r_refdef.viewangles[YAW] + yaw;
    cl.viewent.angles[PITCH] = -(r_refdef.viewangles[PITCH] + pitch);
    cl.viewent.angles[PITCH] /= 1.1;

    if (v_idlescale.value) {
        iscale = 2.0 * v_idlescale.value * healthscale;
        if (cl.stats[STAT_ACTIVEWEAPON] == IT_AXE)
            iscale *= 2.0;

        if (in_zoom.state & 1)
            iscale /= 1.5;
        cl.viewent.angles[YAW] -= iscale * Q_sin (cl.time * v_iyaw_cycle.value) * v_iyaw_level.value;
        cl.viewent.angles[ROLL] -= iscale * Q_sin (cl.time * v_iroll_cycle.value) * v_iroll_level.value;
        cl.viewent.angles[PITCH] -= iscale * Q_sin (cl.time * v_ipitch_cycle.value) * v_ipitch_level.value;
    }

    if (v_noisescale.value) {
        nscale = v_noisescale.value * (healthscale - 1) * powf (healthscale, 5) / 50.0;

        cl.viewent.angles[YAW] -= Q_sin (Q_tan (cl.time * cl.time)) * nscale; // FIXME: noise
        cl.viewent.angles[PITCH] -= Q_cos (Q_tan (cl.time)) * nscale; // FIXME: noise
    }

    if (v_gunbob.value == 2) {
        // gun
        if (cl.stats[STAT_ACTIVEWEAPON] == IT_AXE) {
            cl.viewent.angles[YAW] -= 5.0 * v_gunyoffset.value * attackscale;
            cl.viewent.angles[PITCH] += 15.0 * attackscale;
            gunscale = 5.0 / healthscale;
        } else {
            cl.viewent.angles[YAW] += v_gunyoffset.value / (1.0 + movescale) * attackscale * zoomscale;
            cl.viewent.angles[PITCH] -= 17.5 / powf (1.0 + movescale, 2) * attackscale * zoomscale;
            switch (cl.stats[STAT_ACTIVEWEAPON]) {
                case IT_GRENADE_LAUNCHER:
                case IT_ROCKET_LAUNCHER:
                    cl.viewent.angles[PITCH] += 2.0 * attackscale * zoomscale;
                    break;
                default:
                    cl.viewent.angles[ROLL] -= 5.0 * movescale * attackscale * zoomscale;
            }
            gunscale = 5.0 - zoomscale * healthscale;
        }

        // bob
        bob = airscale * gunscale * movescale;
        cl.viewent.angles[YAW] += Q_sin ((speedcycle - (1.5 + v_gunbobyawshift.value + healthscale / 2.0 - movescale) * M_PI) / 2.0) * bob * v_gunbobyaw.value;
        if (cl.stats[STAT_ACTIVEWEAPON] == IT_AXE)
            cl.viewent.angles[ROLL] += Q_sin (speedcycle - (1.0 + v_gunbobrollshift.value) * M_PI) * slowscale * bob * v_gunbobroll.value;
        else
            cl.viewent.angles[ROLL] += Q_sin (speedcycle - v_gunbobrollshift.value * M_PI) * slowscale * bob * v_gunbobroll.value;
        cl.viewent.angles[PITCH] += atan (Q_sin (speedcycle - v_gunbobpitchshift.value * M_PI) - 1.5 + healthscale) * slowscale * bob * v_gunbobpitch.value;

        // !attack
        cl.viewent.angles[YAW] += bound (-25, (7.5 * movescale + upscale) * v_gunyoffset.value, 25) * attackscale * zoomscale;
        cl.viewent.angles[PITCH] -= bound (-15, ((17.5 - gunscale) * movescale) * healthscale, 15) * attackscale * zoomscale;
    }
}

/*
 * V_AdjustWeaponOffset
 * all weapon do not behave the same way, try to adjust values
 */
void V_AdjustWeaponOffset (void) {
    v_ofs[2] += ratiocorrection;

    switch (cl.stats[STAT_ACTIVEWEAPON]) {
        case IT_AXE:
            if (v_gunbob.value == 2) {
                v_ofs[0] -= 5.0;
                v_ofs[1] /= 2.0;
            }
            break;
        case IT_SUPER_SHOTGUN:
        case IT_SHOTGUN:
            if (v_gunbob.value == 2)
                v_ofs[0] -= 2.5;
            v_ofs[1] /= 2.0;
            v_ofs[2] /= 2.5;
            break;
        case IT_ROCKET_LAUNCHER:
            v_ofs[1] /= 2.0;
            v_ofs[2] /= 2.0;
            break;
        case IT_GRENADE_LAUNCHER:
            v_ofs[2] /= 2.5;
            break;
        case IT_LIGHTNING:
            v_ofs[1] /= 1.5;
            v_ofs[2] /= 2.0;
            break;
        case IT_SUPER_NAILGUN:
            v_ofs[2] /= 1.5;
            break;
        case IT_NAILGUN:
            v_ofs[2] /= 1.5;
            break;
        default:
            v_ofs[2] /= 1.5;
    }
}

/*
 * V_AddViewWeapon
 */
void V_AddViewWeapon (vec3_t vieworg, vec3_t viewangles, float bob) {
    entity_t     *view;
    float         mousescale;

    // view is the weapon model (only visible from inside body)
    view = &cl.viewent;
    view->model = cl.model_precache[cl.stats[STAT_WEAPON]];
    view->frame = cl.stats[STAT_WEAPONFRAME];
    view->colormap = vid.colormap;

    VectorCopy (vieworg, view->origin);        // set up gun position
    VectorCopy (viewangles, view->angles);     // set up gun angles

    V_CalcRatioCorrection ();          // lxndr: added
    V_CalcWeaponAngle (view->angles);
    V_CalcWeaponOffset ();             // lxndr: added

    // inertia
    mousescale = bound (-1.0, in_mousemean_x / host_frametime * v_guninertia.value / 10000.0 * healthscale * zoomscale, 1.0);
    if (cl.stats[STAT_ACTIVEWEAPON] == IT_AXE)
        v_ofs[0] -= fabs (mousescale) * 5.0;
    else
        v_ofs[0] += v_gunyoffset.value > 0 ? mousescale : v_gunyoffset.value < 0 ? -mousescale : -fabs (mousescale);
    v_ofs[1] -= mousescale * 5.0;
    v_ofs[2] += v_gunyoffset.value == 0 ? fabs (mousescale) * zoomscale : mousescale * zoomscale;

    // pitch
    v_ofs[0] -= fabs (view->angles[PITCH]) / 100.0 * zoomscale;
    v_ofs[2] -= view->angles[PITCH] / (20.0 * (ratiocorrection / 2.0 + 1)) * zoomscale;

    // speed
    if (game.legacy) {
        v_ofs[0] += bob;
    } else {
        switch ((int) v_gunbob.value) {
            case 1:
                // bob
//                v_ofs[1] += Q_sin (speedcycle / 2.0 - M_PI) * movescale * zoomscale;
//                v_ofs[2] += Q_sin (speedcycle) * movescale * zoomscale;
                v_ofs[1] += Q_sin ((speedcycle - (1.5 + v_gunbobyshift.value + healthscale + movescale) * M_PI) / 2.0) * movescale * slowscale * zoomscale * v_gunboby.value;
                v_ofs[2] += Q_sin (speedcycle - (v_gunbobzshift.value + 0.5) * M_PI) * slowscale * (0.25 + zoomscale) * v_gunbobz.value;

                // move
                v_ofs[1] += bound (-2, v_gunyoffset.value / 2.0 * movescale * zoomscale, 2);
                v_ofs[2] += 2 * movescale * zoomscale;
                break;

            case 2:
                // bob
                v_ofs[1] += Q_sin ((speedcycle - (1.5 + v_gunbobyshift.value + healthscale / 2.0 + movescale) * M_PI) / 2.0) * movescale * slowscale * zoomscale * v_gunboby.value;
                v_ofs[2] += Q_sin (speedcycle - v_gunbobzshift.value * M_PI) * slowscale * (0.25 + zoomscale) * v_gunbobz.value;

                // attack
                switch (cl.stats[STAT_ACTIVEWEAPON]) {
                    case IT_SHOTGUN:
                    case IT_SUPER_SHOTGUN:
                    case IT_GRENADE_LAUNCHER:
                    case IT_ROCKET_LAUNCHER:
                        v_ofs[0] += (2.0 - zoomscale) * (1.0 - attackscale);
                        break;

                    default:
                        v_ofs[0] -= 5.0 * (1.0 - attackscale) * zoomscale;
                }
                v_ofs[1] -= v_gunyoffset.value / 2.0 * zoomscale * (1 - attackscale); // align with the player view when shooting
                v_ofs[2] -= 10.0 * (zoomscale - 0.2) * (1 - attackscale); // slight recoil

                // move
                if (cl.stats[STAT_ACTIVEWEAPON] == IT_AXE)
                    v_ofs[0] -= 2.5 * movescale;
                else
                    v_ofs[0] -= 2.5 * movescale * zoomscale;
                v_ofs[1] += bound (-5, v_gunyoffset.value / 1.5 * movescale * zoomscale, 5);
                v_ofs[2] += movescale * slowscale * zoomscale;

                // !move
                v_ofs[0] += (4.0 - fabs (v_gunyoffset.value / 2.0)) * zoomscale; // keeps gun near the player
                if (cl.stats[STAT_ACTIVEWEAPON] == IT_AXE)
                    v_ofs[2] -= 2.5 / (1.0 + movescale);
                else
                    v_ofs[2] += (5.5 + 5.5 / (1.0 + movescale)) * zoomscale;

                // up
                v_ofs[2] += upscale * zoomscale;

        }
    }

    V_AdjustWeaponOffset ();
}

/*
 * V_CalcIntermissionRefdef
 */
void V_CalcIntermissionRefdef (void) {
    entity_t     *ent, *view;
    float         old;

    // ent is the player model (visible when out of body)
    ent = &cl_entities[cl.viewentity];
    // view is the weapon model (only visible from inside body)
    view = &cl.viewent;

    VectorCopy (ent->origin, r_refdef.vieworg);
    VectorCopy (ent->angles, r_refdef.viewangles);
    view->model = NULL;

    // always idle in intermission
    old = v_idlescale.value;
    v_idlescale.value = 1;
    V_AddIdle ();
    v_idlescale.value = old;
}

/*
 * V_CalcRefdef
 */
void V_CalcRefdef (void) {
    float         bob;
    entity_t     *ent;
//    vec3_t        viewangles, vieworg;

    V_CalcHealthScale ();              // lxndr: added
    V_CalcSpeedScale ();               // lxndr: added
    V_CalcSpeedCycle ();               // lxndr: added
    V_CalcViewScale ();                // lxndr: added
    V_DriftPitch ();

    // ent is the player model (visible when out of body)
    ent = &cl_entities[cl.viewentity];
    S_Footsteps (ent->origin);

    VectorCopy (ent->origin, r_refdef.vieworg); // set up the refresh position
    VectorCopy (cl.viewangles, r_refdef.viewangles);    // set up the refresh view angles

    if (cl.stats[STAT_HEALTH] <= 0) {
        r_refdef.viewangles[ROLL] = 80; // dead view angle
        r_refdef.vieworg[2] -= 10; // add view height
    } else {
        r_refdef.vieworg[2] += cl.viewheight + cl.crouch; // add view height
    }

//    VectorCopy (r_refdef.vieworg, vieworg);
//    VectorCopy (r_refdef.viewangles, viewangles);

    bob = V_AddBob ();
    if (v_noisescale.value)
        V_AddNoise ();
    V_AddViewWeapon (r_refdef.vieworg, r_refdef.viewangles, bob);
    if (v_idlescale.value)
        V_AddIdle ();
    if (v_kick.value)
        V_AddKick ();
    V_AddRoll ();
    V_BoundOffsets ();

    if (chase_active.value)
        Chase_Update ();
}

/*
 * V_RenderView
 *
 * The player's clipping box goes from (-16 -16 -24) to (16 16 32) from the
 * entity origin, so any view position inside that will be valid
 *
 */
void V_RenderView (void) {
    if (con_forcedup)
        return;

    if (cls.state != ca_connected)
        return;

    V_CalcFPS ();
    V_DropPunchAngle ();
    if (cl.intermission) {             // intermission / finale rendering
        V_CalcIntermissionRefdef ();
    } else {
        if (!cl.paused)
            V_CalcRefdef ();
    }

    R_PushDlights ();
#ifndef GLQUAKE
    if (lcd_x.value) {
        // render two interleaved views
        int           i;

        vid.rowbytes <<= 1;
        vid.aspect *= 0.5;

        r_refdef.viewangles[YAW] -= lcd_yaw.value;
        for (i = 0; i < 3; i++)
            r_refdef.vieworg[i] -= right[i] * lcd_x.value;
        R_RenderView ();

        vid.buffer += vid.rowbytes >> 1;

        R_PushDlights ();

        r_refdef.viewangles[YAW] += lcd_yaw.value * 2;
        for (i = 0; i < 3; i++)
            r_refdef.vieworg[i] += 2 * right[i] * lcd_x.value;
        R_RenderView ();

        vid.buffer -= vid.rowbytes >> 1;

        r_refdef.vrect.height <<= 1;

        vid.rowbytes >>= 1;
        vid.aspect *= 2;
    } else
#endif
        R_RenderView ();
}

#ifdef GLQUAKE
#ifdef DEBUG
void V_BobDebug (float v1, float v2) {
    float x, y;

    glLineWidth (1.0);
    glBegin (GL_LINES);

    glColor4f (0, 1, 0, scr_textalpha.value);
    x = vid.width / 2;
    y = vid.height / 2;
    glVertex2f (x, y);
    glVertex2f (x - 100, y);
    glVertex2f (x, y);
    glVertex2f (x, y - 100);

    glEnd ();
    glColor3ubv (color_white);
}
#endif

/*
 * V_AddFog_f
 *
 * Warp mod seems to want that, should make fog with cshift_empty when !GLQUAKE
 */
void V_AddFog_f (void) {
    extern cvar_t gl_fog;
    Cvar_SetValue (&gl_fog, !gl_fog.value);
}
#endif

// 

/*
 * V_Init
 */
void V_Init (void) {
    Cmd_AddCommand ("v_cshift", V_cshift_f);
    Cmd_AddCommand ("bf", V_BonusFlash_f);
    Cmd_AddCommand ("centerview", V_StartPitchDrift);
    Cmd_AddCommand ("force_centerview", Force_CenterView_f);

#ifndef GLQUAKE
    Cvar_Register (&lcd_x);
    Cvar_Register (&lcd_yaw);
#else
    Cmd_AddCommand ("fog", V_AddFog_f);
#endif

    Cvar_Register (&v_centermove);
    Cvar_Register (&v_centerspeed);

    Cvar_Register (&v_iyaw_cycle);
    Cvar_Register (&v_iroll_cycle);
    Cvar_Register (&v_ipitch_cycle);
    Cvar_Register (&v_iyaw_level);
    Cvar_Register (&v_iroll_level);
    Cvar_Register (&v_ipitch_level);
    Cvar_Register (&v_idlescale);
    Cvar_Register (&v_noisescale);

    Cvar_Register (&crosshair);
    Cvar_Register (&crosshaircolor);
    Cvar_Register (&crosshairsize);

    Cvar_Register (&cl_rollspeed);
    Cvar_Register (&cl_rollangle);

    Cvar_Register (&v_bob);
    Cvar_Register (&v_bobcycle);
    Cvar_Register (&v_bobcyclefine);
    Cvar_Register (&v_bobcyclehealth);
    Cvar_Register (&v_bobpitch);
    Cvar_Register (&v_bobroll);
    Cvar_Register (&v_bobyaw);
    Cvar_Register (&v_bobz);
    Cvar_Register (&v_bobpitchshift);
    Cvar_Register (&v_bobrollshift);
    Cvar_Register (&v_bobyawshift);
    Cvar_Register (&v_bobslowscale);
    Cvar_Register (&v_bobslowthreshold);

    Cvar_Register (&v_gunbob);
    Cvar_Register (&v_gunbobpitch);
    Cvar_Register (&v_gunbobroll);
    Cvar_Register (&v_gunbobyaw);
    Cvar_Register (&v_gunbobx);
    Cvar_Register (&v_gunboby);
    Cvar_Register (&v_gunbobz);
    Cvar_Register (&v_gunbobpitchshift);
    Cvar_Register (&v_gunbobrollshift);
    Cvar_Register (&v_gunbobyawshift);
    Cvar_Register (&v_gunbobxshift);
    Cvar_Register (&v_gunbobyshift);
    Cvar_Register (&v_gunbobzshift);
    Cvar_Register (&v_guninertia);
    Cvar_Register (&v_gunxoffset);
    Cvar_Register (&v_gunyoffset);
    Cvar_Register (&v_gunzoffset);
    Cvar_Register (&v_gunzoffsetzoom);

    Cvar_Register (&v_kick);
    Cvar_Register (&v_kicktime);
    Cvar_Register (&v_kickroll);
    Cvar_Register (&v_kickpitch);

    Cvar_Register (&v_fovcorrection);
    Cvar_Register (&v_ratiocorrection);

    Cvar_Register (&v_cshiftbonus);
    Cvar_Register (&v_cshiftcontent);
    Cvar_Register (&v_cshiftdamage);
    Cvar_Register (&v_cshiftpent);
    Cvar_Register (&v_cshiftquad);
    Cvar_Register (&v_cshiftring);
    Cvar_Register (&v_cshiftsuit);

    Cvar_Register (&v_gamma);
    Cvar_Register (&v_contrast);

#ifdef GLQUAKE
    Cvar_Register (&v_cshiftpercent);

    Cmd_AddLegacyCommand ("gamma", v_gamma.name);
    Cmd_AddLegacyCommand ("contrast", v_contrast.name);
#else
    BuildGammaTable (v_gamma.value, v_contrast.value);
#endif
}
