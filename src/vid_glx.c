/*
 * Copyright (C) 1996-1997 Id Software, Inc.
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation; either version 2 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.
 *
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 */
// vid_glx.c -- Linux GLX driver

#include <pthread.h>
#include <termios.h>
#include <sys/ioctl.h>
#include <sys/stat.h>
#include <sys/vt.h>
#include <stdarg.h>
#include <stdio.h>
#include <signal.h>

#include <dlfcn.h>

#include "quakedef.h"

#include <GL/glx.h>
#include <GL/glxext.h>
#include <X11/cursorfont.h>

#ifdef HAVE_DGA
#  include <X11/extensions/Xxf86dga.h>
static qboolean dgamouse = false, dgakeyb = false;
#endif

#define	WARP_WIDTH	320
#define	WARP_HEIGHT	200

GLXContext    ctx = NULL;

static float  old_windowed_mouse = 0, mouse_x, mouse_y, old_mouse_x, old_mouse_y;

static int    glxattr[] = {
    GLX_RGBA,
    GLX_RED_SIZE, 1,
    GLX_GREEN_SIZE, 1,
    GLX_BLUE_SIZE, 1,
    GLX_DOUBLEBUFFER,
    GLX_DEPTH_SIZE, 1,
    GLX_STENCIL_SIZE, 1,
#ifdef DEVEL0
    GLX_X_VISUAL_TYPE, GLX_DIRECT_COLOR,        // lxndr: for gamma correction in windowed mode
#endif
#ifdef DEVEL0
    GLX_SAMPLE_BUFFERS_ARB, 1, GLX_SAMPLES_ARB, 16,     // lxndr:
#endif
    None
};

qboolean      gl_have_stencil = false;

int           (*qglXSwapInterval) (int count) = NULL;
int           (*qglXGetVideoSync) (unsigned int *count) = NULL;
int           (*qglXWaitVideoSync) (int div, int rem, unsigned int *count) = NULL;

const char   *glx_extensions;

static extension_t extensionlist[] = {
    {"GLX_SGI_swap_control", &gl_ext.swap_control},
    {"GLX_SGI_video_sync", &gl_ext.video_sync}
};

#define NUM_EXTENSIONS (sizeof(extensionlist) / sizeof(extension_t))

qboolean OnChange_vid_vsync (cvar_t * var, char *string) {
    int           newval = Q_atoi (string);

    if (gl_ext.swap_control) {
        if (vid_vsync.value > 1) {
            if (newval < 2)
                qglXSwapInterval (0);
            else
                qglXSwapInterval (newval - 1);
        } else if (vid_vsync.value < 2 && newval > 1) {
            qglXSwapInterval (newval - 1);
        }
    }

    return false;
}

qboolean CheckGLXExtension (Display * dpy, int scr, const char *extension) {
    char         *where, *terminator;
    const char   *start;

    if (!glx_extensions && !(glx_extensions = glXQueryExtensionsString (dpy, scr)))
        return false;

    if (!extension || *extension == 0 || strchr (extension, ' '))
        return false;

    for (start = glx_extensions; (where = strstr (start, extension)); start = terminator) {
        terminator = where + strlen (extension);
        if ((where == start || *(where - 1) == ' ') && (*terminator == 0 || *terminator == ' '))
            return true;
    }

    return false;
}

#ifndef __GLXextFuncPtr
typedef void (*__GLXextFuncPtr)(void);
#endif
#ifndef glXGetProcAddressARB
extern __GLXextFuncPtr glXGetProcAddressARB (const GLubyte *);
#endif

void *qglGetProcAddress (const char *proc) {
    return glXGetProcAddressARB ((byte *) proc);
}

void InitGLXExtensions (Display * dpy, int scrnum) {
    int           i;

    for (i = 0; i < NUM_EXTENSIONS; i++) {
        if (CheckGLXExtension (dpy, scrnum, extensionlist[i].extension)) {
            Con_DPrintf ("  %s\n", extensionlist[i].extension);
            *extensionlist[i].found = true;
        } else {
            *extensionlist[i].found = false;
        }
    }

    qglXGetVideoSync = qglGetProcAddress ("glXGetVideoSyncSGI");
    qglXSwapInterval = qglGetProcAddress ("glXSwapIntervalSGI");
    qglXWaitVideoSync = qglGetProcAddress ("glXWaitVideoSyncSGI");

    if (gl_ext.swap_control) {
        if (!qglXSwapInterval)
            gl_ext.swap_control = false;
    }
    if (gl_ext.video_sync) {
        if (!(qglXGetVideoSync && qglXWaitVideoSync))
            gl_ext.video_sync = false;
    }
}

static pthread_mutex_t mutex1 = PTHREAD_MUTEX_INITIALIZER;
static pthread_mutex_t mutex2 = PTHREAD_MUTEX_INITIALIZER;

inline void VID_ThreadLock (void) {
    if (pthread_mutex_lock (&mutex1))
        Sys_Error ("could not lock the resource");
}

inline void VID_ThreadUnlock (void) {
    if (pthread_mutex_unlock (&mutex1))
        Sys_Error ("could not unlock the resource");
}

inline void VID_ThreadLock2 (void) {
    if (pthread_mutex_lock (&mutex2))
        Sys_Error ("could not lock the resource");
}

inline void VID_ThreadUnlock2 (void) {
    if (pthread_mutex_unlock (&mutex2))
        Sys_Error ("could not unlock the resource");
}

void *VID_ThreadCallback (void (*callback) (void)) {
    GLXContext    threadctx;

    threadctx = glXCreateContext (dpy, visinfo, ctx, True);
    glXMakeCurrent (dpy, win, threadctx);

    Con_DPrintf ("thread started\n");
    callback ();
    Con_DPrintf ("thread ended\n");

    glXDestroyContext (dpy, threadctx);
    pthread_exit (NULL);
}

void VID_ThreadStart (void (*callback) (void), int count) {
    pthread_t     thread;
    pthread_attr_t attr;
    struct sched_param param;
    int           i, ret;

    pthread_attr_init (&attr);
    pthread_mutex_init (&mutex1, NULL);
    pthread_mutex_init (&mutex2, NULL);
//    pthread_attr_setstacksize (&attr, PTHREAD_STACK_MIN);
    pthread_attr_setdetachstate (&attr, PTHREAD_CREATE_DETACHED);
    pthread_attr_setschedpolicy (&attr, SCHED_RR);
    pthread_attr_getschedparam (&attr, &param);
    param.sched_priority = sched_get_priority_min (SCHED_RR);
    pthread_attr_setschedparam (&attr, &param);

    for (i = 0; i < count; i++) {
        ret = pthread_create (&thread, &attr, (void *) VID_ThreadCallback, callback);
        if (ret)
            Con_Warnf ("Failed thread creation (%d): %s\n", ret, strerror (ret));
    }
}

// lxndr: still required ?
void D_BeginDirectRect (int x, int y, byte * pbitmap, int width, int height) {
}
void D_EndDirectRect (int x, int y, int width, int height) {
}

qboolean      config_notify = false;
int           config_notify_width;
int           config_notify_height;

static void GetEvent (void) {
    XEvent        event, next;
    XNextEvent (dpy, &event);

    switch (event.type) {
        case KeyPress:
        case KeyRelease:
            if (event.type == KeyRelease && XEventsQueued (dpy, QueuedAfterReading)) {
                XPeekEvent (dpy, &next);
                if (next.type == KeyPress && next.xkey.keycode == event.xkey.keycode && next.xkey.time == event.xkey.time)
                    break;
            }
            Key_Event (X_LateKey (&event.xkey), event.type == KeyPress);
            break;

        case MotionNotify:
           if (_windowed_mouse.value && (vid.fullscreen || key_dest != key_menu)) {
#ifdef HAVE_DGA
                if (dgamouse) {
                    mouse_x += event.xmotion.x_root;
                    mouse_y += event.xmotion.y_root;
                } else
#endif
                {
                    mouse_x = ((int) event.xmotion.x - (int) (vid.width / 2));
                    mouse_y = ((int) event.xmotion.y - (int) (vid.height / 2));
                }

                // move the mouse to the window center again
                XSelectInput (dpy, win, COMMON_MASK & ~MOTION_MASK);
                XWarpPointer (dpy, None, win, 0, 0, 0, 0, (vid.width / 2), (vid.height / 2));
                XSelectInput (dpy, win, COMMON_MASK);
            }
            break;

        case ButtonPress:
        case ButtonRelease:
            switch (event.xbutton.button) {
                case 1:
                    Key_Event (K_MOUSE1, event.type == ButtonPress);
                    break;
                case 2:
                    Key_Event (K_MOUSE3, event.type == ButtonPress);
                    break;

                case 3:
                    Key_Event (K_MOUSE2, event.type == ButtonPress);
                    break;

                case 4:
                    Key_Event (K_MWHEELUP, event.type == ButtonPress);
                    break;

                case 5:
                    Key_Event (K_MWHEELDOWN, event.type == ButtonPress);
                    break;
            }
            break;

        case ConfigureNotify:
            config_notify_width = event.xconfigure.width;
            config_notify_height = event.xconfigure.height;
            config_notify = true;
//            vid.nodisplay = true;
            break;

        case MappingNotify:
            XSelectInput (dpy, win, COMMON_MASK & ~MOTION_MASK);
            XWarpPointer (dpy, None, win, 0, 0, 0, 0, (vid.width / 2), (vid.height / 2));
            XSelectInput (dpy, win, COMMON_MASK);
            break;

/*        case Expose:
            VID_GrabInputs ();
            break;*/

        default:
            Con_DPrintf ("Got a %s event\n", X_GetEventName (event.type));
#ifdef DEBUG
            X_DumpEvent (event);
#endif
            break;
    }

    if (old_windowed_mouse != _windowed_mouse.value) {
        old_windowed_mouse = _windowed_mouse.value;
        if (!_windowed_mouse.value)
            VID_UngrabInputs ();
        else
            VID_GrabInputs ();
    } else if (!vid.fullscreen) {
        if (key_dest == key_menu)
            VID_UngrabInputs ();
    }
}

void signal_handler (int sig) {
    printf ("Received signal %d, exiting...\n", sig);
#ifdef DEBUG
    Com_DumpMe ();
#endif
    Sys_Quit ();
    exit (0);
}

void InitSig (void) {
    signal (SIGHUP, signal_handler);
    signal (SIGINT, signal_handler);
    signal (SIGQUIT, signal_handler);
    signal (SIGILL, signal_handler);
    signal (SIGTRAP, signal_handler);
    signal (SIGIOT, signal_handler);
    signal (SIGBUS, signal_handler);
    signal (SIGFPE, signal_handler);
    signal (SIGSEGV, signal_handler);
    signal (SIGTERM, signal_handler);
}

void VID_ShiftPalette (unsigned char *p) {
}

/*
 * GL_BeginRendering
 */
void GL_BeginRendering (int *x, int *y, int *width, int *height) {
    *x = *y = 0;
    *width = vid.width;
    *height = vid.height;
}

/*
 * GL_EndRendering
 */
void GL_EndRendering (void) {
    unsigned int  sync;

#ifdef HAVE_VMODE
    VID_SetGamma (vid.fullscreen);
#endif

    if (vid_vsync.value == 1 && gl_ext.video_sync) {
        qglXGetVideoSync (&sync);
        qglXWaitVideoSync (2, (sync + 1) % 2, &sync);
    }

    glXSwapBuffers (dpy, win);

    if (config_notify) {
        config_notify = false;
        vid.width = config_notify_width;
        vid.height = config_notify_height;
        vid.conwidth = vid.width;
        vid.conheight = vid.height;
        vid.nodisplay = false;
        vid.recalc_refdef = true;
        Con_CheckResize ();
    }
}

void VID_Shutdown (void) {
    if (!dpy)
        return;

    GL_Shutdown ();
    if (ctx)
        glXDestroyContext (dpy, ctx);

    VID_UngrabInputs ();
#ifdef HAVE_MISC
    VID_Manage3rdButton (false);
#endif
    VID_RestoreScreenSaver ();
    if (win)
        XDestroyWindow (dpy, win);
#ifdef HAVE_VMODE
    VID_RestoreHWGamma ();
    VID_ShutdownVMode ();
#endif
    XCloseDisplay (dpy);
}

void VID_Init (unsigned char *palette) {
    int           i;
    XSetWindowAttributes attr;
    unsigned long mask;
    Window        root;
    qboolean      setFullscreen;

    Cvar_Register (&vid_vsync);

    vid.maxwarpwidth = WARP_WIDTH;
    vid.maxwarpheight = WARP_HEIGHT;
    vid.colormap = host_colormap;
    if (!game.free)
        vid.fullbright = 256 - LittleLong (*((int *) vid.colormap + 2048)); // lxndr: not used

    if (!XInitThreads ())
        Sys_Error ("couln't initialize multithreading");

    if (!(dpy = XOpenDisplay (NULL)))
        Sys_Error ("couldn't open the X display");

    Con_DPrintf ("X Server: %s, release %d\n", ServerVendor (dpy), VendorRelease (dpy));
    Con_DPrintf ("X Protocol: version %d, revision %d\n", ProtocolVersion (dpy), ProtocolRevision (dpy));

    scrnum = DefaultScreen (dpy);
    if (!(visinfo = glXChooseVisual (dpy, scrnum, glxattr)))
        Sys_Error ("couldn't get a visual with following: RGBA, Double Buffer, Depth and Stencil Buffers");
    gl_have_stencil = true;            // lxndr: still here, stencil ok

    root = RootWindow (dpy, scrnum);

    VID_InitCommon ();

    setFullscreen = VID_InitWindowSize ();

    //    vid.aspect = ((float) vid.height / (float) vid.width) * (320.0 / 240.0); // lxndr: not used in gl, but should
    vid.numpages = 2;

    if ((i = Com_CheckParm ("-conwidth")) && Com_IsParmData (i + 1))
        vid.conwidth = Q_atoi (com_argv[i + 1]);
    else
        vid.conwidth = vid.width;
    vid.conwidth &= 0xfff8;            // make it a multiple of eight
    if ((i = Com_CheckParm ("-conheight")) && Com_IsParmData (i + 1))
        vid.conheight = Q_atoi (com_argv[i + 1]);
    else
        vid.conheight = vid.conwidth * vid.height / vid.width;

    VID_DisableScreenSaver ();
#ifdef HAVE_MISC
    VID_Manage3rdButton (true);
#endif

    // window attributes
    attr.background_pixel = 0;
    attr.border_pixel = 0;
    attr.colormap = XCreateColormap (dpy, root, visinfo->visual, AllocNone);
    attr.event_mask = COMMON_MASK;
    mask = CWBackPixel | CWBorderPixel | CWColormap | CWEventMask;

#ifdef HAVE_VMODE
    VID_InitVMode (setFullscreen, &attr, &mask);
#endif

    win = XCreateWindow (dpy, root, 0, 0, vid.width, vid.height, 0, visinfo->depth, InputOutput, visinfo->visual, mask, &attr);
    XDefineCursor (dpy, win, X_CreateNullCursor ());
    XStoreName (dpy, win, va ("%s %s", ENGINE_NAME, VersionString ()));
    XMapWindow (dpy, win);
    XFlush (dpy);

    ctx = glXCreateContext (dpy, visinfo, NULL, True);
    if (!ctx)
        Sys_Error ("glx module might not be currently loaded, please check your X server logs");
    glXMakeCurrent (dpy, win, ctx);

    InitSig ();                        // trap evil signals
    GL_Init ();
    InitGLXExtensions (dpy, scrnum);

    if (!game.free) {
        Check_Gamma (palette);
        VID_SetPalette (palette);
    }

#ifdef HAVE_VMODE
    VID_InitHWGamma ();
    VID_SetResolution (setFullscreen, vid.width, vid.height);
#endif

    VID_InitEnd ();
}

void Sys_SendKeyEvents (void) {
    if (dpy) {
        while (XPending (dpy))
            GetEvent ();
    }
}

void IN_Init (void) {
}

void IN_Shutdown (void) {
}

void IN_Commands (void) {
}

void IN_MouseMove (usercmd_t * cmd) {
    float         tx, ty;
    int           i;
    float         ema_x, ema_y, a;

    tx = mouse_x;
    ty = mouse_y;

    if (m_filter.value) {
        mouse_x = (tx + old_mouse_x) * 0.5;
        mouse_y = (ty + old_mouse_y) * 0.5;
    }

    old_mouse_x = tx;
    old_mouse_y = ty;

    ema_x = ema_y = 0;
    a = host_frametime * 5;
    for (i = MOUSE_HIST_SIZE - 1; i > 0; i--) {
        in_mousehist_x[i] = in_mousehist_x[i - 1];
        in_mousehist_y[i] = in_mousehist_y[i - 1];
        ema_x = a * in_mousehist_x[i] + (1 - a) * ema_x;
        ema_y = a * in_mousehist_y[i] + (1 - a) * ema_y;
    }
    in_mousehist_x[0] = tx;
    in_mousehist_y[0] = ty;
    in_mousemean_x = a * in_mousehist_x[0] + (1 - a) * ema_x;
    in_mousemean_y = a * in_mousehist_y[0] + (1 - a) * ema_y;

//Con_Printf ("%f %f %f %f %f\n", in_mousemean_x, in_mousemean_y, a, tx, ty);
    mouse_x *= sensitivity.value;
    mouse_y *= sensitivity.value;

    // add mouse X/Y movement to cmd
    if ((in_strafe.state & 1) || (lookstrafe.value && mlook_active))
        cmd->sidemove += m_side.value * mouse_x;
    else
        cl.viewangles[YAW] -= m_yaw.value * mouse_x;

    if (mlook_active)
        V_StopPitchDrift ();

    if (mlook_active && !(in_strafe.state & 1)) {
        cl.viewangles[PITCH] += m_pitch.value * mouse_y;
    } else {
        if ((in_strafe.state & 1) && noclip_anglehack)
            cmd->upmove -= m_forward.value * mouse_y;
        else
            cmd->forwardmove -= m_forward.value * mouse_y;
    }
    mouse_x = mouse_y = 0.0;
}

void IN_Move (usercmd_t * cmd) {
    IN_MouseMove (cmd);
}

#ifdef DEVEL
// lxndr: from bzflag
void VisualInfo (void) {
    int           n_fbc;
    GLXFBConfig  *fbc;
    int           value, ret, i;

    fbc = glXGetFBConfigs (dpy, DefaultScreen (dpy), &n_fbc);

    if (fbc) {
        if (1) {
            /* print table header */
            Sys_Printf (" +-----+-------------------------+-----------------+----------+-------------+-------+------+\n");
            Sys_Printf (" |     |        visual           |      color      | ax dp st |    accum    |   ms  |  cav |\n");
            Sys_Printf (" |  id | tp xr cl fm db st lv xp |  sz  r  g  b  a | bf th cl | r  g  b  a  | ns  b |  eat |\n");
            Sys_Printf (" +-----+-------------------------+-----------------+----------+-------------+-------+------+\n");
            /* loop through all the fbcs */
            for (i = 0; i < n_fbc; i++) {
                /* print out the information for this fbc */
                /* visual id */
                ret = glXGetFBConfigAttrib (dpy, fbc[i], GLX_FBCONFIG_ID, &value);
                if (ret != Success) {
                    Sys_Printf ("|  ?  |");
                } else {
                    Sys_Printf (" |% 4d | ", value);
                }
                /* visual type */
                ret = glXGetFBConfigAttrib (dpy, fbc[i], GLX_DRAWABLE_TYPE, &value);
                if (ret != Success) {
                    Sys_Printf (" ? ");
                } else {
                    if (value & GLX_WINDOW_BIT) {
                        if (value & GLX_PBUFFER_BIT) {
                            Sys_Printf ("wp ");
                        } else {
                            Sys_Printf ("wn ");
                        }
                    } else {
                        if (value & GLX_PBUFFER_BIT) {
                            Sys_Printf ("pb ");
                        } else if (value & GLX_PIXMAP_BIT) {
                            Sys_Printf ("pm ");
                        } else {
                            Sys_Printf (" ? ");
                        }
                    }
                }
                /* x renderable */
                ret = glXGetFBConfigAttrib (dpy, fbc[i], GLX_X_RENDERABLE, &value);
                if (ret != Success) {
                    Sys_Printf (" ? ");
                } else {
                    Sys_Printf (value ? " y " : " n ");
                }
                /* class */
                ret = glXGetFBConfigAttrib (dpy, fbc[i], GLX_X_VISUAL_TYPE, &value);
                if (ret != Success) {
                    Sys_Printf (" ? ");
                } else {
                    if (GLX_TRUE_COLOR == value)
                        Sys_Printf ("tc ");
                    else if (GLX_DIRECT_COLOR == value)
                        Sys_Printf ("dc ");
                    else if (GLX_PSEUDO_COLOR == value)
                        Sys_Printf ("pc ");
                    else if (GLX_STATIC_COLOR == value)
                        Sys_Printf ("sc ");
                    else if (GLX_GRAY_SCALE == value)
                        Sys_Printf ("gs ");
                    else if (GLX_STATIC_GRAY == value)
                        Sys_Printf ("sg ");
                    else if (GLX_X_VISUAL_TYPE == value)
                        Sys_Printf (" . ");
                    else
                        Sys_Printf (" ? ");
                }
                /* format */
                ret = glXGetFBConfigAttrib (dpy, fbc[i], GLX_RENDER_TYPE, &value);
                Sys_Printf (" ? ");
/*                if (ret != Success) {
                    Sys_Printf (" ? ");
                } else {
                    if (GLXEW_NV_float_buffer) {
                        int           ret2, value2;
                        ret2 = glXGetFBConfigAttrib (dpy, fbc[i], GLX_FLOAT_COMPONENTS_NV, &value2);
                        if (Success == ret2 && GL_TRUE == value2) {
                            Sys_Printf (" f ");
                        } else if (value & GLX_RGBA_BIT)
                            Sys_Printf (" i ");
                        else if (value & GLX_COLOR_INDEX_BIT)
                            Sys_Printf (" c ");
                        else
                            Sys_Printf (" ? ");
                    } else {
                        if (value & GLX_RGBA_FLOAT_ATI_BIT)
                            Sys_Printf (" f ");
                        else if (value & GLX_RGBA_BIT)
                            Sys_Printf (" i ");
                        else if (value & GLX_COLOR_INDEX_BIT)
                            Sys_Printf (" c ");
                        else
                            Sys_Printf (" ? ");
                    }
                }*/
                /* double buffer */
                ret = glXGetFBConfigAttrib (dpy, fbc[i], GLX_DOUBLEBUFFER, &value);
                Sys_Printf (" %c ", Success != ret ? '?' : (value ? 'y' : '.'));
                /* stereo */
                ret = glXGetFBConfigAttrib (dpy, fbc[i], GLX_STEREO, &value);
                Sys_Printf (" %c ", Success != ret ? '?' : (value ? 'y' : '.'));
                /* level */
                ret = glXGetFBConfigAttrib (dpy, fbc[i], GLX_LEVEL, &value);
                if (Success != ret) {
                    Sys_Printf (" ? ");
                } else {
                    Sys_Printf ("%2d ", value);
                }
                /* transparency */
                ret = glXGetFBConfigAttrib (dpy, fbc[i], GLX_TRANSPARENT_TYPE, &value);
                if (Success != ret) {
                    Sys_Printf (" ? | ");
                } else {
                    if (GLX_TRANSPARENT_RGB == value)
                        Sys_Printf (" r | ");
                    else if (GLX_TRANSPARENT_INDEX == value)
                        Sys_Printf (" i | ");
                    else if (GLX_NONE == value)
                        Sys_Printf (" . | ");
                    else
                        Sys_Printf (" ? | ");
                }
                /* color size */
                ret = glXGetFBConfigAttrib (dpy, fbc[i], GLX_BUFFER_SIZE, &value);
                if (Success != ret) {
                    Sys_Printf ("  ? ");
                } else {
                    if (value)
                        Sys_Printf ("%3d ", value);
                    else
                        Sys_Printf ("  . ");
                }
                /* red size */
                ret = glXGetFBConfigAttrib (dpy, fbc[i], GLX_RED_SIZE, &value);
                if (Success != ret) {
                    Sys_Printf (" ? ");
                } else {
                    if (value)
                        Sys_Printf ("%2d ", value);
                    else
                        Sys_Printf (" . ");
                }
                /* green size */
                ret = glXGetFBConfigAttrib (dpy, fbc[i], GLX_GREEN_SIZE, &value);
                if (Success != ret) {
                    Sys_Printf (" ? ");
                } else {
                    if (value)
                        Sys_Printf ("%2d ", value);
                    else
                        Sys_Printf (" . ");
                }
                /* blue size */
                ret = glXGetFBConfigAttrib (dpy, fbc[i], GLX_BLUE_SIZE, &value);
                if (Success != ret) {
                    Sys_Printf (" ? ");
                } else {
                    if (value)
                        Sys_Printf ("%2d ", value);
                    else
                        Sys_Printf (" . ");
                }
                /* alpha size */
                ret = glXGetFBConfigAttrib (dpy, fbc[i], GLX_ALPHA_SIZE, &value);
                if (Success != ret) {
                    Sys_Printf (" ? | ");
                } else {
                    if (value)
                        Sys_Printf ("%2d | ", value);
                    else
                        Sys_Printf (" . | ");
                }
                /* aux buffers */
                ret = glXGetFBConfigAttrib (dpy, fbc[i], GLX_AUX_BUFFERS, &value);
                if (Success != ret) {
                    Sys_Printf (" ? ");
                } else {
                    if (value)
                        Sys_Printf ("%2d ", value);
                    else
                        Sys_Printf (" . ");
                }
                /* depth size */
                ret = glXGetFBConfigAttrib (dpy, fbc[i], GLX_DEPTH_SIZE, &value);
                if (Success != ret) {
                    Sys_Printf (" ? ");
                } else {
                    if (value)
                        Sys_Printf ("%2d ", value);
                    else
                        Sys_Printf (" . ");
                }
                /* stencil size */
                ret = glXGetFBConfigAttrib (dpy, fbc[i], GLX_STENCIL_SIZE, &value);
                if (Success != ret) {
                    Sys_Printf (" ? | ");
                } else {
                    if (value)
                        Sys_Printf ("%2d | ", value);
                    else
                        Sys_Printf (" . | ");
                }
                /* accum red size */
                ret = glXGetFBConfigAttrib (dpy, fbc[i], GLX_ACCUM_RED_SIZE, &value);
                if (Success != ret) {
                    Sys_Printf (" ? ");
                } else {
                    if (value)
                        Sys_Printf ("%2d ", value);
                    else
                        Sys_Printf (" . ");
                }
                /* accum green size */
                ret = glXGetFBConfigAttrib (dpy, fbc[i], GLX_ACCUM_GREEN_SIZE, &value);
                if (Success != ret) {
                    Sys_Printf (" ? ");
                } else {
                    if (value)
                        Sys_Printf ("%2d ", value);
                    else
                        Sys_Printf (" . ");
                }
                /* accum blue size */
                ret = glXGetFBConfigAttrib (dpy, fbc[i], GLX_ACCUM_BLUE_SIZE, &value);
                if (Success != ret) {
                    Sys_Printf (" ? ");
                } else {
                    if (value)
                        Sys_Printf ("%2d ", value);
                    else
                        Sys_Printf (" . ");
                }
                /* accum alpha size */
                ret = glXGetFBConfigAttrib (dpy, fbc[i], GLX_ACCUM_ALPHA_SIZE, &value);
                if (Success != ret) {
                    Sys_Printf (" ? | ");
                } else {
                    if (value)
                        Sys_Printf ("%2d | ", value);
                    else
                        Sys_Printf (" . | ");
                }
                /* multisample */
                ret = glXGetFBConfigAttrib (dpy, fbc[i], GLX_SAMPLES, &value);
                if (Success != ret) {
                    Sys_Printf (" ? ");
                } else {
                    Sys_Printf ("%2d ", value);
                }
                ret = glXGetFBConfigAttrib (dpy, fbc[i], GLX_SAMPLE_BUFFERS, &value);
                if (Success != ret) {
                    Sys_Printf (" ? | ");
                } else {
                    Sys_Printf ("%2d | ", value);
                }
                /* caveat */
                ret = glXGetFBConfigAttrib (dpy, fbc[i], GLX_CONFIG_CAVEAT, &value);
                if (Success != ret) {
                    Sys_Printf ("???? |");
                } else {
                    if (GLX_NONE == value)
                        Sys_Printf ("none |\n");
                    else if (GLX_SLOW_CONFIG == value)
                        Sys_Printf ("slow |\n");
                    else if (GLX_NON_CONFORMANT_CONFIG == value)
                        Sys_Printf ("ncft |\n");
                    else
                        Sys_Printf ("???? |\n");
                }
            }
            /* print table footer */
            Sys_Printf (" +-----+-------------------------+-----------------+----------+-------------+-------+------+\n");
            Sys_Printf (" |  id | tp xr cl fm db st lv xp |  sz  r  g  b  a | bf th cl | r  g  b  a  | ns  b |  eat |\n");
            Sys_Printf (" |     |        visual           |      color      | ax dp st |    accum    |   ms  |  cav |\n");
            Sys_Printf (" +-----+-------------------------+-----------------+----------+-------------+-------+------+\n");
        }
    }
}
#endif
