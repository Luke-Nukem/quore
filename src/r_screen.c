/*
 * Copyright (C) 1996-1997 Id Software, Inc.
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation; either version 2 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.
 *
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 */
// r_screen.c -- master for refresh, status bar, console, chat, notify, etc

#include "quakedef.h"
#include "r_local.h"
#ifdef _WIN32
#  include "movie.h"
#endif

int           hudwidth, hudheight;

// only the refresh window will be updated unless these variables are flagged
int           scr_copytop;
int           scr_copyeverything;

float         scr_con_current;
float         scr_conlines;            // lines of console to display

float         oldscreensize, oldfov, oldsbar;

double        oldzoomstate;

cvar_t        scr_viewsize = { "viewsize", "110" };
cvar_t        scr_fov = { "fov", "90", CVAR_NONE };     // 10 - 170
cvar_t        scr_consize = { "scr_consize", "0.2" };
cvar_t        scr_conspeed = { "scr_conspeed", "1000" };
cvar_t        scr_conswitch = { "scr_conswitch", "1" };
cvar_t        scr_centerlog = { "scr_centerlog", "1" };
cvar_t        scr_centertime = { "scr_centertime", "2" };
cvar_t        scr_printspeed = { "scr_printspeed", "8" };
cvar_t        scr_shotauto = { "scr_shotauto", "1" };
cvar_t        scr_shotformat = { "scr_shotformat", "png" };

extern cvar_t crosshair;

qboolean      scr_initialized;         // ready to draw

int           scr_fullupdate;

int           clearconsole;
int           clearnotify;

viddef_t      vid;                     // global video state

vrect_t       scr_vrect;

char         *scr_notifystring;

qboolean      scr_drawdialog;
qboolean      scr_drawloading;
qboolean      scr_drawnothing;
qboolean      scr_drawtransition;

qboolean      scr_disabled_for_loading;
float         scr_disabled_time;

qboolean      scr_skipupdate;

qboolean      block_drawing;

void          SCR_ScreenShot_f (void);

/*
 *
 *
 * CENTER PRINTING
 *
 *
 */

char          scr_centerstring[1024];
float         scr_centertime_start;    // for slow victory printing
float         scr_centertime_off;
int           scr_center_lines;
int           scr_erase_lines;
int           scr_erase_center;

/*
 * SCR_CenterPrint
 *
 * Called for important messages that should stay in the center of the screen
 * for a few moments
 */
void SCR_CenterPrint (char *str) {
    int           l;
    char          out[48];
    char         *p = str;

    if (!str[0])
        return;

    strncpy (scr_centerstring, str, sizeof (scr_centerstring) - 1);
    scr_centertime_off = scr_centertime.value;
    scr_centertime_start = cl.time;

    while (*p) {
        for (l = 0; l < 40; l++) {
            if (p[l] == '\n' || !p[l])
                break;
            out[l] = p[l];
        }
        p += l;
        if (*p == '\n')
            p++;
        while (l < 40)
            out[l++] = ' ';
        out[l] = '\0';
        if (scr_centerlog.value)
            Con_Printf ("\x1d\x1e %s \x1e\x1f\n", out);
    }
    if (scr_centerlog.value)
        Con_Printf ("\n");

    // count the number of lines for centering
    scr_center_lines = 1;
    while (*str) {
        if (*str == '\n')
            scr_center_lines++;
        str++;
    }
}

void SCR_EraseCenterString (void) {
    int           y;

    if (scr_erase_center++ > vid.numpages) {
        scr_erase_lines = 0;
        return;
    }

    if (scr_center_lines <= 4)
        y = vid.height * 0.35;
    else
        y = 48;

    scr_copytop = 1;
    Draw_TileClear (0, y, vid.width, 8 * scr_erase_lines);
}

void SCR_DrawCenterString (void) {
    char         *start;
    int           l, j, x, y, remaining;

    // the finale prints the characters one at a time
    if (cl.intermission)
        remaining = scr_printspeed.value * (cl.time - scr_centertime_start);
    else
        remaining = 9999;

    scr_erase_center = 0;
    start = scr_centerstring;

    if (scr_center_lines <= 4)
        y = vid.height * 0.35;
    else
        y = 48;

    do {
        // scan the width of the line
        for (l = 0; l < 40; l++)
            if (start[l] == '\n' || !start[l])
                break;
        x = (vid.width - l * 8) / 2;
        for (j = 0; j < l; j++, x += 8) {
            Draw_Character (x, y, start[j]);
            if (!remaining--)
                return;
        }

        y += 8;

        while (*start && *start != '\n')
            start++;

        if (!*start)
            break;
        start++;                       // skip the \n
    }
    while (1);
}

void SCR_CheckDrawCenterString (void) {
    scr_copytop = 1;
    if (scr_center_lines > scr_erase_lines)
        scr_erase_lines = scr_center_lines;

    scr_centertime_off -= host_frametime;

    if (scr_centertime_off <= 0 && !cl.intermission)
        return;

    if (key_dest != key_game)
        return;

    SCR_DrawCenterString ();
}

// 

/*
 * SCR_CalcRefdef
 *
 * Must be called whenever vid changes Internal use only
 */
static void SCR_CalcRefdef (void) {
    vrect_t       vrect;
    float         size;

    scr_fullupdate = 0;                // force a background redraw
    vid.recalc_refdef = 0;

    // force the status bar to redraw
    Sbar_Changed ();

    // 

    // bound viewsize
    if (scr_viewsize.value < 20)
        Cvar_SetValue (&scr_viewsize, 20);
    if (scr_viewsize.value > 120)
        Cvar_SetValue (&scr_viewsize, 120);

    // bound field of view
    if (scr_fov.value < 10)
        Cvar_SetValue (&scr_fov, 10);
    if (scr_fov.value > 170)
        Cvar_SetValue (&scr_fov, 170);

    V_CalcZoom ();                     // lxndr: new method

    // intermission is always full screen
    if (cl.intermission)
        size = 120;
    else
        size = scr_viewsize.value;

    if (size >= 120)
        sb_lines = 0;                  // no status bar at all
    else if (size >= 110)
        sb_lines = 24;                 // no inventory
    else
        sb_lines = 24 + 16 + 8;

    // these calculations mirror those in R_Init() for r_refdef, but take no account of water warping
    vrect.x = 0;
    vrect.y = 0;
    vrect.width = vid.width;
    vrect.height = vid.height;

    R_SetVrect (&vrect, &scr_vrect, sb_lines);

    // guard against going from one mode to another that's less than half the vertical resolution
    if (scr_con_current > vid.height)
        scr_con_current = vid.height;

    // notify the refresh of the change
    R_ViewChanged (&vrect, sb_lines, vid.aspect);
}

/*
 * SCR_SizeUp_f
 *
 * Keybinding command
 */
void SCR_SizeUp_f (void) {
    Cvar_SetValue (&scr_viewsize, scr_viewsize.value + 10);
    vid.recalc_refdef = 1;
}

/*
 * SCR_SizeDown_f
 *
 * Keybinding command
 */
void SCR_SizeDown_f (void) {
    Cvar_SetValue (&scr_viewsize, scr_viewsize.value - 10);
    vid.recalc_refdef = 1;
}

// 

/*
 * SCR_SetupConsole
 */
void SCR_SetupConsole (void) {
    // decide on the height of the console
    con_forcedup = !cl.worldmodel || cls.signon != SIGNONS;

    if (host_initialized)                // lxndr: quick hack to prevent console clearing
        Con_CheckResize ();

    conwidth = vid.width;
    conheight = vid.height;
    if (con_forcedup) {
        scr_conlines = conheight;      // full screen
        scr_con_current = scr_conlines;
    } else if (scr_con_current > conheight) {
        scr_con_current = 0;
    } else if (key_dest == key_console && !con_message) {
        if (scr_conswitch.value && conwidth > 480)
            scr_conlines = conheight;
        else
            scr_conlines = conheight * scr_consize.value;
        if (scr_conlines < 20)         // lxndr: was 30
            scr_conlines = 20;         // lxndr: was 30
        if (scr_conlines > conheight)
            scr_conlines = conheight;
    } else {
        scr_conlines = 0;              // none visible
    }

    if (scr_conlines < scr_con_current) {
//        scr_con_current -= scr_conspeed.value * (host_frametime + 0.0001);  // lxndr: hostframetime can be equal to zero when playing demo
        scr_con_current -= scr_conspeed.value * (host_frametime + 0.01) * conheight / 320;
        if (scr_conlines > scr_con_current)
            scr_con_current = scr_conlines;

    } else if (scr_conlines > scr_con_current) {
//        scr_con_current += scr_conspeed.value * (host_frametime + 0.0001);        // lxndr: hostframetime can be equal to zero when playing demo
        scr_con_current += scr_conspeed.value * (host_frametime + 0.01) * conheight / 320;
        if (scr_conlines < scr_con_current)
            scr_con_current = scr_conlines;
    }

    if (clearconsole++ < vid.numpages) {
        scr_copytop = 1;
        Draw_TileClear (0, (int) scr_con_current, conwidth, conheight - (int) scr_con_current);
        Sbar_Changed ();
    } else if (clearnotify++ < vid.numpages) {
        scr_copytop = 1;
        Draw_TileClear (0, 0, conwidth, con_clearnotifylines);
    } else {
        con_clearnotifylines = 0;
    }
}

/*
 * SCR_DrawConsole
 */
void SCR_DrawConsole (void) {
    if (scr_con_current) {
        scr_copyeverything = 1;
        Con_DrawConsole (scr_con_current, true);
        clearconsole = 0;
    } else {
        if (key_dest == key_game || key_dest == key_message)
            Con_DrawNotify ();         // only draw notify in game
    }
}

/*
 *
 * SCREEN SHOTS
 *
 *
 */

int SCR_ScreenShot (char *name) {
    qboolean      ok = false;
    char         *ext;

    D_EnableBackBufferAccess ();       // enable direct drawing of console to back buffer

    ext = Com_FileExtension (name);

    if (!Q_strcasecmp (ext, "png"))
        ok = Image_WritePNGPLTE (name, png_compression_level.value, vid.buffer, vid.width, vid.height, vid.rowbytes, current_pal);
    else
        ok = Image_WritePCX (name, vid.buffer, vid.width, vid.height, vid.rowbytes, current_pal);

    D_DisableBackBufferAccess ();      // for adapters that can't stay mapped in for linear writes all the time

    return ok;
}

qboolean SCR_FindScreenShot (char *name) {
    if (Com_FindFile (va ("%s.pcx", name)) || Com_FindFile (va ("%s.png", name)))
        return true;
    return false;
}

void SCR_ScreenShotAuto (void) {
    char          name[MAX_OSPATH];
    qboolean      success;

    if (!scr_drawnothing)
        return;

    Q_snprintfz (name, sizeof (name), "%s-%s-00", ENGINE_FSNAME, cl_mapname.string);
    if (!SCR_FindScreenShot (name)) {
        success = SCR_ScreenShot (va ("%s/%s.%s", com_screenshotdir, name, scr_shotformat.string));
        Con_Printf ("%s %s.%s\n", success ? "Wrote" : "Couldn't write", name, scr_shotformat.string);
    }

    scr_drawnothing = false;
}

/*
 * SCR_ScreenShot_f
 */
void SCR_ScreenShot_f (void) {
    int           i, success;
    char          name[MAX_OSPATH], ext[4];

    if (Cmd_Argc () == 2) {
        Q_strncpyz (name, Cmd_Argv (1), sizeof (name));
    } else if (Cmd_Argc () == 1) {
        // find a file name to save it to
        if (!Q_strcasecmp (scr_shotformat.string, "jpg") || !Q_strcasecmp (scr_shotformat.string, "jpeg"))
            Q_strncpyz (ext, "jpg", 4);
        else if (!Q_strcasecmp (scr_shotformat.string, "png"))
            Q_strncpyz (ext, "png", 4);
        else
            Q_strncpyz (ext, "pcx", 4);

        for (i = 0; i < 99; i++) {
            if (cl.worldmodel)
                Q_snprintfz (name, sizeof (name), "%s-%s-%02i.%s", ENGINE_FSNAME, cl_mapname.string, i, ext);
            else
                Q_snprintfz (name, sizeof (name), "%s-%02i.%s", ENGINE_FSNAME, i, ext);
            if (Sys_FileTime (va ("%s/%s", com_screenshotdir, name)) == -1)
                break;                 // file doesn't exist
        }

        if (i == 100) {
            Con_Printf ("ERROR: Cannot create more screenshots\n");
            return;
        }
    } else {
        Con_Printf ("Usage: %s [<filename>]", Cmd_Argv (0));
        return;
    }

    success = SCR_ScreenShot (va ("%s/%s", com_screenshotdir, name));
    Con_Printf ("%s %s\n", success ? "Wrote" : "Couldn't write", name);
}

// 

void SCR_DrawNotifyString (void) {
    char         *start;
    int           l, j, x, y;

    scr_copyeverything = true;

    Draw_FadeScreen ();

    start = scr_notifystring;

    y = vid.height * 0.35;

    do {
        // scan the width of the line
        for (l = 0; l < 40; l++)
            if (start[l] == '\n' || !start[l])
                break;
        x = (vid.width - l * 8) / 2;
        for (j = 0; j < l; j++, x += 8)
            Draw_Character (x, y, start[j]);

        y += 8;

        while (*start && *start != '\n')
            start++;

        if (!*start)
            break;
        start++;                       // skip the \n
    }
    while (1);
}

// 

void SCR_BuildGammaTable (float g, float c) {
    int           i, inf;

    g = bound (0.3, g, 3);
    c = bound (1, c, 3);

    if (g == 1 && c == 1) {
        for (i = 0; i < 256; i++)
            gammatable[i] = i;
        return;
    }

    for (i = 0; i < 256; i++) {
        inf = 255 * pow ((i + 0.5) / 255.5 * c, g) + 0.5;
        gammatable[i] = bound (0, inf, 255);
    }
}

qboolean SCR_CheckGamma (void) {
    static float  old_gamma, old_contrast;

    if (v_gamma.value == old_gamma && v_contrast.value == old_contrast)
        return false;

    old_gamma = v_gamma.value;
    old_contrast = v_contrast.value;

    SCR_BuildGammaTable (v_gamma.value, v_contrast.value);
    vid.recalc_refdef = 1;             // force a surface cache flush

    return true;
}

void SCR_UpdatePalette (void) {
    int           i, j, r, g, b;
    qboolean      new, force;
    byte         *basepal, *newpal;
    static cshift_t prev_cshifts[NUM_CSHIFTS];
    extern cshift_t cshift_empty;

    if (cls.state != ca_connected) {
        cl.cshifts[CSHIFT_CONTENTS] = cshift_empty;
        cl.cshifts[CSHIFT_POWERUP].percent = 0;
    } else {
        V_CalcPowerupCshift ();
    }

    new = false;

    for (i = 0; i < NUM_CSHIFTS; i++) {
        if (cl.cshifts[i].percent != prev_cshifts[i].percent) {
            new = true;
            prev_cshifts[i].percent = cl.cshifts[i].percent;
        }
        for (j = 0; j < 3; j++) {
            if (cl.cshifts[i].destcolor[j] != prev_cshifts[i].destcolor[j]) {
                new = true;
                prev_cshifts[i].destcolor[j] = cl.cshifts[i].destcolor[j];
            }
        }
    }

    // drop the damage value
    cl.cshifts[CSHIFT_DAMAGE].percent -= host_frametime * 150;
    if (cl.cshifts[CSHIFT_DAMAGE].percent <= 0)
        cl.cshifts[CSHIFT_DAMAGE].percent = 0;

    // drop the bonus value
    cl.cshifts[CSHIFT_BONUS].percent -= host_frametime * 100;
    if (cl.cshifts[CSHIFT_BONUS].percent <= 0)
        cl.cshifts[CSHIFT_BONUS].percent = 0;

    force = SCR_CheckGamma ();
    if (!new && !force)
        return;

    basepal = host_basepal;
    newpal = current_pal;              // Tonik: so we can use current_pal for screenshots

    for (i = 0; i < 256; i++) {
        r = basepal[0];
        g = basepal[1];
        b = basepal[2];
        basepal += 3;

        for (j = 0; j < NUM_CSHIFTS; j++) {
            r += (cl.cshifts[j].percent * (cl.cshifts[j].destcolor[0] - r)) >> 8;
            g += (cl.cshifts[j].percent * (cl.cshifts[j].destcolor[1] - g)) >> 8;
            b += (cl.cshifts[j].percent * (cl.cshifts[j].destcolor[2] - b)) >> 8;
        }

        newpal[0] = gammatable[r];
        newpal[1] = gammatable[g];
        newpal[2] = gammatable[b];
        newpal += 3;
    }

    VID_ShiftPalette (current_pal);
}

/*
 * SCR_UpdateScreen
 *
 * This is called every frame, and can also be called explicitly to flush text
 * to the screen.
 *
 * WARNING: be very careful calling this from elsewhere, because the refresh
 * needs almost the entire 256k of stack space!
 */
void SCR_UpdateScreen (void) {
    static float  oldlcd_x;
    vrect_t       vrect;
    extern float  zoom;

    if (cls.state == ca_dedicated)
        return;                        // stdout only
    if (!scr_initialized || !con_initialized)
        return;                        // not initialized yet
    if (vid.nodisplay)
        return;

    hudwidth = vid.width;
    hudheight = vid.height;

    scr_copytop = 0;
    scr_copyeverything = 0;

    // lxndr: cvars should use an OnChange structure
    if (oldfov != scr_fov.value) {
        oldfov = scr_fov.value;
        vid.recalc_refdef = true;
    }
    if (oldlcd_x != lcd_x.value) {
        oldlcd_x = lcd_x.value;
        vid.recalc_refdef = true;
    }
    if (oldscreensize != scr_viewsize.value) {
        oldscreensize = scr_viewsize.value;
        vid.recalc_refdef = true;
    }
    if ((in_zoom.state & 1) != oldzoomstate || zoom > 0 || (zoom > 0 && cl.stats[STAT_HEALTH] <= 0)) {
        oldzoomstate = (in_zoom.state & 1);
        vid.recalc_refdef = true;
    }

    if (vid.recalc_refdef || scr_drawloading || cl.time < 2.5)
        SCR_CalcRefdef (); // something changed, so reorder the screen

    // do 3D refresh drawing, and then update the screen
    D_EnableBackBufferAccess ();       // of all overlay stuff if drawing directly

    // lxndr: FIXME: clear screen everytime
//    if (scr_fullupdate++ < vid.numpages) {      // clear the entire screen
        scr_copyeverything = 1;
        Draw_TileClear (0, 0, vid.width, vid.height);
        Sbar_Changed ();
//    }

    SCR_SetupConsole ();
    SCR_EraseCenterString ();

    D_DisableBackBufferAccess ();      // for adapters that can't stay mapped in for linear writes all the time

    V_RenderView ();

    D_EnableBackBufferAccess ();       // of all overlay stuff if drawing directly

    if (scr_drawnothing) {
    } else if (scr_drawdialog) {
        Sbar_Draw ();
        SCR_DrawNotifyString ();
    } else if (scr_drawloading) {
        Sbar_Draw ();
        SCR_DrawLoading ();
    } else if (cl.intermission == 1 && key_dest == key_game) {
        SCR_DrawIntermissionOverlay ();
    } else if (cl.intermission == 2 && key_dest == key_game) {
        SCR_DrawFinaleOverlay ();
        SCR_CheckDrawCenterString ();
    } else if (cl.intermission == 3 && key_dest == key_game) {
        SCR_CheckDrawCenterString ();
    } else {
        Draw_Crosshair ();
        SCR_DrawRam ();
        SCR_DrawNet ();
        SCR_DrawTurtle ();
        SCR_DrawPause ();
        SCR_CheckDrawCenterString ();
        SCR_DrawClock ();
        SCR_DrawFPS ();
        SCR_DrawSpeed ();
        SCR_DrawStats ();
        SCR_DrawVolume ();
        Sbar_Draw ();
        SCR_DrawConsole ();
        M_Draw ();
    }

    D_DisableBackBufferAccess ();      // for adapters that can't stay mapped in for linear writes all the time

    SCR_UpdatePalette ();

    // update one of three areas
    if (scr_copyeverything) {
        vrect.x = 0;
        vrect.y = 0;
        vrect.width = vid.width;
        vrect.height = vid.height;
        vrect.pnext = 0;

        VID_Update (&vrect);
    } else if (scr_copytop) {
        vrect.x = 0;
        vrect.y = 0;
        vrect.width = vid.width;
        vrect.height = vid.height - sb_lines;
        vrect.pnext = 0;

        VID_Update (&vrect);
    } else {
        vrect.x = scr_vrect.x;
        vrect.y = scr_vrect.y;
        vrect.width = scr_vrect.width;
        vrect.height = scr_vrect.height;
        vrect.pnext = 0;

        VID_Update (&vrect);
    }

#ifdef _WIN32
    Movie_UpdateScreen ();
#endif
}

/*
 * SCR_UpdateWholeScreen
 */
void SCR_UpdateWholeScreen (void) {
    scr_fullupdate = 0;
    SCR_UpdateScreen ();
}

/*
 * SCR_Init
 */
void SCR_Init (void) {
    Cvar_Register (&scr_fov);
    Cvar_Register (&scr_viewsize);
    Cvar_Register (&scr_consize);
    Cvar_Register (&scr_conspeed);
    Cvar_Register (&scr_conswitch);
    Cvar_Register (&scr_centerlog);
    Cvar_Register (&scr_centertime);
    Cvar_Register (&scr_printspeed);
    Cvar_Register (&scr_shotauto);
    Cvar_Register (&scr_shotformat);

    // register our commands
    Cmd_AddCommand ("screenshot", SCR_ScreenShot_f);
    Cmd_AddCommand ("sizeup", SCR_SizeUp_f);
    Cmd_AddCommand ("sizedown", SCR_SizeDown_f);

#ifdef _WIN32
    Movie_Init ();
#endif

    SCR_BuildGammaTable (v_gamma.value, v_contrast.value);
    scr_initialized = true;
}
