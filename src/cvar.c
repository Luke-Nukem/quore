/*
 * Copyright (C) 1996-1997 Id Software, Inc.
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation; either version 2 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.
 *
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 */
// cvar.c -- dynamic variable tracking

#include "quakedef.h"

#ifdef DEBUG
cvar_t        debug_cvar = { "debug_cvar", "" };
#endif

cvar_t       *cvar_vars;
char         *cvar_null_string = "";

qboolean      Cvar_Delete (char *name);

void Cvar_Print (cvar_t *var) {
    if (developer.value)
        Con_Printf ("%20s : %s\tdefault: %s%s\n", var->name, var->string, var->defaultvalue, var->flags & CVAR_LOCK ? ", locked" : "");
    else
        Con_Printf ("%20s : %s%s\n", var->name, var->string, var->flags & CVAR_LOCK ? ", locked" : "");
}

/*
 * Cvar_FindVar
 */
cvar_t       *Cvar_FindVar (char *var_name) {
    cvar_t       *var;

    for (var = cvar_vars; var; var = var->next) {
        if (!Q_strcasecmp (var_name, var->name))
            return var;
    }

    return NULL;
}

/*
 * Cvar_ResetVar
 */
void Cvar_ResetVar (cvar_t * var) {
    if (var && var->defaultvalue && strcmp (var->string, var->defaultvalue))
        Cvar_Set (var, var->defaultvalue);
}

/*
 * Cvar_Reset_f
 */
void Cvar_Reset_f (void) {
    int           c;
    char         *s;
    cvar_t       *var;

    if ((c = Cmd_Argc ()) != 2) {
        Con_Printf ("Usage: %s <variable>\n", Cmd_Argv (0));
        return;
    }

    s = Cmd_Argv (1);

    if ((var = Cvar_FindVar (s)))
        Cvar_ResetVar (var);
    else
        Con_Printf ("%s : No variable with name %s\n", Cmd_Argv (0), s);
}

/*
 * Cvar_IsDefault
 */
qboolean Cvar_IsDefault (cvar_t * var) {
    return !strcmp (var->string, var->defaultvalue);
}

/*
 * Cvar_SetDefault
 */
void Cvar_SetDefault (cvar_t * var, char *value) {
    int           i;

    for (i = strlen (value) - 1; i > 0 && value[i] == '0'; i--)
        value[i] = 0;
    if (value[i] == '.')
        value[i] = 0;

    Z_Free (var->defaultvalue);
    var->defaultvalue = Z_Strdup (value);
    Cvar_Set (var, value);
}

/*
 * Cvar_SetDefaultValue
 */
void Cvar_SetDefaultValue (cvar_t * var, float value) {
    char          val[MAX_VARLEN];

    Q_snprintfz (val, sizeof (val), "%f", value);
    Cvar_SetDefault (var, val);
}

/*
 * Cvar_VariableValue
 */
float Cvar_VariableValue (char *var_name) {
    cvar_t       *var;

    if (!(var = Cvar_FindVar (var_name)))
        return 0;

    return Q_atof (var->string);
}

/*
 * Cvar_VariableString
 */
char         *Cvar_VariableString (char *var_name) {
    cvar_t       *var;

    if (!(var = Cvar_FindVar (var_name)))
        return cvar_null_string;

    return var->string;
}

/*
 * Cvar_CompleteVariable
 */
char         *Cvar_CompleteVariable (char *partial) {
    cvar_t       *cvar;
    int           len;

    if (!(len = strlen (partial)))
        return NULL;

    // check functions
    for (cvar = cvar_vars; cvar; cvar = cvar->next)
        if (!Q_strncasecmp (partial, cvar->name, len))
            return cvar->name;

    return NULL;
}

/*
 * Cvar_CompleteCount
 */
int Cvar_CompleteCount (char *partial) {
    cvar_t       *cvar;
    int           len, c = 0;

    if (!(len = strlen (partial)))
        return 0;

    // check partial match
    for (cvar = cvar_vars; cvar; cvar = cvar->next)
        if (!Q_strncasecmp (partial, cvar->name, len))
            c++;

    return c;
}

static qboolean changing = false;
/*
 * Cvar_OnChange
 */
qboolean Cvar_OnChange (qboolean (*OnChange) (struct cvar_s * var, char *value), cvar_t * var, char *value) {
    if (OnChange && !changing) {
        if (var->flags & CVAR_AGGREGATE) {
            if (!(var->flags & CVAR_FORCE) && cls.signon < SIGNONS)
                changing = true;
        } else {
            changing = true;
        }

        if (OnChange (var, value)) {
            changing = false;
            return true;
        }
        changing = false;
    }
    return false;
}

/*
 * Cvar_Set
 */
void Cvar_Set (cvar_t * var, char *value) {
    char oldvalue[MAX_VARLEN];

#ifdef DEBUG
    if (!strcmp (var->name, debug_cvar.string))
        Cvar_Print (var);
#endif

    if (!var || !Cvar_FindVar (var->name)) {
        if (cl_warncvar.value || developer.value)
            Con_Printf ("Unknown cvar \"%s\"\n", var->name);
        return;
    } else if (!(var->flags & CVAR_FORCE) && changing && cls.signon < SIGNONS) {
        if (developer.value > 1)
            Con_Printf ("Cvar \"%s\" is write protected\n", var->name);
        return;
    } else if ((var->flags & CVAR_INIT) && host_initialized) {
        if (cl_warncvar.value || developer.value)
            Con_Printf ("Cvar \"%s\" can only be changed with \"+set %s %s\" on the command line\n", var->name, var->name, value);
        return;
    } else if ((var->flags & CVAR_AFTER_INIT) && !host_initialized) { // lxndr: to be removed
        if (cl_warncvar.value || developer.value)
            Con_Printf ("Cvar \"%s\" can only be changed after initialization\n", var->name);
        return;
    } else if ((var->flags & CVAR_LOCK)) {
        if ((cl_warncvar.value && cls.signon == SIGNONS) || developer.value || key_dest == key_console)
            Con_Printf ("Cvar \"%s\" is write protected\n", var->name);
        var->value = Q_atof (var->string);      // lxndr: keeps float value directly used by the menu synchronized
        return;
    } else if (!(var->flags & CVAR_AGGREGATE) && !strcmp (var->string, value)) {
        if (!(var->flags & CVAR_FORCE) && (developer.value > 1 || key_dest == key_console))
            Con_Printf ("Cvar \"%s\" is already set.\n", var->name);
        return;
    }

    if (developer.value)
        strcpy (oldvalue, var->string);

    if (var->OnChange && !(var->flags & CVAR_AFTER_CHANGE)) {
        if (Cvar_OnChange (var->OnChange, var, value))
            return;
    }

#ifdef GLQUAKE
    if ((var->flags & CVAR_AMBIENCE) && ambience_switchtime.value) {
        if (var->prevvalue == var->nextvalue) {
            var->nextvalue = Q_atof (value);
            var->prevvalue = var->value;
        } else {
            var->value = Q_atof (value);
            if (var->value == var->nextvalue) {
                var->prevvalue = var->nextvalue;
                Z_Free (var->string);      // free the old value string
                var->string = Z_Strdup (value);
            }
        }
    } else
#endif
    {
        Z_Free (var->string);              // free the old value string
        var->string = Z_Strdup (value);
        var->value = Q_atof (var->string);
    }
    if (var->flags & CVAR_LOWFPP) {
        var->value = FloatPrecision (var->value, 1);
        Q_snprintfz (var->string, sizeof (var->string), "%f", var->value);
    }

    if ((var->flags & CVAR_SERVER) && sv.active) {
        SV_BroadcastPrintf ("\"%s\" changed to \"%s\"\n", var->name, var->string);
    } else if (developer.value || key_dest == key_console) {
        if (developer.value)
            Con_Printf ("\"%s\" changed to \"%s\" from \"%s\"\n", var->name, value, oldvalue);
        else
            Con_Printf ("\"%s\" changed to \"%s\"\n", var->name, value);
    }

    // joe, from ProQuake: rcon (64 doesn't mean anything special,
    // but we need some extra space because NET_MAXMESSAGE == RCON_BUFF_SIZE)
    if (rcon_active && (rcon_message.cursize < rcon_message.maxsize - strlen (var->name) - strlen (var->string) - 64)) {
        rcon_message.cursize--;
        MSG_WriteString (&rcon_message, va ("\"%s\" set to \"%s\"\n", var->name, var->string));
    }

    if (var->OnChange && (var->flags & CVAR_AFTER_CHANGE)) {
        if (Cvar_OnChange (var->OnChange, var, value))
            return;
    }
}

/*
 * Cvar_ForceSet
 */
void Cvar_ForceSet (cvar_t * var, char *value) {
    int           saved_flags;

    if (!var)
        return;

    saved_flags = var->flags;
    var->flags &= ~CVAR_LOCK;
    var->flags |= CVAR_FORCE;
    Cvar_Set (var, value);
    var->flags = saved_flags;
}

/*
 * Cvar_SetValue
 */
void Cvar_SetValue (cvar_t * var, float value) {
    char          val[MAX_VARLEN];
    int           i;

    Q_snprintfz (val, sizeof (val), "%f", value);

    for (i = strlen (val) - 1; i > 0 && val[i] == '0'; i--)
        val[i] = 0;
    if (val[i] == '.')
        val[i] = 0;

    Cvar_Set (var, val);
}

/*
 * Cvar_SetAndLock
 */
void Cvar_SetAndLock (cvar_t * var, char *value) {
    if (!var)
        return;

    var->flags &= ~CVAR_LOCK;
    Cvar_Set (var, value);
    var->flags |= CVAR_LOCK;
}

/*
 * Cvar_Lock_f
 */
void Cvar_Lock_f (void) {
    cvar_t       *var;
    int           argc;

    argc = Cmd_Argc ();
    if (argc != 2 && argc != 3) {
        Con_Printf ("Usage:  %s <variable> [<value>]\n", Cmd_Argv (0));
        return;
    }

    if (!(var = Cvar_FindVar (Cmd_Argv (1)))) {
        Con_Printf ("%s : No variable with name %s\n", Cmd_Argv (0), Cmd_Argv (1));
        return;
    }

    if (argc == 3)
        Cvar_SetAndLock (var, Cmd_Argv (2));
    else
        Cvar_SetAndLock (var, var->string);
    Con_Printf ("Locked %s\n", var->name);
}

/*
 * Cvar_Unlock
 */
void Cvar_Unlock (cvar_t * var) {
    if (!var)
        return;

    var->flags &= ~CVAR_LOCK;
}

/*
 * Cvar_UnlockAndSet
 */
void Cvar_UnlockAndSet (cvar_t * var, char *value) {
    if (!var)
        return;

    var->flags &= ~CVAR_LOCK;
    Cvar_Set (var, value);
}

/*
 * Cvar_Unlock_f
 */
void Cvar_Unlock_f (void) {
    cvar_t       *var;
    int           argc;

    argc = Cmd_Argc ();
    if (argc != 2 && argc != 3) {
        Con_Printf ("Usage:  %s <variable> [<value>]\n", Cmd_Argv (0));
        return;
    }

    if (!(var = Cvar_FindVar (Cmd_Argv (1)))) {
        Con_Printf ("%s : No variable with name %s\n", Cmd_Argv (0), Cmd_Argv (1));
        return;
    }

    if (argc == 3)
        Cvar_UnlockAndSet (var, Cmd_Argv (2));
    else
        Cvar_Unlock (var);
    Con_Printf ("Unlocked %s\n", var->name);
}

/*
 * Cvar_Register
 *
 * Adds a freestanding variable to the variable list.
 */
void Cvar_Register (cvar_t * var) {
    char          string[MAX_VARLEN];          // lxndr: was 512
    cvar_t       *old;

    // first check to see if it has already been defined
    old = Cvar_FindVar (var->name);
    if (old && !(old->flags & CVAR_USER_CREATED)) {
        Con_Printf ("Can't register variable %s, already defined\n", var->name);
        return;
    }
    // check for overlap with a command
    if (Cmd_FindCommand (var->name)) {
        Con_Printf ("Cvar_Register: %s is a command\n", var->name);
        return;
    }

    var->defaultvalue = Z_Strdup (var->string);
    if (old) {
        var->flags |= old->flags & ~CVAR_USER_CREATED;
        Q_strncpyz (string, old->string, sizeof (string));
        Cvar_Delete (old->name);
        if (!(var->flags & CVAR_LOCK))
            var->string = Z_Strdup (string);
        else
            var->string = Z_Strdup (var->string);
    } else {                           // allocate the string on zone because
        // future sets will Z_Free it
        var->string = Z_Strdup (var->string);
    }
    var->prevvalue = var->nextvalue = var->value = Q_atof (var->string);

/*    for (old = cvar_vars; old; old = old->next) {
        Con_Printf ("loop: %s %s\n", old->name, old->next ? old->next->name : "NULL");
        if (old->next && Q_strcasecmp (var->name, old->next->name) > 0)
            break;
    }
    if (old && old->next && Q_strcasecmp (var->name, old->next->name) > 0) {
        var->next = old->next;
        old->next = var;
        if (old == cvar_vars)
            cvar_vars = var;
        Con_Printf ("match: %s: %s %s %s (%p)\n", cvar_vars->name, old->name, var->name, old->next->name, cvar_vars->next);
    } else {*/

    // link the variable in
    var->next = cvar_vars;
    cvar_vars = var;

//        Con_Printf ("end: %s: %s\n", cvar_vars->name, var->name);
//    }
    Con_Printf ("\n");
}

/*
 * Cvar_Command
 *
 * Handles variable inspection and changing from the console
 */
qboolean Cvar_Command (void) {
    cvar_t       *var;

    // check variables
    if (!(var = Cvar_FindVar (Cmd_Argv (0))))
        return false;

    // perform a variable print or set
    if (Cmd_Argc () == 1)
        Cvar_Print (var);
    else
        Cvar_Set (var, Cmd_Argv (1));

    return true;
}

/*
 * Cvar_WriteVariable
 */
void Cvar_WriteVariable (FILE *f, cvar_t *var) {
    fprintf (f, "%s \"%s\"\n", var->name, var->string);
}

/*
 * Cvar_WriteAllVariables
 *
 * Writes lines containing "set variable value" for all variables with the
 * archive flag set to true.
 */
void Cvar_WriteAllVariables (FILE *f, qboolean global) {
    cvar_t       *var;

    for (var = cvar_vars; var; var = var->next) {
        if (var->flags & CVAR_VOLATILE)
            continue;
        if ((global && !(var->flags & (CVAR_GLOBAL | CVAR_INIT))) || (!global && (var->flags & (CVAR_GLOBAL | CVAR_INIT))))
            continue;
        else if (!Cvar_IsDefault (var))
            Cvar_WriteVariable (f, var);
    }
}

/*
 * Cvar_Find_f
 *
 * List found console variables
 */
void Cvar_Find_f (void) {
    cvar_t       *var;
    int           counter;
    qboolean      lock, unlock, reset;

    if (cmd_source != src_command)
        return;

    if (Cmd_Argc () < 2) {
        Con_Printf ("Usage: %s <pattern> [{lock|unlock|reset}]\n", Cmd_Argv (0));
        return;
    }

    lock = unlock = reset = false;
    if (Cmd_Argc () == 3) {
        if (!strcmp (Cmd_Argv (2), "lock"))
            lock = true;
        else if (!strcmp (Cmd_Argv (2), "unlock"))
            unlock = true;
        else if (!strcmp (Cmd_Argv (2), "reset"))
            reset = true;
    }

    for (counter = 0, var = cvar_vars; var; var = var->next) {
        if (strstr (var->name, Cmd_Argv (1))) {
            if (lock)
                Cvar_SetAndLock (var, var->string);
            else if (unlock)
                Cvar_Unlock (var);
            else if (reset)
                Cvar_ResetVar (var);
            Cvar_Print (var);
            counter++;
        }
    }

    Con_Printf ("------------\nFound %d variables\n", counter);
}

/*
 * Cvar_List_f
 *
 * List all console variables
 */
void Cvar_List_f (void) {
    cvar_t       *var;
    int           counter;
    qboolean      locked = false;

    if (cmd_source != src_command)
        return;

    if (Cmd_Argc () == 2) {
        if (strcmp (Cmd_Argv (1), "locked")) {
            Con_Printf ("Usage: %s [locked]\n", Cmd_Argv (0));
            return;
        }
        locked = true;
    }

    for (counter = 0, var = cvar_vars; var; var = var->next, counter++)
        if (!locked || (var->flags & CVAR_LOCK))
            Cvar_Print (var);

    Con_Printf ("------------\n%d variables\n", counter);
}

/*
 * Cvar_ResetAll
 */
int Cvar_ResetAll (void) {
    cvar_t       *var;
    int           counter;
    int           mode;

    mode = CL_CheckMode ();

    for (counter = 0, var = cvar_vars; var; var = var->next, counter++)
        Cvar_ResetVar (var);

    CL_SetMode (mode);

    return counter;
}

/*
 * Cvar_ResetCvars_f
 *
 * Reset all console variables
 */
void Cvar_ResetCvars_f (void) {
    Con_Printf ("------------\nAll %d variables reset\n", Cvar_ResetAll ());
}

/*
 * Cvar_Create
 */
cvar_t       *Cvar_Create (char *name, char *string, int cvarflags) {
    cvar_t       *v;

    if ((v = Cvar_FindVar (name)))
        return v;
    v = (cvar_t *) Z_Malloc (sizeof (cvar_t));

    // Cvar doesn't exist, so we create it
    v->next = cvar_vars;
    cvar_vars = v;

    v->name = Z_Strdup (name);
    v->string = Z_Strdup (string);
    v->defaultvalue = Z_Strdup (string);
    v->flags = cvarflags;
    v->value = Q_atof (v->string);

    return v;
}

/*
 * Cvar_Delete
 *
 * returns true if the cvar was found (and deleted)
 */
qboolean Cvar_Delete (char *name) {
    cvar_t       *var, *prev;

    if (!(var = Cvar_FindVar (name)))
        return false;

    prev = NULL;
    for (var = cvar_vars; var; var = var->next) {
        if (!Q_strcasecmp (var->name, name)) {
            // unlink from cvar list
            if (prev)
                prev->next = var->next;
            else
                cvar_vars = var->next;

            // free
            Z_Free (var->defaultvalue);
            Z_Free (var->string);
            Z_Free (var->name);
            Z_Free (var);
            return true;
        }
        prev = var;
    }

    return false;
}

void Cvar_Set_f (void) {
    cvar_t       *var;
    char         *var_name;

    if (Cmd_Argc () != 3) {
        Con_Printf ("Usage: %s <cvar> <value>\n", Cmd_Argv (0));
        return;
    }

    var_name = Cmd_Argv (1);
    if ((var = Cvar_FindVar (var_name))) {
        Cvar_ForceSet (var, Cmd_Argv (2));
    } else {
        if (Cmd_FindCommand (var_name)) {
            Con_Printf ("\"%s\" is a command\n", var_name);
            return;
        }
        var = Cvar_Create (var_name, Cmd_Argv (2), CVAR_USER_CREATED);
    }
}

/*
 * Cvar_Init
 */
void Cvar_Init (void) {
    Cmd_AddCommand ("cvarfind", Cvar_Find_f);
    Cmd_AddCommand ("cvarlist", Cvar_List_f);
    Cmd_AddCommand ("cvarlock", Cvar_Lock_f);
    Cmd_AddCommand ("cvarreset", Cvar_Reset_f);
    Cmd_AddCommand ("cvarunlock", Cvar_Unlock_f);
    Cmd_AddCommand ("resetcvars", Cvar_ResetCvars_f);
    Cmd_AddCommand ("set", Cvar_Set_f);

#ifdef DEBUG
    Cvar_Register (&debug_cvar);
#endif
}
