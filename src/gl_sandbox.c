/*
 * Copyright (C) 2010 lxndr.
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation; either version 2 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.
 *
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 */
// gl_sandbox.c

#ifdef DEVEL
#  include "quakedef.h"

int           blurtexture;
int           saturationtexture;

void R_InitSandbox (void) {
    blurtexture = texture_extension_number++;
    saturationtexture = texture_extension_number++;
}

void R_RenderRadialBlur (int times, float inc) {        /* Draw The Blurred Image */
    int           width, height;
    int           num;
    float         spost = 0.0f;        /* Starting Texture Coordinate Offset */
    float         alphainc = 0.9f / times;      /* Fade Speed For Alpha Blending */
    float         alpha = 0.2f;        /* Starting Alpha Value */

    R_CalcScreenTextureSize (&width, &height);

    /* Disable AutoTexture Coordinates */
    glDisable (GL_TEXTURE_GEN_S);
    glDisable (GL_TEXTURE_GEN_T);

    glEnable (GL_BLEND);               /* Enable Blending */
    glDisable (GL_DEPTH_TEST);         /* Disable Depth Testing */
//        glEnable(GL_TEXTURE_2D);                                                                /* Enable 2D Texture Mapping */

    GL_Set2D (glx, gly, glwidth, glheight, NEARVAL, FARVAL);    /* Switch To An Ortho View */
    glBlendFunc (GL_SRC_ALPHA, GL_ONE); /* Set Blending Mode */
    glBindTexture (GL_TEXTURE_2D, blurtexture); /* Bind To The Blur Texture */

    alphainc = alpha / times;          /* alphainc=0.2f / Times To Render Blur */

    glBegin (GL_QUADS);                /* Begin Drawing Quads */
    for (num = 0; num < times; num++) { /* Number Of Times To Render Blur */
        glColor4f (1.0f, 1.0f, 1.0f, alpha);    /* Set The Alpha Value (Starts At 0.2) */
        glTexCoord2f (0 + spost, 1 - spost);    /* Texture Coordinate   ( 0, 1 ) */
        glVertex2f (0, 0);             /* First Vertex         (   0,   0 ) */
        glTexCoord2f (0 + spost, 0 + spost);    /* Texture Coordinate   ( 0, 0 ) */
        glVertex2f (0, height);        /* Second Vertex        (   0, height ) */
        glTexCoord2f (1 - spost, 0 + spost);    /* Texture Coordinate   ( 1, 0 ) */
        glVertex2f (width, height);    /* Third Vertex         ( width, height ) */
        glTexCoord2f (1 - spost, 1 - spost);    /* Texture Coordinate   ( 1, 1 ) */
        glVertex2f (width, 0);         /* Fourth Vertex        ( width,   0 ) */

        spost += inc;                  /* Gradually Increase spost (Zooming Closer To Texture Center) */
        alpha = alpha - alphainc;      /* Gradually Decrease alpha (Gradually Fading Image Out) */
    }
    glEnd ();                          /* Done Drawing Quads */

    GL_Set3D ();                       /* Switch To A Perspective View */

    glEnable (GL_DEPTH_TEST);          /* Enable Depth Testing */
//        glDisable(GL_TEXTURE_2D);                                                               /* Disable 2D Texture Mapping */
    glDisable (GL_BLEND);              /* Disable Blending */
    glBindTexture (GL_TEXTURE_2D, 0);  /* Unbind The Blur Texture */
}

void R_RenderSaturation (void) {
    //    if () {
    int           s_x, s_y, s_width, s_height;
    extern vrect_t scr_vrect;
    GLfloat       constant[4];

    s_x = scr_vrect.x * ((float) glwidth / vid.width);
    s_y = scr_vrect.y * ((float) glheight / vid.height);
    s_width = scr_vrect.width * ((float) glwidth / vid.width);
    s_height = scr_vrect.height * ((float) glheight / vid.height);

    constant[0] = gl_blurbrightness.value;        // + 0.5f * SAT_WEIGHT_RED;
    constant[1] = gl_blurbrightness.value;        // + 0.5f * SAT_WEIGHT_GREEN;
    constant[2] = gl_blurbrightness.value;        // + 0.5f * SAT_WEIGHT_BLUE;
    constant[3] = gl_blurbrightness.value;

    GL_DisableMultitexture ();
    glPushMatrix ();
    glLoadIdentity ();
    glEnable (GL_BLEND);
    glBlendFunc (GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

    glEnable (GL_TEXTURE_RECTANGLE_ARB);
    glBindTexture (GL_TEXTURE_RECTANGLE_ARB, saturationtexture);
    glCopyTexSubImage2D (GL_TEXTURE_RECTANGLE_ARB, 0, 0, 0, s_x, glheight - (s_y + s_height), s_width, s_height);

    glEnable (GL_TEXTURE_2D);
    glBindTexture (GL_TEXTURE_2D, saturationtexture);
    glCopyTexSubImage2D (GL_TEXTURE_2D, 0, 0, 0, s_x, glheight - (s_y + s_height), s_width, s_height);

    glTexEnvf (GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_COMBINE);
    glTexEnvf (GL_TEXTURE_ENV, GL_COMBINE_RGB, GL_INTERPOLATE);
    glTexEnvf (GL_TEXTURE_ENV, GL_SOURCE0_RGB, GL_TEXTURE);
    glTexEnvf (GL_TEXTURE_ENV, GL_SOURCE1_RGB, GL_PRIMARY_COLOR);
    glTexEnvf (GL_TEXTURE_ENV, GL_SOURCE2_RGB, GL_PRIMARY_COLOR);
    glTexEnvf (GL_TEXTURE_ENV, GL_OPERAND0_RGB, GL_SRC_COLOR);
    glTexEnvf (GL_TEXTURE_ENV, GL_OPERAND1_RGB, GL_SRC_COLOR);
    glTexEnvf (GL_TEXTURE_ENV, GL_OPERAND2_RGB, GL_SRC_ALPHA);
    glTexEnvf (GL_TEXTURE_ENV, GL_COMBINE_ALPHA, GL_REPLACE);
    glTexEnvf (GL_TEXTURE_ENV, GL_SOURCE0_ALPHA, GL_TEXTURE);
    glTexEnvf (GL_TEXTURE_ENV, GL_OPERAND0_ALPHA, GL_SRC_ALPHA);

    glColor4f (1.0, 1.0, 1.0, gl_bluralpha.value);
    GL_EnableMultitexture ();
    GL_SelectTexture (GL_TEXTURE0_ARB);

    glEnable (GL_TEXTURE_RECTANGLE_ARB);
    glBindTexture (GL_TEXTURE_RECTANGLE_ARB, saturationtexture);
    glCopyTexSubImage2D (GL_TEXTURE_RECTANGLE_ARB, 0, 0, 0, s_x, glheight - (s_y + s_height), s_width, s_height);

    glEnable (GL_TEXTURE_2D);
    glBindTexture (GL_TEXTURE_2D, saturationtexture);

    glTexEnvf (GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_COMBINE);
    glTexEnvf (GL_TEXTURE_ENV, GL_COMBINE_RGB, GL_DOT3_RGB);
    glTexEnvf (GL_TEXTURE_ENV, GL_SOURCE0_RGB, GL_PREVIOUS);
    glTexEnvf (GL_TEXTURE_ENV, GL_SOURCE1_RGB, GL_CONSTANT);
    glTexEnvf (GL_TEXTURE_ENV, GL_OPERAND0_RGB, GL_SRC_COLOR);
    glTexEnvf (GL_TEXTURE_ENV, GL_OPERAND1_RGB, GL_SRC_COLOR);
    glTexEnvf (GL_TEXTURE_ENV, GL_COMBINE_ALPHA, GL_REPLACE);
    glTexEnvf (GL_TEXTURE_ENV, GL_SOURCE0_ALPHA, GL_CONSTANT);
    glTexEnvf (GL_TEXTURE_ENV, GL_OPERAND0_ALPHA, GL_SRC_ALPHA);
    glTexEnvfv (GL_TEXTURE_ENV, GL_TEXTURE_ENV_COLOR, constant);

    glBegin (GL_QUADS);
    glTexCoord2f (s_x, glheight - s_y);
    glVertex2f (s_x, s_y);
    glTexCoord2f (s_x, glheight - s_height - s_y);
    glVertex2f (s_x, s_y + s_height);
    glTexCoord2f (s_x + s_width, glheight - s_height - s_y);
    glVertex2f (s_x + s_width, s_y + s_height);
    glTexCoord2f (s_x + s_width, glheight - s_y);
    glVertex2f (s_x + s_width, s_y);
    glEnd ();

    glTexEnvf (GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_REPLACE);
    glDisable (GL_TEXTURE_RECTANGLE_ARB);
    GL_SelectTexture (GL_TEXTURE0_ARB);
    GL_DisableMultitexture ();

    glEnable (GL_TEXTURE_RECTANGLE_ARB);
    glTexEnvf (GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_REPLACE);
    glDisable (GL_TEXTURE_RECTANGLE_ARB);

    glColor4f (1.0, 1.0, 1.0, gl_bluralpha.value);
    glTexEnvf (GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);
    glTexEnvf (GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_REPLACE);
    glBlendFunc (GL_ONE, GL_ONE_MINUS_SRC_ALPHA);
    glDisable (GL_BLEND);
    glPopMatrix ();
    //    }
}

void R_RenderSandbox (void) {
//    R_RenderRadialBlur (5, 0.2);
//    R_RenderSaturation ();
}
#endif
