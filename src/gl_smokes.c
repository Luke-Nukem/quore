/*
 * Copyright (C) 1997-2001.
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * 
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 * 
 */

#include "quakedef.h"

int           smoketexture;

#define NUM_SMOKE_PARTICLES 64

float         PRGB[3][256];
float         rgba[4];

typedef struct sparticle_s {
    float         alpha;
    float         color;
    int           entropy;
    vec3_t        org;
    float         radius;
    float         velocity;
} sparticle_t;

cvar_t        gl_smokes = { "gl_smokes", "0" };   //MHQuake
cvar_t        gl_smokesalpha = { "gl_smokesalpha", "0.5", CVAR_AMBIENCE };
cvar_t        gl_smokesbrightness = { "gl_smokesbrightness", "0.5", CVAR_AMBIENCE };
cvar_t        gl_smokescolor = { "gl_smokescolor", "0.5", CVAR_AMBIENCE };
cvar_t        gl_smokesdensity = { "gl_smokesdensity", "0.5", CVAR_AMBIENCE };
cvar_t        gl_smokesentropy = { "gl_smokesentropy", "0.5", CVAR_AMBIENCE };
cvar_t        gl_smokesradius = { "gl_smokesradius", "0.5", CVAR_AMBIENCE };
cvar_t        gl_smokesvelocity = { "gl_smokesvelocity", "5", CVAR_AMBIENCE };

sparticle_t   R_SmokeParticles[NUM_SMOKE_PARTICLES];

void R_InitSmokeParticle (sparticle_t * p) {
    float scale;

    p->alpha = bound (0, gl_smokesalpha.value * 0.8 + 0.2, 1);
    p->entropy = bound (0, gl_smokesentropy.value * 10, 10) + 2;
    p->radius = bound (0, gl_smokesradius.value * 10, 10) + 20;
    p->velocity = bound (0, gl_smokesvelocity.value * 10, 10);

    if (rand () & 1)
        p->color = 0 + (rand () % 7);
    else
        p->color = 96 + (rand () % 7);

    scale = gl_subdivide_size.value / (16 - p->entropy);
    if (gl_smokes.value == 1) {
        p->org[0] = ((rand () % (int) scale) - scale / 2) * (16 - p->entropy);
        p->org[1] = ((rand () % (int) scale) - scale / 2) * (16 - p->entropy);
        p->org[2] = 0; //sqrt (rand () % 64);
    } else {
        p->org[0] = (rand () % 4) - 2;
        p->org[1] = (rand () % 4) - 2;
        p->org[2] = (rand () % 4) - 2;
    }
}

void R_UpdateSmokeParticle (sparticle_t * p, float updatetime) {
    float entropy;

    p->org[0] += updatetime * ((rand () % p->entropy) - p->entropy / 2);
    p->org[1] += updatetime * ((rand () % p->entropy) - p->entropy / 2);
    p->org[2] += p->velocity * p->velocity * updatetime / 2;
    if (gl_smokes.value == 1) {
        entropy = (rand () % p->entropy) - p->entropy / 2;
        p->alpha -= (updatetime / (entropy + 8) * (1 + p->velocity));
        p->radius += (updatetime * (entropy + 4) * (10 + p->velocity));    //was 30
        p->velocity += (updatetime * (entropy + 4));    //was 12
    } else {
        p->alpha -= (updatetime / 1);      //was 2
        p->radius += (updatetime * 30);    //was 30
        p->velocity += (updatetime * 8);   //was 12
    }
}

int           NewParticle = NUM_SMOKE_PARTICLES - 1;

void R_UpdateSmokeParticles (void) {
    int           i;

if (gl_smokes.value == 1) {
    for (i = 0; i < NUM_SMOKE_PARTICLES; i++) {
        if (R_SmokeParticles[i].alpha > 0)
            R_UpdateSmokeParticle (&R_SmokeParticles[i], host_frametime);
        else
            R_InitSmokeParticle (&R_SmokeParticles[i]);
    }
} else {
    for (i = 0; i < NUM_SMOKE_PARTICLES; i++)
        R_UpdateSmokeParticle (&R_SmokeParticles[i], host_frametime);

    // add a new particle to the list in the correct position
    // done here to ensure that only one new particle is added each frame, otherwise the
    // smoke animation experiences some strange blips every now and then
    R_InitSmokeParticle (&R_SmokeParticles[NewParticle]);

    // find the new correct position for the next frame
    if (--NewParticle < 0)
        NewParticle = NUM_SMOKE_PARTICLES - 1;
}
}

void R_InitSmokeParticles (void) {
    int           i;

    for (i = 0; i < NUM_SMOKE_PARTICLES; i++) {
        R_InitSmokeParticle (&R_SmokeParticles[i]);
        R_SmokeParticles[i].alpha = (float) (rand () % 10) / 10;
    }
}

void R_InitSmokes (void) {
    byte          basedata[32][32] = {
        {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
        {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 2, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
        {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 3, 3, 4, 4, 4, 3, 2, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
        {0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 2, 2, 3, 4, 5, 7, 6, 6, 6, 3, 2, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0},
        {0, 0, 0, 0, 0, 0, 0, 0, 1, 3, 3, 3, 3, 4, 6, 8, 10, 8, 6, 4, 3, 2, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0},
        {0, 0, 0, 0, 0, 0, 0, 0, 2, 4, 6, 6, 6, 5, 8, 12, 10, 6, 5, 4, 4, 3, 1, 2, 2, 1, 0, 0, 0, 0, 0, 0},
        {0, 0, 0, 0, 0, 0, 0, 1, 3, 5, 8, 11, 11, 11, 12, 12, 11, 7, 7, 6, 5, 3, 2, 2, 1, 1, 1, 0, 0, 0, 0, 0},
        {0, 0, 0, 0, 0, 1, 0, 2, 6, 9, 11, 13, 15, 17, 16, 14, 13, 11, 9, 6, 4, 3, 3, 1, 1, 0, 0, 0, 0, 0, 0, 0},
        {0, 0, 0, 0, 1, 1, 1, 4, 8, 12, 14, 16, 20, 22, 24, 24, 24, 22, 16, 12, 7, 5, 4, 2, 3, 0, 0, 0, 0, 0, 0, 0},
        {0, 0, 0, 1, 3, 3, 2, 5, 8, 12, 12, 18, 26, 29, 32, 35, 33, 28, 23, 18, 11, 6, 3, 2, 3, 1, 1, 1, 0, 0, 0, 0},
        {0, 0, 0, 1, 3, 3, 5, 7, 11, 15, 15, 18, 32, 38, 39, 40, 37, 29, 23, 22, 17, 11, 6, 3, 3, 2, 1, 1, 1, 0, 0, 0},
        {0, 0, 1, 1, 3, 4, 8, 11, 19, 21, 20, 27, 36, 42, 41, 38, 37, 27, 24, 25, 22, 16, 11, 5, 4, 3, 3, 3, 1, 0, 0, 0},
        {0, 0, 1, 2, 5, 7, 11, 17, 23, 21, 22, 34, 39, 46, 43, 36, 31, 27, 22, 21, 20, 15, 11, 8, 5, 4, 6, 5, 3, 1, 0, 0},
        {0, 0, 2, 4, 7, 11, 16, 24, 25, 20, 27, 39, 45, 51, 50, 36, 21, 19, 20, 20, 18, 14, 10, 6, 4, 5, 8, 7, 5, 2, 0, 0},
        {0, 0, 3, 6, 10, 13, 19, 25, 27, 25, 36, 43, 45, 48, 48, 36, 29, 25, 25, 24, 20, 15, 10, 9, 9, 10, 9, 7, 4, 3, 0, 0},
        {0, 0, 2, 5, 8, 14, 18, 23, 27, 27, 37, 44, 43, 43, 41, 41, 40, 36, 32, 28, 22, 17, 17, 17, 16, 13, 9, 6, 5, 3, 1, 0},
        {0, 0, 2, 5, 9, 13, 17, 21, 26, 27, 33, 37, 36, 38, 39, 42, 48, 45, 37, 34, 27, 18, 21, 22, 19, 15, 11, 6, 6, 4, 2, 0},
        {0, 1, 3, 5, 10, 14, 17, 22, 27, 26, 26, 28, 33, 40, 39, 40, 45, 43, 36, 34, 29, 23, 24, 26, 22, 18, 11, 7, 6, 3, 1, 0},
        {0, 0, 2, 6, 11, 15, 17, 24, 26, 21, 21, 23, 31, 34, 37, 36, 41, 41, 31, 34, 28, 24, 26, 26, 21, 18, 11, 6, 3, 2, 0, 0},
        {0, 0, 2, 4, 11, 16, 19, 22, 22, 18, 18, 17, 19, 21, 24, 32, 36, 37, 34, 34, 29, 24, 24, 22, 18, 12, 11, 7, 3, 1, 0, 0},
        {0, 0, 2, 4, 11, 15, 19, 22, 23, 18, 14, 11, 11, 12, 18, 26, 30, 33, 38, 35, 28, 23, 23, 20, 17, 12, 10, 6, 3, 0, 0, 0},
        {0, 0, 2, 5, 10, 13, 17, 19, 19, 15, 13, 8, 7, 11, 13, 19, 23, 24, 28, 31, 27, 24, 20, 17, 13, 10, 9, 6, 2, 0, 0, 0},
        {0, 0, 0, 4, 7, 9, 13, 15, 13, 11, 8, 6, 6, 13, 15, 19, 19, 18, 17, 16, 17, 15, 14, 11, 8, 6, 6, 4, 2, 0, 0, 0},
        {0, 0, 0, 2, 4, 7, 10, 12, 10, 8, 5, 4, 6, 12, 16, 20, 17, 14, 8, 8, 9, 9, 10, 10, 6, 4, 3, 3, 1, 0, 0, 0},
        {0, 0, 0, 0, 2, 5, 7, 8, 9, 8, 5, 6, 9, 12, 16, 19, 14, 8, 8, 6, 7, 8, 10, 9, 6, 3, 3, 1, 0, 0, 0, 0},
        {0, 0, 0, 0, 0, 2, 4, 5, 6, 6, 5, 6, 8, 11, 12, 11, 6, 4, 6, 7, 9, 12, 9, 8, 5, 3, 2, 0, 0, 0, 0, 0},
        {0, 0, 0, 0, 0, 0, 1, 2, 4, 5, 5, 6, 6, 7, 8, 5, 3, 0, 3, 6, 8, 9, 8, 6, 3, 2, 0, 0, 0, 0, 0, 0},
        {0, 0, 0, 0, 0, 0, 0, 0, 2, 2, 3, 3, 3, 4, 4, 3, 2, 0, 2, 6, 7, 7, 6, 3, 2, 0, 0, 0, 0, 0, 0, 0},
        {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2, 3, 3, 3, 2, 1, 0, 0, 0, 2, 5, 5, 3, 2, 0, 0, 0, 0, 0, 0, 0, 0},
        {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 2, 2, 1, 0, 0, 0, 0, 1, 2, 2, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0},
        {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
        {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    };
    byte          smokedata[32][32][4];
    int           x, y;

    for (x = 0; x < 32; x++) {
        for (y = 0; y < 32; y++) {
            smokedata[x][y][0] = 255;
            smokedata[x][y][1] = 255;
            smokedata[x][y][2] = 255;
            smokedata[x][y][3] = basedata[x][y];
        }
    }

    smoketexture = GL_LoadTexture ("part:smokes", 32, 32, (byte *) smokedata, TEX_MIPMAP | TEX_ALPHA | TEX_TRILINEAR | TEX_NOLEVELS, 4);

    Cvar_Register (&gl_smokes);        //R00k -end
    Cvar_Register (&gl_smokesalpha);
    Cvar_Register (&gl_smokesbrightness);
    Cvar_Register (&gl_smokescolor);
    Cvar_Register (&gl_smokesdensity);
    Cvar_Register (&gl_smokesentropy);
    Cvar_Register (&gl_smokesradius);
    Cvar_Register (&gl_smokesvelocity);

    R_InitSmokeParticles ();

    for (x = 0; x < 256; x++) {
        PRGB[0][x] = 0.00392156862745 * ((byte *) & d_8to24table[x])[0];
        PRGB[1][x] = 0.00392156862745 * ((byte *) & d_8to24table[x])[1];
        PRGB[2][x] = 0.00392156862745 * ((byte *) & d_8to24table[x])[2];
    }
}

vec3_t        pUp, pRight, pOrg;

void R_DrawSurfaceSmoke (glpoly_t *p) {
    int           i, density, mod2;
    float         org0, org1;

    density = bound (1, 11 - gl_smokesdensity.value * 10, 11);
    mod2 = p->rand % 2;
    for (i = p->rand % density; i < NUM_SMOKE_PARTICLES; i += density) {
        org0 = p->midpoint[0] + R_SmokeParticles[i].org[mod2];
        org1 = p->midpoint[1] + R_SmokeParticles[i].org[!mod2];

        if (R_SmokeParticles[i].alpha < 0)
            continue;
        if (org0 <= p->minmaxs[0] || org0 >= p->minmaxs[3 + 0])
            continue;
        if (org1 <= p->minmaxs[1] || org1 >= p->minmaxs[3 + 1])
            continue;

        // calculate the scaled vertex offset factors
        VectorScale (vup, R_SmokeParticles[i].radius, pUp);
        VectorScale (vright, R_SmokeParticles[i].radius, pRight);

        // these particles will be off-center, so correct that by calcing a new origin
        // to offset the verts from, which is half the offset away in each direction
        pOrg[0] = org0 - (pRight[0] + pUp[0]) / 2;
        pOrg[1] = org1 - (pRight[1] + pUp[1]) / 2;
        pOrg[2] = p->midpoint[2] + R_SmokeParticles[i].org[2] - (pRight[2] + pUp[2]) / 2 - p->rand % 16;

        if (gl_smokes.value == 1) {
            rgba[3] = R_SmokeParticles[i].alpha;
            glColor4fv (rgba);
        } else {
            glColor4f (PRGB[0][(int) R_SmokeParticles[i].color], PRGB[1][(int) R_SmokeParticles[i].color], PRGB[2][(int) R_SmokeParticles[i].color], R_SmokeParticles[i].alpha);
        }

        glBegin (GL_QUADS);
        glTexCoord2f (0, 0);
        glVertex3fv (pOrg);
        glTexCoord2f (1, 0);
        glVertex3f (pOrg[0] + pUp[0], pOrg[1] + pUp[1], pOrg[2] + pUp[2]);
        glTexCoord2f (1, 1);
        glVertex3f (pOrg[0] + pRight[0] + pUp[0], pOrg[1] + pRight[1] + pUp[1], pOrg[2] + pRight[2] + pUp[2]);
        glTexCoord2f (0, 1);
        glVertex3f (pOrg[0] + pRight[0], pOrg[1] + pRight[1], pOrg[2] + pRight[2]);
        glEnd ();
    }
}

void R_DrawPointSmoke (vec3_t org) {
    int           i;

    for (i = 0; i < NUM_SMOKE_PARTICLES; i++) {

        if (R_SmokeParticles[i].alpha < 0)
            continue;

        // calculate the scaled vertex offset factors
        VectorScale (vup, R_SmokeParticles[i].radius / 4, pUp);
        VectorScale (vright, R_SmokeParticles[i].radius / 4, pRight);

        // these particles will be off-center, so correct that by calcing a new origin
        // to offset the verts from, which is half the offset away in each direction
        pOrg[0] = org[0] - (pRight[0] + pUp[0]) / 2;
        pOrg[1] = org[1] - (pRight[1] + pUp[1]) / 2;
        pOrg[2] = org[2] + fabs (R_SmokeParticles[i].org[2]) - (pRight[2] + pUp[2]) / 2 + 2.0;

        if (gl_smokes.value == 1) {
            rgba[3] = R_SmokeParticles[i].alpha;
            glColor4fv (rgba);
        } else {
            glColor4f (PRGB[0][(int) R_SmokeParticles[i].color], PRGB[1][(int) R_SmokeParticles[i].color], PRGB[2][(int) R_SmokeParticles[i].color], R_SmokeParticles[i].alpha);
        }

        glBegin (GL_QUADS);
        glTexCoord2f (0, 0);
        glVertex3fv (pOrg);
        glTexCoord2f (1, 0);
        glVertex3f (pOrg[0] + pUp[0], pOrg[1] + pUp[1], pOrg[2] + pUp[2]);
        glTexCoord2f (1, 1);
        glVertex3f (pOrg[0] + pRight[0] + pUp[0], pOrg[1] + pRight[1] + pUp[1], pOrg[2] + pRight[2] + pUp[2]);
        glTexCoord2f (0, 1);
        glVertex3f (pOrg[0] + pRight[0], pOrg[1] + pRight[1], pOrg[2] + pRight[2]);
        glEnd ();
    }
}

inline qboolean Probe (vec3_t vec) {
    float  probe;
    vec3_t b00, b01, b11, b10, t;

    probe = gl_subdivide_size.value;
    VectorCopy (vec, t);
    t[2] -= 64;
    VectorCopy (t, b00);
    VectorCopy (t, b01);
    VectorCopy (t, b11);
    VectorCopy (t, b10);
    b00[0] -= probe;
    b00[1] -= probe;
    b01[0] += probe;
    b01[1] -= probe;
    b11[0] += probe;
    b11[1] += probe;
    b10[0] -= probe;
    b10[1] += probe;
    if (SV_PointContents (b00) == CONTENTS_SOLID && SV_PointContents (b01) == CONTENTS_SOLID && SV_PointContents (b11) == CONTENTS_SOLID && SV_PointContents (b10) == CONTENTS_SOLID) {
        t[2] -= 64;
        if (SV_PointContents (t) == CONTENTS_SOLID) {
            t[2] += 144;
            if (SV_PointContents (t) == CONTENTS_EMPTY)
                return true;
        }
    }
    return false;
}

//extern int    particle_mode;
void R_RenderSmokes (void) {
    int           i;
    msurface_t   *surf;
    glpoly_t     *p;
    vec3_t        distance;

    if (!gl_smokes.value)
        return;

    R_CalcRGB (gl_smokescolor.value, gl_smokesbrightness.value, (float *) rgba);

    glEnable (GL_BLEND);
    glEnable (GL_TEXTURE_2D);
    glEnable (GL_DEPTH_TEST);

//    glBlendFunc (GL_SRC_ALPHA, GL_ONE);
//    glBlendFunc (GL_SRC_ALPHA, GL_DST_ALPHA);
    glBlendFunc (GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    glDepthMask (GL_FALSE);
    glShadeModel (GL_SMOOTH);
    glTexEnvi (GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);

    GL_Bind (smoketexture);

    // do lava smoke/haze

    for (i = 0, surf = cl.worldmodel->surfaces; i < cl.worldmodel->numsurfaces; i++, surf++) {
        if (surf->visframe != r_framecount)
            continue;
        if (!((surf->texinfo->flags & TEX_LAVA) || (surf->texinfo->flags & TEX_SLIME)))
            continue;

        // ok, we have a lava surf which is visible
        for (p = surf->polys; p; p = p->next) {
            VectorSubtract (r_refdef.vieworg, p->midpoint, distance);
            if (VectorLength (distance) > r_farclip.value)
                continue;

            if (surf->plane->normal[0] == 0 && surf->plane->normal[1] == 0 && surf->plane->normal[2] == 1)
//            if (Probe (p->midpoint))
                R_DrawSurfaceSmoke (p);
        }
    }

    // now do torch smoke
    for (i = 0; i < cl_numvisedicts; i++) {
        if (cl_visedicts[i]->model->modhint != MOD_FLAME)
            continue;

        R_DrawPointSmoke (cl_visedicts[i]->origin);
    }

    glBlendFunc (GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    glDepthMask (GL_TRUE);
    glShadeModel (GL_FLAT);
    glTexEnvi (GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_REPLACE);

    glDisable (GL_BLEND);
    glDisable (GL_DEPTH_TEST);
    glDisable (GL_TEXTURE_2D);

    R_UpdateSmokeParticles ();
}
