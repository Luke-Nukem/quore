
#include "quakedef.h"

// joe: ReadDir()'s stuff
#ifndef _WIN32
#  include <glob.h>
#  include <unistd.h>
#  include <sys/stat.h>
#endif

int           RDFlags = 0;
direntry_t   *filelist = NULL;
int           num_files = 0;

#define SLASHJMP(x, y)	(x = !(x = strrchr(y, '/')) ? y : x + 1)

void FS_AddEntry (char *fname, int ftype, long fsize, qboolean pack) {
    int           i, pos;

    filelist = Q_realloc (filelist, (num_files + 1) * sizeof (direntry_t));

#ifdef _WIN32
    toLower (fname);                   // lxndr: marker
    // else don't convert, linux is case sensitive
#endif

    // inclusion sort
    for (i = 0; i < num_files; i++) {
        if (ftype < filelist[i].type)
            continue;
        else if (ftype > filelist[i].type)
            break;

        if (strcmp (fname, filelist[i].name) < 0)
            break;
    }
    pos = i;
    for (i = num_files; i > pos; i--)
        filelist[i] = filelist[i - 1];

    filelist[i].name = Q_strdup (fname);
    filelist[i].type = ftype;
    filelist[i].size = fsize;
    filelist[i].pack = pack;

    num_files++;
}

qboolean FS_CheckEntry (char *ename) {
    int           i;

    for (i = 0; i < num_files; i++)
        if (!Q_strcasecmp (ename, filelist[i].name))
            return true;

    return false;
}

void FS_EraseAllEntries (void) {
    int           i;

    for (i = 0; i < num_files; i++)
        if (filelist[i].name)
            Q_free (filelist[i].name);

    if (filelist) {
        Q_free (filelist);
        filelist = NULL;
        num_files = 0;
    }
    RDFlags = 0;
}

qboolean FS_IsMap (char *bspname) {
    if (!strcmp (bspname, "b_batt0.bsp") || !strcmp (bspname, "b_batt1.bsp")
        || !strcmp (bspname, "b_bh10.bsp") || !strcmp (bspname, "b_bh100.bsp")
        || !strcmp (bspname, "b_bh25.bsp") || !strcmp (bspname, "b_explob.bsp")
        || !strcmp (bspname, "b_nail0.bsp") || !strcmp (bspname, "b_nail1.bsp")
        || !strcmp (bspname, "b_rock0.bsp") || !strcmp (bspname, "b_rock1.bsp")
        || !strcmp (bspname, "b_shell0.bsp") || !strcmp (bspname, "b_shell1.bsp")
        || !strcmp (bspname, "b_wbh10.bsp") || !strcmp (bspname, "b_wbh25.bsp")
        || !strcmp (bspname, "b_exbox2.bsp") || !strcmp (bspname, "b_barrel.bsp"))
        return false;

    return true;
}

void FS_ReadDir (char *path, char *arg) {
#ifdef _WIN32
    HANDLE        h;
    WIN32_FIND_DATA fd;
#else
    int           h, i = 0;
    char         *foundname;
    glob_t        fd;
    struct stat   fileinfo;
#endif

    if (path[strlen (path) - 1] == '/')
        path[strlen (path) - 1] = 0;

//    if (!(RDFlags & RD_NOERASE))
//        FS_EraseAllEntries ();

#ifdef _WIN32
    h = FindFirstFile (va ("%s/%s", path, arg), &fd);
    if (h == INVALID_HANDLE_VALUE)
#else
    h = glob (va ("%s/%s", path, arg), 0, NULL, &fd);
    if (h == GLOB_ABORTED)
#endif
    {
        if (RDFlags & RD_MENU_DEMOS) {
            FS_AddEntry ("Error reading directory", 3, 0, false);
            num_files = 1;
//        } else if (RDFlags & RD_COMPLAIN) {
//            Con_Printf ("No such file\n");
        }
        return;
    }

    if (RDFlags & RD_MENU_DEMOS) {
        if (RDFlags & RD_ROOT)
            FS_AddEntry ("demos", 1, 0, false);
        else
            FS_AddEntry ("..", 2, 0, false);
        num_files = 1;
    }

    do {
        int           fdtype;
        long          fdsize;
        char          filename[MAX_FILENAME];

#ifdef _WIN32
        if (fd.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY) {
            if (!(RDFlags & (RD_MENU_DEMOS | RD_GAMEDIR)) || !strcmp (fd.cFileName, ".") || !strcmp (fd.cFileName, ".."))
                continue;

            fdtype = 1;
            fdsize = 0;
            Q_strncpyz (filename, fd.cFileName, sizeof (filename));
        } else {
           char          ext[8];

            if (RDFlags & RD_GAMEDIR)
                continue;

            Q_strncpyz (ext, Com_FileExtension (fd.cFileName), sizeof (ext));

            if (RDFlags & RD_MENU_DEMOS && Q_strcasecmp (ext, "dem") && Q_strcasecmp (ext, "dz"))
                continue;

            fdtype = 0;
            fdsize = fd.nFileSizeLow;
            if (Q_strcasecmp (ext, "dz") && RDFlags & (RD_STRIPEXT | RD_MENU_DEMOS)) {
                Com_StripExtension (fd.cFileName, filename);
                if (RDFlags & RD_SKYBOX) {
                    int           idx = strlen (filename) - 3;

                    filename[filename[idx] == '_' ? idx : idx + 1] = 0; // cut off skybox_ext
                }
            } else {
                Q_strncpyz (filename, fd.cFileName, sizeof (filename));
            }

            if (FS_CheckEntry (filename))
                continue;              // file already on list
        }
#else
        if (h == GLOB_NOMATCH || !fd.gl_pathc)
            break;

        SLASHJMP (foundname, fd.gl_pathv[i]);
        stat (fd.gl_pathv[i], &fileinfo);
        if (S_ISDIR (fileinfo.st_mode)) {
            if (!(RDFlags & (RD_MENU_DEMOS | RD_GAMEDIR)))
                continue;

            fdtype = 1;
            fdsize = 0;
            Q_strncpyz (filename, foundname, sizeof (filename));
        } else {
            char          ext[8];

            if (RDFlags & RD_GAMEDIR)
                continue;

            Q_strncpyz (ext, Com_FileExtension (foundname), sizeof (ext));

            if (RDFlags & RD_MENU_DEMOS && Q_strcasecmp (ext, "dem") && Q_strcasecmp (ext, "dz"))
                continue;

            if (!Q_strcasecmp (ext, "bsp") && !FS_IsMap (foundname))
                continue;

            fdtype = 0;
            fdsize = fileinfo.st_size;
            if (Q_strcasecmp (ext, "dz") && RDFlags & (RD_STRIPEXT | RD_MENU_DEMOS)) {
                Com_StripExtension (foundname, filename);
                if (RDFlags & RD_SKYBOX) {
                    int           idx = strlen (filename) - 3;

                    filename[filename[idx] == '_' ? idx : idx + 1] = 0; // cut off skybox_ext
                }
            } else {
                Q_strncpyz (filename, foundname, sizeof (filename));
            }

            if (FS_CheckEntry (filename))
                continue;              // file already on list
        }
#endif
        FS_AddEntry (filename, fdtype, fdsize, false);
    }
#ifdef _WIN32
    while (FindNextFile (h, &fd));
    FindClose (h);
#else
    while (++i < fd.gl_pathc);
    globfree (&fd);
#endif

    if (!num_files) {
        if (RDFlags & RD_MENU_DEMOS) {
            FS_AddEntry ("[ no files ]", 3, 0, false);
            num_files = 1;
        } else if (RDFlags & RD_COMPLAIN) {
            Con_Printf ("No such file\n");
        }
    }
}

void FS_SearchDir (char *subdir, char *param, int flags) {
    searchpath_t *search;

    RDFlags |= (flags | RD_STRIPEXT);
    for (search = com_searchpaths; search; search = search->next)
        if (!search->pack)
            FS_ReadDir (va("%s/%s", search->filename, subdir), param);
}

void FS_SearchPack (char *subdir, char *param, int flags) {
    int           i;
    searchpath_t *search;
    pack_t       *pak;
    char         *myarg;

    SLASHJMP (myarg, param);
    for (search = com_searchpaths; search; search = search->next) {
        if (search->pack) {
            int           extlen;
            char         *s, *p, ext[8], ext2[8], filename[MAX_FILENAME];

            // look through all the pak file elements
            pak = search->pack;
            for (i = 0; i < pak->numfiles; i++) {
                s = pak->files[i].name;

                Q_strncpyz (ext, Com_FileExtension (s), sizeof (ext));
                Q_strncpyz (ext2, Com_FileExtension (myarg), sizeof (ext2));
                extlen = strlen (ext2);
                if (!Q_strcasecmp (ext, ext2)) {
                    SLASHJMP (p, s);
                    if (!Q_strcasecmp (ext, "bsp") && !FS_IsMap (p))
                        continue;
                    if (!Q_strncasecmp (s, param, strlen (param) - 2 - extlen) || (*myarg == '*' && !Q_strncasecmp (s, param, strlen (param) - 2 - extlen /*- compl_len*/))) {
                        if (Q_strcasecmp (ext, "dz") && Q_strcasecmp (ext, "skin"))
                            Com_StripExtension (p, filename);
                        else
                            Q_strncpyz (filename, p, sizeof (filename));

                        if (FS_CheckEntry (filename))
                            continue;

                        FS_AddEntry (filename, 0, pak->files[i].filelen, true);
                    }
                }
            }
        }
    }
}

void FS_SearchAll (char *subdir, char *param, int flags) {
    FS_SearchDir (subdir, param, flags);
    FS_SearchPack (subdir, param, flags);
}

void FS_Shutdown (void) {
    FS_EraseAllEntries ();
}

