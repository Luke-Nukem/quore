#include "quakedef.h"

cvar_t        s_device = { "s_device", "default" };

#ifdef HAVE_ALSA
qboolean alsa_initialized = false;
#endif

qboolean SNDDMA_Init (void) {
    int i;

    Cvar_Register (&s_device);

    if ((i = Com_CheckParm ("-sound")) && Com_IsParmData (i + 1))
        Cvar_Set (&s_device, com_argv[i + 1]);

#ifdef HAVE_ALSA
    if (!Com_CheckParm ("-noalsa")) {
        alsa_initialized = SNDDMA_InitAlsa ();
        if (alsa_initialized)
            return true;
    }
#endif
    Cvar_SetDefault (&s_device, "/dev/dsp");
    return SNDDMA_InitOSS ();
}

int SNDDMA_GetDMAPos (void) {
#ifdef HAVE_ALSA
    if (alsa_initialized)
        return SNDDMA_GetDMAPosAlsa ();
#endif
    return SNDDMA_GetDMAPosOSS ();
}

void SNDDMA_Shutdown (void) {
#ifdef HAVE_ALSA
    if (alsa_initialized)
        SNDDMA_ShutdownAlsa ();
    else
#endif
    SNDDMA_ShutdownOSS ();
}

void SNDDMA_Submit (void) {
#ifdef HAVE_ALSA
    if (alsa_initialized)
        SNDDMA_SubmitAlsa ();
    else
#endif
    SNDDMA_SubmitOSS ();
}

