/*
 * Support for the ALSA 1.0.1 sound driver
 *
 * Copyright (C) 1999,2000 contributors of the QuakeForge project Extensively
 * modified for inclusion in ZQuake as alsa_snd_linux.c
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation; either version 2 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.
 *
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to:
 *
 * Free Software Foundation, Inc. 59 Temple Place - Suite 330 Boston, MA
 * 02111-1307, USA
 */

#include "quakedef.h"

#ifdef HAVE_ALSA

#  include <dlfcn.h>
#  include <sys/shm.h>
#  include <alsa/asoundlib.h>

// Global Variables
int           paintedtime, soundtime;
static snd_pcm_uframes_t buffer_size;
static snd_pcm_t *pcm;
static void  *alsa_handle;

// Prototypes
int           SNDDMA_GetDMAPosAlsa (void);

// Main functions

qboolean SNDDMA_InitAlsa (void) {
    int           err, i, bits = -1, channels = -1;
    unsigned int  rate = 0;
    snd_pcm_hw_params_t *hw;
    snd_pcm_sw_params_t *sw;
    snd_pcm_uframes_t period_size;

    if (!(alsa_handle = dlopen ("libasound.so.2", RTLD_GLOBAL | RTLD_NOW)))
        return 0;

    // Allocate memory for configuration of ALSA...
    snd_pcm_hw_params_alloca (&hw);
    snd_pcm_sw_params_alloca (&sw);

    // Check for user-specified parameters...
    if (s_bits.value) {
        bits = s_bits.value;
        if (bits != 24 && bits != 16 && bits != 8) {
            Con_Errorf ("ALSA: invalid sample bits: %d\n", bits);
            return 0;
        }
    }
    if (s_rate.value) {
        rate = s_rate.value;
        if (rate != 96000 && rate != 48000 && rate != 44100 && rate != 22050 && rate != 11025) {
            Con_Errorf ("ALSA: invalid sample rate: %d\n", rate);
            return 0;
        }
    }
    channels = s_channels.value;

    // Initialise ALSA...
    err = snd_pcm_open (&pcm, s_device.string, SND_PCM_STREAM_PLAYBACK, SND_PCM_NONBLOCK);
    if (0 > err) {
        Con_Errorf ("ALSA: could not open '%s'. %s\n", s_device.string, snd_strerror (err));
        return 0;
    }
    Con_DPrintf ("Using PCM %s.\n", s_device.string);

    err = snd_pcm_hw_params_any (pcm, hw);
    if (0 > err) {
        Con_Errorf ("ALSA: error setting hw_params_any. %s\n", snd_strerror (err));
        goto error;
    }
    err = snd_pcm_hw_params_set_access (pcm, hw, SND_PCM_ACCESS_MMAP_INTERLEAVED);
    if (0 > err) {
        Con_Errorf ("ALSA: error setting interleaved PCM access. %s\n", snd_strerror (err));
        goto error;
    }

    // Work out which sound format to use...
    switch (bits) {
        case -1:
            err = snd_pcm_hw_params_set_format (pcm, hw, SND_PCM_FORMAT_S16);   // was _LE
            if (0 <= err) {
                bits = 16;
            } else {
                err = snd_pcm_hw_params_set_format (pcm, hw, SND_PCM_FORMAT_U8);
                if (0 <= err) {
                    bits = 8;
                } else {
                    Con_Errorf ("ALSA: no useable formats. %s\n", snd_strerror (err));
                    goto error;
                }
            }
            break;
        case 8:
        case 16:
            err = snd_pcm_hw_params_set_format (pcm, hw, bits == 8 ? SND_PCM_FORMAT_U8 : SND_PCM_FORMAT_S16); // was _LE
            if (0 > err) {
                Con_Errorf ("ALSA: no usable formats. %s\n", snd_strerror (err));
                goto error;
            }
            break;
        case 24:
            err = snd_pcm_hw_params_set_format (pcm, hw, SND_PCM_FORMAT_S24);
            if (0 > err) {
                Con_Errorf ("ALSA: no usable formats. %s\n", snd_strerror (err));
                goto error;
            }
            break;
        default:
            Con_Errorf ("ALSA: desired format not supported\n");
            goto error;
    }

    // Work out wether to use stereo or not...
    switch (channels) {
        case -1:
            err = snd_pcm_hw_params_set_channels (pcm, hw, 2);
            if (0 <= err) {
                channels = 2;
            } else {
                if (0 <= (err = snd_pcm_hw_params_set_channels (pcm, hw, 1))) {
                    channels = 1;
                } else {
                    Con_Errorf ("ALSA: no usable channels. %s\n", snd_strerror (err));
                    goto error;
                }
            }
            break;
        default:
            err = snd_pcm_hw_params_set_channels (pcm, hw, channels);
            if (0 > err) {
                Con_Errorf ("ALSA: desired channels not supported. %s\n", snd_strerror (err));
                goto error;
            }
    }

    // Sample rate...
    switch (rate) {
        case 0:
            for (i = 0; i < sizeof (s_rates) / 4; i++) {
                rate = s_rates[i];
                err = snd_pcm_hw_params_set_rate_near (pcm, hw, &rate, 0);
                if (!err)
                    break;
            }
            if (i == sizeof (s_rates) / 4) {
                Con_Errorf ("ALSA: no usable rates. %s\n", snd_strerror (err));
                goto error;
            }
            break;
        default:
            err = snd_pcm_hw_params_set_rate_near (pcm, hw, &rate, 0);
            if (0 > err) {
                Con_Errorf ("ALSA: desired rate %i not supported. %s\n", rate, snd_strerror (err));
                goto error;
            }
    }

    buffer_size = 0.1 * bits * channels * rate; // lxndr: 0.1 = min fps
    err = snd_pcm_hw_params_set_buffer_size_near (pcm, hw, &buffer_size);
    if (0 > err) {
        Con_Errorf ("ALSA: unable to set buffer size near %i. %s\n", (int) buffer_size, snd_strerror (err));
        goto error;
    }
    period_size = buffer_size / 2;
    err = snd_pcm_hw_params_set_period_size_near (pcm, hw, &period_size, 0);
    if (0 > err) {
        Con_Errorf ("ALSA: unable to set period size near %i. %s\n", (int) period_size, snd_strerror (err));
        goto error;
    }

    err = snd_pcm_hw_params (pcm, hw);
    if (0 > err) {
        Con_Errorf ("ALSA: unable to install hw params: %s\n", snd_strerror (err));
        goto error;
    }
    err = snd_pcm_sw_params_current (pcm, sw);
    if (0 > err) {
        Con_Errorf ("ALSA: unable to determine current sw params. %s\n", snd_strerror (err));
        goto error;
    }
    err = snd_pcm_sw_params_set_start_threshold (pcm, sw, ~0U);
    if (0 > err) {
        Con_Errorf ("ALSA: unable to set playback threshold. %s\n", snd_strerror (err));
        goto error;
    }
    err = snd_pcm_sw_params_set_stop_threshold (pcm, sw, ~0U);
    if (0 > err) {
        Con_Errorf ("ALSA: unable to set playback stop threshold. %s\n", snd_strerror (err));
        goto error;
    }
    err = snd_pcm_sw_params (pcm, sw);
    if (0 > err) {
        Con_Errorf ("ALSA: unable to install sw params. %s\n", snd_strerror (err));
        goto error;
    }

    shm = &sn;
    shm->channels = channels;
    shm->samplepos = 0;
    shm->samplebits = bits;

    // don't mix less than this in mono samples:
    err = snd_pcm_hw_params_get_period_size (hw, (snd_pcm_uframes_t *) & shm->submission_chunk, 0);
    if (0 > err) {
        Con_Errorf ("ALSA: unable to get period size. %s\n", snd_strerror (err));
        goto error;
    }
    err = snd_pcm_hw_params_get_buffer_size (hw, &buffer_size);
    if (0 > err) {
        Con_Errorf ("ALSA: unable to get buffer size. %s\n", snd_strerror (err));
        goto error;
    }

    shm->samples = buffer_size * shm->channels;
    shm->speed = rate;
    SNDDMA_GetDMAPosAlsa ();

    Con_DPrintf ("%5d channels\n", shm->channels);
    Con_DPrintf ("%5d samples\n", shm->samples);        // lxndr: buffer_size * channels
    Con_DPrintf ("%5d samplepos\n", shm->samplepos);
    Con_DPrintf ("%5d samplebits\n", shm->samplebits);
    Con_DPrintf ("%5d submission_chunk\n", shm->submission_chunk);      // lxndr: period_size
    Con_DPrintf ("%5d speed\n", shm->speed);
    Con_DPrintf ("0x%lx dma buffer\n", (long) shm->buffer);

    return 1;
  error:
    snd_pcm_close (pcm);
    return 0;
}

int SNDDMA_GetDMAPosAlsa (void) {
    const snd_pcm_channel_area_t *areas;
    snd_pcm_uframes_t offset;
    snd_pcm_uframes_t nframes = shm->samples / shm->channels;

    snd_pcm_avail_update (pcm);
    snd_pcm_mmap_begin (pcm, &areas, &offset, &nframes);
    offset *= shm->channels;
    nframes *= shm->channels;
    shm->samplepos = offset;
    shm->buffer = areas->addr;
    return shm->samplepos;
}

void SNDDMA_ShutdownAlsa (void) {
    snd_pcm_close (pcm);
    if (alsa_handle)
        dlclose (alsa_handle);
}

/*
 * SNDDMA_SubmitAlsa
 *
 * Send sound to device if buffer isn't really the dma buffer
 */
void SNDDMA_SubmitAlsa (void) {
    int           state;
    int           count = paintedtime - soundtime;
    const snd_pcm_channel_area_t *areas;
    snd_pcm_uframes_t nframes;
    snd_pcm_uframes_t offset;

    if (snd_blocked)
        return;

    nframes = count / shm->channels;

    snd_pcm_avail_update (pcm);
    snd_pcm_mmap_begin (pcm, &areas, &offset, &nframes);

    state = snd_pcm_state (pcm);

    switch (state) {
        case SND_PCM_STATE_PREPARED:
            snd_pcm_mmap_commit (pcm, offset, nframes);
            snd_pcm_start (pcm);
            break;
        case SND_PCM_STATE_RUNNING:
            snd_pcm_mmap_commit (pcm, offset, nframes);
            break;
        default:
            break;
    }
}

/*
 * These functions are not currently needed by ZQuake, but could be of use in
 * the future...
 *
 * Remember, they assume that sound is already inited.
 *
static void SNDDMA_BlockSound (void) {
    if(++snd_blocked == 1)
        alsa_snd_pcm_pause (pcm, 1);
}

static void SNDDMA_UnblockSound (void) { if(!snd_blocked) return;
    if(!--snd_blocked)
        alsa_snd_pcm_pause (pcm, 0);
}
 */

#endif
