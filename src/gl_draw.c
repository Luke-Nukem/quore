/*
 * Copyright (C) 1996-1997 Id Software, Inc.
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation; either version 2 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.
 *
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 */
// gl_draw.c -- this is the only file outside the refresh that touches the vid buffer

#include "quakedef.h"
#include "free.h"

qboolean      OnChange_scr_crosshairimage (cvar_t * var, char *string);
cvar_t        scr_crosshairimage = { "crosshairimage", "", CVAR_NONE, OnChange_scr_crosshairimage };
cvar_t        scr_crosshairalpha = { "crosshairalpha", "1" };

qboolean      OnChange_scr_conchars (cvar_t * var, char *string);
cvar_t        scr_conchars = { "scr_conchars", "conchars", CVAR_NONE, OnChange_scr_conchars };
qboolean      OnChange_scr_hudchars (cvar_t * var, char *string);
cvar_t        scr_hudchars = { "scr_hudchars", "hudchars", CVAR_NONE, OnChange_scr_hudchars };

qboolean      OnChange_scr_conback (cvar_t * var, char *string);
cvar_t        scr_conback = { "scr_conback", "conback", CVAR_NONE, OnChange_scr_conback };
cvar_t        scr_conbacksize = { "scr_conbacksize", "0" };

qboolean      OnChange_scr_consmooth (cvar_t * var, char *string);
cvar_t        scr_consmooth = { "scr_consmooth", "0", CVAR_NONE, OnChange_scr_consmooth };
qboolean      OnChange_scr_crosshairsmooth (cvar_t * var, char *string);
cvar_t        scr_crosshairsmooth = { "crosshairsmooth", "0", CVAR_NONE, OnChange_scr_crosshairsmooth };
qboolean      OnChange_scr_hudsmooth (cvar_t * var, char *string);
cvar_t        scr_hudsmooth = { "scr_hudsmooth", "0", CVAR_AGGREGATE, OnChange_scr_hudsmooth };
qboolean      OnChange_scr_menusmooth (cvar_t * var, char *string);
cvar_t        scr_menusmooth = { "scr_menusmooth", "0", CVAR_NONE, OnChange_scr_menusmooth };
qboolean      OnChange_scr_sbarsmooth (cvar_t * var, char *string);
cvar_t        scr_sbarsmooth = { "scr_sbarsmooth", "0", CVAR_NONE, OnChange_scr_sbarsmooth };
qboolean      OnChange_scr_textsmooth (cvar_t * var, char *string);
cvar_t        scr_textsmooth = { "scr_textsmooth", "0", CVAR_NONE, OnChange_scr_textsmooth };
qboolean      OnChange_scr_weaponsmooth (cvar_t * var, char *string);
cvar_t        scr_weaponsmooth = { "scr_weaponsmooth", "1", CVAR_NONE, OnChange_scr_weaponsmooth };

cvar_t        scr_conalpha = { "scr_conalpha", "0.5" };
cvar_t        scr_menualpha = { "scr_menualpha", "0.5" };
cvar_t        scr_sbaralpha = { "scr_sbaralpha", "0.5" };
cvar_t        scr_textalpha = { "scr_textalpha", "0.8" };

cvar_t        scr_conbrightness = { "scr_conbrightness", "0.5" };
cvar_t        scr_sbarbrightness = { "scr_sbarbrightness", "0.5" };
cvar_t        scr_textbrightness = { "scr_textbrightness", "0.8" };

cvar_t        gl_vbo = { "gl_vbo", "0" };
cvar_t        gl_vbomapped = { "gl_vbomapped", "0" };

float        *vertexarrayf[MAX_VERTEXARRAY];
int          *vertexarrayi[MAX_VERTEXARRAY];

byte         *draw_charset;            // 8*8 graphic characters
mpic_t       *draw_disc;
mpic_t       *draw_backtile;
mpic_t        draw_null;

static byte   nulldata[64] = {
    252,252,252,252,  0,  0,  0,  0,
    252,252,252,252,  0,  0,  0,  0,
    252,252,252,252,  0,  0,  0,  0,
    252,252,252,252,  0,  0,  0,  0,
      0,  0,  0,  0,252,252,252,252,
      0,  0,  0,  0,252,252,252,252,
      0,  0,  0,  0,252,252,252,252,
      0,  0,  0,  0,252,252,252,252
};

int           fade_texture;
int           translate_texture;
int           char_texturedefault;
int           char_texturehud;
int           char_textureconsole;

extern cvar_t crosshair, crosshaircolor;

mpic_t        crosshairpic;
static qboolean crosshairimage_loaded = false;

mpic_t        conback_data;
mpic_t       *conback = &conback_data;

extern byte   vid_gamma_table[256];
extern float  vid_gamma;

int           crosshairtextures[NUMCROSSHAIRS];

static byte   crosshairdata[NUMCROSSHAIRS][64] = {
    {0xff, 0xff, 0xff, 0xfe, 0xff, 0xff, 0xff, 0xff,
     0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff,
     0xff, 0xff, 0xff, 0xfe, 0xff, 0xff, 0xff, 0xff,
     0xfe, 0xff, 0xfe, 0xff, 0xfe, 0xff, 0xfe, 0xff,
     0xff, 0xff, 0xff, 0xfe, 0xff, 0xff, 0xff, 0xff,
     0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff,
     0xff, 0xff, 0xff, 0xfe, 0xff, 0xff, 0xff, 0xff,
     0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff},

    {0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff,
     0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff,
     0xff, 0xff, 0xff, 0xfe, 0xff, 0xff, 0xff, 0xff,
     0xff, 0xff, 0xfe, 0xfe, 0xfe, 0xff, 0xff, 0xff,
     0xff, 0xff, 0xff, 0xfe, 0xff, 0xff, 0xff, 0xff,
     0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff,
     0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff,
     0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff},

    {0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff,
     0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff,
     0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff,
     0xff, 0xff, 0xff, 0xfe, 0xff, 0xff, 0xff, 0xff,
     0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff,
     0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff,
     0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff,
     0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff},

    {0xfe, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xfe,
     0xff, 0xfe, 0xff, 0xff, 0xff, 0xff, 0xfe, 0xff,
     0xff, 0xff, 0xfe, 0xff, 0xff, 0xfe, 0xff, 0xff,
     0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff,
     0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff,
     0xff, 0xff, 0xfe, 0xff, 0xff, 0xfe, 0xff, 0xff,
     0xff, 0xfe, 0xff, 0xff, 0xff, 0xff, 0xfe, 0xff,
     0xfe, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xfe},

    {0xff, 0xff, 0xfe, 0xfe, 0xfe, 0xff, 0xff, 0xff,
     0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff,
     0xfe, 0xff, 0xff, 0xff, 0xff, 0xff, 0xfe, 0xff,
     0xfe, 0xff, 0xff, 0xfe, 0xff, 0xff, 0xfe, 0xff,
     0xfe, 0xff, 0xff, 0xff, 0xff, 0xff, 0xfe, 0xff,
     0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff,
     0xff, 0xff, 0xfe, 0xfe, 0xfe, 0xff, 0xff, 0xff,
     0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff}
};

static qboolean Draw_LoadCharset (char *name, char *identifier, int texnum, int mode);
static void Draw_LoadDefaultCharset (char *identifier, int texnum, int mode);
static qboolean Draw_LoadConback (char *name);
static void Draw_LoadDefaultConback (void);

/*
 *
 * OnChange
 *
 */

qboolean OnChange_scr_conback (cvar_t * var, char *string) {
    if (!Q_strcasecmp (string, "default")) {
        Draw_LoadDefaultConback ();
        return false;
    }

    return !Draw_LoadConback (string);
}

qboolean OnChange_scr_conchars (cvar_t * var, char *string) {
    if (!Q_strcasecmp (string, "default")) {
        Draw_LoadDefaultCharset ("pic:charsetconsole", char_textureconsole, TEX_ALPHA | TEX_CONSOLE | TEX_TEXT);
        return false;
    }

    return !Draw_LoadCharset (string, "pic:charsetconsole", char_textureconsole, TEX_ALPHA | TEX_CONSOLE | TEX_TEXT);
}

qboolean OnChange_scr_hudchars (cvar_t * var, char *string) {
    if (!Q_strcasecmp (string, "default")) {
        Draw_LoadDefaultCharset ("pic:charsethud", char_texturehud, TEX_ALPHA | TEX_TEXT);
        return false;
    }

    return !Draw_LoadCharset (string, "pic:charsethud", char_texturehud, TEX_ALPHA | TEX_TEXT);
}

qboolean OnChange_scr_crosshairimage (cvar_t * var, char *string) {
    mpic_t       *pic_24bit;

    if (!string[0]) {
        crosshairimage_loaded = false;
        return true;
    }

    if (!(pic_24bit = GL_LoadPicImage (va ("gfx/crosshairs/%s", string), "crosshair", 0, 0, TEX_ALPHA))) {
        if (!(pic_24bit = GL_LoadPicImage (va ("gfx/%s", string), "crosshair", 0, 0, TEX_ALPHA))) {
            crosshairimage_loaded = false;
            Con_Warnf ("Couldn't load crosshair \"%s\"\n", string);
            return true;
        }
    }

    crosshairpic = *pic_24bit;
    crosshairimage_loaded = true;
    return false;
}

qboolean OnChange_scr_crosshairsmooth (cvar_t * var, char *string) {
    float         newval;

    newval = Q_atof (string);
    if (!newval == !scr_crosshairsmooth.value)
        return false;

    GL_ToggleFilter (newval, TEX_CROSSHAIR);
    return false;
}

qboolean OnChange_scr_hudsmooth (cvar_t * var, char *string) {
    if (Q_atof (string)) {
        Cvar_SetValue (&scr_consmooth, 1);
        Cvar_SetValue (&scr_crosshairsmooth, 1);
        Cvar_SetValue (&scr_menusmooth, 1);
        Cvar_SetValue (&scr_sbarsmooth, 1);
        Cvar_SetValue (&scr_textsmooth, 1);
    } else {
        Cvar_SetValue (&scr_consmooth, 0);
        Cvar_SetValue (&scr_crosshairsmooth, 0);
        Cvar_SetValue (&scr_menusmooth, 0);
        Cvar_SetValue (&scr_sbarsmooth, 0);
        Cvar_SetValue (&scr_textsmooth, 0);
    }
    return false;
}

qboolean OnChange_scr_consmooth (cvar_t * var, char *string) {
    float         newval;

    newval = Q_atof (string);
    if (!newval == !scr_consmooth.value)
        return false;

    GL_ToggleFilter (newval, TEX_CONSOLE);
    return false;
}

qboolean OnChange_scr_menusmooth (cvar_t * var, char *string) {
    float         newval;

    newval = Q_atof (string);
    if (!newval == !scr_menusmooth.value)
        return false;

    GL_ToggleFilter (newval, TEX_MENU);
    return false;
}

qboolean OnChange_scr_sbarsmooth (cvar_t * var, char *string) {
    float         newval;

    newval = Q_atof (string);
    if (!newval == !scr_sbarsmooth.value)
        return false;

    GL_ToggleFilter (newval, TEX_SBAR);
    return false;
}

qboolean OnChange_scr_textsmooth (cvar_t * var, char *string) {
    float         newval;

    newval = Q_atof (string);
    if (!newval == !scr_textsmooth.value)
        return false;

    GL_ToggleFilter (newval, TEX_TEXT);
    return false;
}

qboolean OnChange_scr_weaponsmooth (cvar_t * var, char *string) {
    float         newval;

    newval = Q_atof (string);
    if (!newval == !scr_weaponsmooth.value)
        return false;

    GL_ToggleFilter (newval, TEX_WEAPON);
    return false;
}

/*
 * Support Routines
 */

typedef struct cachepic_s {
    char          name[MAX_QPATH];
    mpic_t        pic;
} cachepic_t;

#define	MAX_CACHED_PICS		128
cachepic_t    cachepics[MAX_CACHED_PICS];
int           numcachepics;

byte          menuplyr_pixels[4096];

int           pic_texels;
int           pic_count;

freepic_t *Free_FindPic (char *name) {
    freepic_t *p;

    for (p = freepics; p->name; p++)
        if (!strcmp (name, p->name))
            return p;
    return NULL;
}

void Free_InitPic (char *name, mpic_t *pic) {
    freepic_t *p;

    if ((p = Free_FindPic (name))) {
        pic->width = p->width;
        pic->height = p->height;
    } else {
        Con_Warnf ("missing %s at %p with %d %d\n", name, pic, pic->width, pic->height);
    }
}

/*
 * Draw_PicFromWadLegacy
 */
mpic_t       *Draw_PicFromWadLegacy (char *name) {
    qpic_t       *p;
    mpic_t       *pic, *pic_24bit;

    p = W_GetLumpName (name);
    pic = (mpic_t *) p;

    Free_InitPic (name, pic);
    if ((pic_24bit = GL_LoadPicImage (va ("gfx/%s", name), name, 0, 0, TEX_ALPHA))) {
        memcpy (&pic->glt, &pic_24bit->glt, sizeof (mpic_t) - 8);
        return pic;
    }

    GL_LoadPicTexture (name, pic, p->data, 0);
    return pic;
}

/*
 * Draw_CachePicLegacy
 */
mpic_t       *Draw_CachePicLegacy (char *path) {
    int           i;
    cachepic_t   *pic;
    qpic_t       *dat = NULL;
    mpic_t       *pic_24bit;
    qboolean      gamepic;

    for (pic = cachepics, i = 0; i < numcachepics; pic++, i++)
        if (!strcmp (path, pic->name))
            return &pic->pic;

    if (numcachepics == MAX_CACHED_PICS)
        Sys_Error ("numcachepics == MAX_CACHED_PICS");

    gamepic = Free_FindPic (path) ? true : false;
    if (gamepic) {
        // load the pic from disk
        if (!(dat = (qpic_t *) Com_LoadTempFile (path))) {
            Sys_Warnf ("Draw_CachePic: failed to load %s", path);
            return NULL;
        }
        SwapPic (dat);

        // HACK HACK HACK --- we need to keep the bytes for the translatable
        // player picture just for the menu configuration dialog
        if (!strcmp (path, "gfx/menuplyr.lmp"))
            memcpy (menuplyr_pixels, dat->data, dat->width * dat->height);

        pic->pic.width = dat->width;
        pic->pic.height = dat->height;
    }

    if ((pic_24bit = GL_LoadPicImage (path, NULL, 0, 0, gamepic ? TEX_ALPHA : TEX_NOLEVELS))) {
        memcpy (&pic->pic.glt, &pic_24bit->glt, sizeof (mpic_t) - 8);
        if (!gamepic) { // lxndr: free pic size
            pic->pic.width = pic_24bit->width;
            pic->pic.height = pic_24bit->height;
        }
    } else if (gamepic) {
        GL_LoadPicTexture (path, &pic->pic, dat->data, 0);
    } else {
        if ((dat = (qpic_t *) Com_LoadTempFile (path))) {
            SwapPic (dat);
            GL_LoadPicTexture (path, &pic->pic, dat->data, 0);
            Con_Warnf ("missing %s (%d:%d) and free content support is disabled\n", path, dat->width, dat->height);
        } else {
            Con_DPrintf ("unknown %s and free content support is disabled\n", path);
            return NULL;
        }
    }
    numcachepics++;
    Q_strncpyz (pic->name, path, sizeof (pic->name));
    return &pic->pic;
}

/*
 * Draw_PicFromWadFree
 */
mpic_t       *Draw_PicFromWadFree (char *name) {
    mpic_t       *pic_24bit;
    mpic_t       *ret;

    if ((pic_24bit = GL_LoadPicImage (va ("gfx/%s", name), name, 0, 0, TEX_ALPHA))) {
        ret = Z_Malloc (sizeof (mpic_t));
        memcpy (&ret->glt, &pic_24bit->glt, sizeof (mpic_t) - 8);
        Free_InitPic (name, ret);
        return ret;
    }
    return &draw_null;
}

/*
 * Draw_CachePicFree
 */
mpic_t       *Draw_CachePicFree (char *path) {
    int           i;
    cachepic_t   *pic;
    mpic_t       *p;

    for (pic = cachepics, i = 0; i < numcachepics; pic++, i++)
        if (!strcmp (path, pic->name))
            return &pic->pic;

    if (numcachepics == MAX_CACHED_PICS)
        Sys_Error ("numcachepics == MAX_CACHED_PICS");
    numcachepics++;
    Q_strncpyz (pic->name, path, sizeof (pic->name));

    if (!(p = GL_LoadPicImage (path, NULL, 0, 0, TEX_ALPHA)))
        p = &draw_null;
    memcpy (&pic->pic.glt, &p->glt, sizeof (mpic_t) - 8);
    Free_InitPic (path, &pic->pic);
    return &pic->pic;
}

mpic_t       *Draw_PicFromWad (char *name) {
    if (game.free)
        return Draw_PicFromWadFree (name);
    return Draw_PicFromWadLegacy (name);
}

mpic_t       *Draw_CachePic (char *path) {
    if (game.free)
        return Draw_CachePicFree (path);
    return Draw_CachePicLegacy (path);
}

/*
 * Draw_Character
 *
 * Draws one 8*8 graphics character with 0 being transparent. It can be clipped
 * to the top of the screen to allow the console to be smoothly scrolled off.
 *
 */
static void Draw_CharacterTexture (int x, int y, int num, int texture) {
    int           row, col;
    float         frow, fcol;

    if (y <= -8)
        return;                        // totally off screen
    if (num == 32)
        return;                        // space

    num &= 255;
    row = num >> 4;
    col = num & 15;

    frow = row * 0.0625;
    fcol = col * 0.0625;

    glEnable (GL_BLEND);
    glColor4f (scr_textbrightness.value, scr_textbrightness.value, scr_textbrightness.value, scr_textalpha.value);

    GL_Bind (texture);
    glBegin (GL_QUADS);
    glTexCoord2f (fcol, frow);
    glVertex2f (x, y);
    glTexCoord2f (fcol + 0.0625, frow);
    glVertex2f (x + 8, y);
    glTexCoord2f (fcol + 0.0625, frow + 0.03125);
    glVertex2f (x + 8, y + 8);
    glTexCoord2f (fcol, frow + 0.03125);
    glVertex2f (x, y + 8);
    glEnd ();

    glColor3ubv (color_white);
    glDisable (GL_BLEND);
}

void Draw_Character (int x, int y, int num) {
    Draw_CharacterTexture (x, y, num, char_texturehud);
}

void Draw_CharacterConsole (int x, int y, int num) {
    Draw_CharacterTexture (x, y, num, char_textureconsole);
}

void Draw_StringTexture3 (int x, int y, int z, char *str, int texture) {
    float         frow, fcol;
    int           num = 0;

    if (!*str)
        return;

    glEnable (GL_BLEND);
    glColor4f (scr_textbrightness.value, scr_textbrightness.value, scr_textbrightness.value, scr_textalpha.value);

    GL_Bind (texture);
    glBegin (GL_QUADS);
    while (*str) {                     // stop rendering when out of characters
        if ((num = *str++) != 32) {    // skip spaces
            frow = (float) (num >> 4) * 0.0625;
            fcol = (float) (num & 15) * 0.0625;
            glTexCoord2f (fcol, frow);
            glVertex3f (x, y, z);
            glTexCoord2f (fcol + 0.0625, frow);
            glVertex3f (x + 8, y, z);
            glTexCoord2f (fcol + 0.0625, frow + 0.03125);
            glVertex3f (x + 8, y + 8, z);
            glTexCoord2f (fcol, frow + 0.03125);
            glVertex3f (x, y + 8, z);
        }
        x += 8;
    }
    glEnd ();

    glColor3ubv (color_white);
    glDisable (GL_BLEND);
}

/*
 * Draw_String
 */
void Draw_StringTexture (int x, int y, char *str, int texture) {
    float         frow, fcol;
    int           num = 0;

    if (y <= -8)
        return;                        // totally off screen
    if (!*str)
        return;

    glEnable (GL_BLEND);
    glColor4f (scr_textbrightness.value, scr_textbrightness.value, scr_textbrightness.value, scr_textalpha.value);

    GL_Bind (texture);
    glBegin (GL_QUADS);
    while (*str) {                     // stop rendering when out of characters
        if ((num = *str++) != 32) {    // skip spaces
            frow = (float) (num >> 4) * 0.0625;
            fcol = (float) (num & 15) * 0.0625;
            glTexCoord2f (fcol, frow);
            glVertex2f (x, y);
            glTexCoord2f (fcol + 0.0625, frow);
            glVertex2f (x + 8, y);
            glTexCoord2f (fcol + 0.0625, frow + 0.03125);
            glVertex2f (x + 8, y + 8);
            glTexCoord2f (fcol, frow + 0.03125);
            glVertex2f (x, y + 8);
        }
        x += 8;
    }
    glEnd ();

    glColor3ubv (color_white);
    glDisable (GL_BLEND);
}

void Draw_String (int x, int y, char *str) {
    Draw_StringTexture (x, y, str, char_texturehud);
}

void Draw_StringConsole (int x, int y, char *str) {
    Draw_StringTexture (x, y, str, char_textureconsole);
}

/*
 * Draw_Alt_String
 */
void Draw_Alt_String (int x, int y, char *str) {
    float         frow, fcol;
    int           num;

    if (y <= -8)
        return;                        // totally off screen
    if (!*str)
        return;

    glEnable (GL_BLEND);
    glColor4f (scr_textbrightness.value, scr_textbrightness.value, scr_textbrightness.value, scr_textalpha.value);

    GL_Bind (char_texturehud);
    glBegin (GL_QUADS);
    while (*str) {                     // stop rendering when out of characters
        if ((num = *str++ | 0x80) != (32 | 0x80)) {     // skip spaces
            frow = (float) (num >> 4) * 0.0625;
            fcol = (float) (num & 15) * 0.0625;
            glTexCoord2f (fcol, frow);
            glVertex2f (x, y);
            glTexCoord2f (fcol + 0.0625, frow);
            glVertex2f (x + 8, y);
            glTexCoord2f (fcol + 0.0625, frow + 0.03125);
            glVertex2f (x + 8, y + 8);
            glTexCoord2f (fcol, frow + 0.03125);
            glVertex2f (x, y + 8);
        }
        x += 8;
    }
    glEnd ();

    glColor3ubv (color_white);
    glDisable (GL_BLEND);
}

/*
 * Draw_TextBox
 */
void Draw_TextBox (int x, int y, int width, int lines) {
    mpic_t       *p;
    int           cx, cy, n;

    // draw left side
    cx = x;
    cy = y;
    p = Draw_CachePic ("gfx/box_tl.lmp");
    Draw_TransPic (cx, cy, p);
    p = Draw_CachePic ("gfx/box_ml.lmp");
    for (n = 0; n < lines; n++) {
        cy += 8;
        Draw_TransPic (cx, cy, p);
    }
    p = Draw_CachePic ("gfx/box_bl.lmp");
    Draw_TransPic (cx, cy + 8, p);

    // draw middle
    cx += 8;
    while (width > 0) {
        cy = y;
        p = Draw_CachePic ("gfx/box_tm.lmp");
        Draw_TransPic (cx, cy, p);
        p = Draw_CachePic ("gfx/box_mm.lmp");
        for (n = 0; n < lines; n++) {
            cy += 8;
            if (n == 1)
                p = Draw_CachePic ("gfx/box_mm2.lmp");
            Draw_TransPic (cx, cy, p);
        }
        p = Draw_CachePic ("gfx/box_bm.lmp");
        Draw_TransPic (cx, cy + 8, p);
        width -= 2;
        cx += 16;
    }

    // draw right side
    cy = y;
    p = Draw_CachePic ("gfx/box_tr.lmp");
    Draw_TransPic (cx, cy, p);
    p = Draw_CachePic ("gfx/box_mr.lmp");
    for (n = 0; n < lines; n++) {
        cy += 8;
        Draw_TransPic (cx, cy, p);
    }
    p = Draw_CachePic ("gfx/box_br.lmp");
    Draw_TransPic (cx, cy + 8, p);
}

/*
 * Draw_DebugChar
 *
 * Draws a single character directly to the upper right corner of the screen.
 * This is for debugging lockups by drawing different chars in different parts
 * of the code.
 */
void Draw_DebugChar (char num) {
}

/*
 * Draw_AlphaPicColor
 */
void Draw_AlphaPicColor (int x, int y, mpic_t * pic, float alpha, float color) {
//Con_Printf ("%d %d %p\n", pic->width, pic->height, pic->glt);
    glEnable (GL_BLEND);
    GL_Bind (pic->glt->texnum);
    glColor4f (color, color, color, alpha);
    glBegin (GL_QUADS);
    glTexCoord2f (pic->sl, pic->tl);
    glVertex2f (x, y);
    glTexCoord2f (pic->sh, pic->tl);
    glVertex2f (x + pic->width, y);
    glTexCoord2f (pic->sh, pic->th);
    glVertex2f (x + pic->width, y + pic->height);
    glTexCoord2f (pic->sl, pic->th);
    glVertex2f (x, y + pic->height);
    glEnd ();
    glColor3ubv (color_white);
    glDisable (GL_BLEND);
}

/*
 * Draw_AlphaPic
 */
void Draw_AlphaPic (int x, int y, mpic_t * pic, float alpha) {
    Draw_AlphaPicColor (x, y, pic, alpha, 1);
}

/*
 * Draw_AlphaSubPicColor
 */
void Draw_AlphaSubPicColor (int x, int y, mpic_t * pic, float alpha, float color, int srcx, int srcy, int width, int height) {
    float         newsl, newtl, newsh, newth;
    float         oldglwidth, oldglheight;

    oldglwidth = pic->sh - pic->sl;
    oldglheight = pic->th - pic->tl;

    newsl = pic->sl + (srcx * oldglwidth) / pic->width;
    newsh = newsl + (width * oldglwidth) / pic->width;
    newtl = pic->tl + (srcy * oldglheight) / pic->height;
    newth = newtl + (height * oldglheight) / pic->height;

    glEnable (GL_BLEND);
    GL_Bind (pic->glt->texnum);
    glColor4f (color, color, color, alpha);
    glBegin (GL_QUADS);
    glTexCoord2f (newsl, newtl);
    glVertex2f (x, y);
    glTexCoord2f (newsh, newtl);
    glVertex2f (x + width, y);
    glTexCoord2f (newsh, newth);
    glVertex2f (x + width, y + height);
    glTexCoord2f (newsl, newth);
    glVertex2f (x, y + height);
    glEnd ();
    glColor3ubv (color_white);
    glDisable (GL_BLEND);
}

/*
 * Draw_AlphaSubPic
 */
void Draw_AlphaSubPic (int x, int y, mpic_t * pic, float alpha, int srcx, int srcy, int width, int height) {
    Draw_AlphaSubPicColor (x, y, pic, alpha, 1, srcx, srcy, width, height);
}

/*
 * Draw_SubPic
 */
void Draw_SubPic (int x, int y, mpic_t * pic, int srcx, int srcy, int width, int height) {
    Draw_AlphaSubPicColor (x, y, pic, scr_sbaralpha.value, scr_sbarbrightness.value, srcx, srcy, width, height);
}

/*
 * Draw_Pic
 */
void Draw_Pic (int x, int y, mpic_t * pic) {
    Draw_AlphaPicColor (x, y, pic, scr_sbaralpha.value, scr_sbarbrightness.value);
}

void Draw_TextPic (int x, int y, mpic_t * pic) {
    Draw_AlphaPicColor (x, y, pic, scr_textalpha.value, scr_textbrightness.value);
}

void Draw_RawPic (int x, int y, mpic_t * pic) {
    GL_Bind (pic->glt->texnum);
    glBegin (GL_QUADS);
    glTexCoord2f (pic->sl, pic->tl);
    glVertex2f (x, y);
    glTexCoord2f (pic->sh, pic->tl);
    glVertex2f (x + pic->width, y);
    glTexCoord2f (pic->sh, pic->th);
    glVertex2f (x + pic->width, y + pic->height);
    glTexCoord2f (pic->sl, pic->th);
    glVertex2f (x, y + pic->height);
    glEnd ();
}

/*
 * Draw_ConsolePic
 */
void Draw_ConsolePic (int x, int y, mpic_t * pic, float alpha, float color, int width, int height) {
    float         newsl, newsh, newtl, newth, diff;
    float         newwidth, newheight;

    newwidth = width;
    newheight = height;
    if ((float) pic->width / pic->height > (float) width / height) {
        if (scr_conbacksize.value > 0) {
            diff = height - (float) height * scr_conbacksize.value;
            y += diff / 2;
            newheight = height - diff;
            newwidth = newheight / pic->height * pic->width;
            if (newwidth > width)
                newwidth = width;
            diff = width - newwidth;
            if (diff > 0)
                x += diff / 2;
        }
        diff = ((float) (pic->height * newwidth) / pic->width - newheight);
        diff = (pic->sh - pic->sl) * (1.0 - (float) newwidth / (newwidth - diff));
        newsl = pic->sl + diff / 2;
        newsh = pic->sh - diff / 2;
        newtl = pic->tl;
        newth = pic->th;
    } else {
        if (scr_conbacksize.value > 0) {
            diff = width - (float) width * scr_conbacksize.value;
            x += diff / 2;
            newwidth = width - diff;
            newheight = newwidth / pic->width * pic->height;
            if (newheight > height)
                newheight = height;
            diff = height - newheight;
            if (diff > 0)
                y += diff / 2;
        }
        diff = ((float) (pic->width * newheight) / pic->height - newwidth);
        diff = (pic->th - pic->tl) * (1.0 - (float) newheight / (newheight - diff));
        newsl = pic->sl;
        newsh = pic->sh;
        newtl = pic->tl + diff / 2;
        newth = pic->th - diff / 2;
    }

    glEnable (GL_BLEND);
    GL_Bind (pic->glt->texnum);
    glColor4f (color, color, color, alpha);
    glBegin (GL_QUADS);
    glTexCoord2f (newsl, newtl);
    glVertex2f (x, y);
    glTexCoord2f (newsh, newtl);
    glVertex2f (x + newwidth, y);
    glTexCoord2f (newsh, newth);
    glVertex2f (x + newwidth, y + newheight);
    glTexCoord2f (newsl, newth);
    glVertex2f (x, y + newheight);
    glEnd ();
    glColor3ubv (color_white);
    glDisable (GL_BLEND);
}

/*
 * Draw_TransPic
 */
void Draw_TransPic (int x, int y, mpic_t * pic) {
    if (x < 0 || (unsigned) (x + pic->width) > vid.width || y < 0 || (unsigned) (y + pic->height) > vid.height)
        Sys_DPrintf ("Draw_TransPic: bad coordinates"); // lxndr: was an error

    Draw_AlphaPicColor (x, y, pic, scr_textalpha.value, scr_textbrightness.value);     // lxndr: only used for characters ?
}

/*
 * Draw_TransPicTranslate
 *
 * Only used for the player color selection menu
 */
void Draw_TransPicTranslate (int x, int y, mpic_t * pic, byte * translation) {
    int           v, u, c, p;
    unsigned      trans[64 * 64], *dest;
    byte         *src;

    GL_Bind (translate_texture);

    c = pic->width * pic->height;

    dest = trans;
    for (v = 0; v < 64; v++, dest += 64) {
        src = &menuplyr_pixels[((v * pic->height) >> 6) * pic->width];
        for (u = 0; u < 64; u++) {
            p = src[(u * pic->width) >> 6];
            dest[u] = (p == 255) ? p : d_8to24table[translation[p]];
        }
    }

    glTexImage2D (GL_TEXTURE_2D, 0, gl_alpha_format, 64, 64, 0, GL_RGBA, GL_UNSIGNED_BYTE, trans);

    glTexParameterf (GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, scr_menusmooth.value ? GL_LINEAR : GL_NEAREST);
    glTexParameterf (GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, scr_menusmooth.value ? GL_LINEAR : GL_NEAREST);

    glEnable (GL_ALPHA_TEST);

    glBegin (GL_QUADS);
    glTexCoord2f (0, 0);
    glVertex2f (x, y);
    glTexCoord2f (1, 0);
    glVertex2f (x + pic->width, y);
    glTexCoord2f (1, 1);
    glVertex2f (x + pic->width, y + pic->height);
    glTexCoord2f (0, 1);
    glVertex2f (x, y + pic->height);
    glEnd ();

    glDisable (GL_ALPHA_TEST);
}

/*
 * Draw_TileClear
 *
 * This repeats a 64*64 tile graphic to fill the screen around a sized down
 * refresh window.
 */
void Draw_TileClear (int x, int y, int w, int h) {
    GL_Bind (draw_backtile->glt->texnum);
    glBegin (GL_QUADS);
    glTexCoord2f (x / 64.0, y / 64.0);
    glVertex2f (x, y);
    glTexCoord2f ((x + w) / 64.0, y / 64.0);
    glVertex2f (x + w, y);
    glTexCoord2f ((x + w) / 64.0, (y + h) / 64.0);
    glVertex2f (x + w, y + h);
    glTexCoord2f (x / 64.0, (y + h) / 64.0);
    glVertex2f (x, y + h);
    glEnd ();
}

/*
 * Draw_Fill
 *
 * Fills a box of pixels with a single color
 */
void Draw_Fill (int x, int y, int w, int h, int c) {
    glDisable (GL_TEXTURE_2D);

    if (game.free)
        glColor3f (c / 255.0, c / 255.0, c / 255.0);
    else
        glColor3f (host_basepal[c * 3] / 255.0, host_basepal[c * 3 + 1] / 255.0, host_basepal[c * 3 + 2] / 255.0);

    glBegin (GL_QUADS);
    glVertex2f (x, y);
    glVertex2f (x + w, y);
    glVertex2f (x + w, y + h);
    glVertex2f (x, y + h);
    glEnd ();

    glColor3ubv (color_white);

    glEnable (GL_TEXTURE_2D);
}

// 

/*
 * Draw_FadeScreen
 */
void Draw_FadeScreen (void) {
    GL_Bind (fade_texture);

    glEnable (GL_BLEND);
    glDisable (GL_TEXTURE_2D);

    glColor4f (0, 0, 0, scr_menualpha.value);

    glBegin (GL_QUADS);
    glVertex2f (0, 0);
    glVertex2f (hudwidth, 0);
    glVertex2f (hudwidth, hudheight);
    glVertex2f (0, hudheight);
    glEnd ();

    glColor3ubv (color_white);

    glDisable (GL_BLEND);
    glEnable (GL_TEXTURE_2D);

    //    Sbar_Changed ();
}

// 

/*
 * Draw_BeginDisc
 *
 * Draws the little blue disc in the corner of the screen. Call before
 * beginning any disc IO.
 */
void Draw_BeginDisc (void) {
    if (!draw_disc)
        return;

//    glDrawBuffer (GL_FRONT);
    Draw_Pic (vid.width - 24, 0, draw_disc);
//    glDrawBuffer (GL_BACK);
}

/*
 * Draw_EndDisc
 *
 * Erases the disc icon. Call after completing any disc IO
 */
void Draw_EndDisc (void) {
}

/*
 *
 * Console
 *
 */

void Draw_CharToConback (int num, byte * dest) {
    int           row, col, drawline, x;
    byte         *src;

    row = num >> 4;
    col = num & 15;
    src = draw_charset + (row << 10) + (col << 3);

    drawline = 8;

    while (drawline--) {
        for (x = 0; x < 8; x++)
            if (src[x] != 255)
                dest[x] = 0x60 + src[x];
        src += 128;
        dest += 320;
    }
}

static qboolean Draw_LoadConback (char *name) {
    mpic_t       *pic_24bit;

    if (!strcmp (name, "default"))
       return false;

    if (!(pic_24bit = GL_LoadPicImage (va ("gfx/conbacks/%s", name), name, 0, 0, TEX_CONSOLE))) {
        if (!(pic_24bit = GL_LoadPicImage (va ("gfx/%s", name), name, 0, 0, TEX_CONSOLE))) {
            Con_Warnf ("Couldn't load conback \"%s\"\n", name);
            return false;
        }
    }

    memcpy (&conback->glt, &pic_24bit->glt, sizeof (mpic_t) - 8);
    conback->width = pic_24bit->width;
    conback->height = pic_24bit->height;
    return true;
}

static void Draw_LoadDefaultConback (void) {
    int           start;
    qpic_t       *cb;

    if (!game.free) {
        start = Hunk_LowMark ();

        if (!(cb = (qpic_t *) Com_LoadHunkFile ("gfx/conback.lmp")))
            Sys_Error ("Couldn't load gfx/conback.lmp");

        SwapPic (cb);
        if (cb->width != 320 || cb->height != 200)
            Sys_Printf ("Draw_InitConback: conback.lmp size is not 320x200");       // lxndr: was an error

        conback->width = cb->width;
        conback->height = cb->height;
        GL_LoadPicTexture ("conback", conback, cb->data, TEX_CONSOLE);

        // free loaded console
        Hunk_FreeToLowMark (start);
    }
}

void Draw_InitConback (void) {
    if (!Draw_LoadConback ("conback"))
        Draw_LoadDefaultConback ();
}

/*
 * Draw_ConsoleBackground
 */
void Draw_ConsoleBackground (int lines, float xscale) {
    char          ver[80];
    float         target;
    extern float  ratiocorrection;
    extern cvar_t scr_conspeed;
    static float  alpha;

    if (scr_hudsize.value) {
        glMatrixMode (GL_PROJECTION);
        glLoadIdentity ();
        glOrtho (0, conwidth, conheight, 0, NEARVAL, FARVAL);
    }

    target = con_forcedup ? 1 : scr_conalpha.value;
    if (scr_conlines == 0)
        alpha = max (0, alpha - host_frametime * scr_conspeed.value / 1000);
    else if (alpha > target)
        alpha = max (target, alpha - host_frametime / scr_conspeed.value * 500);
    else if (alpha < target)
        alpha = min (target, alpha + host_frametime / scr_conspeed.value * 500);

    if (con_forcedup) {
        Draw_Fill (0, 0, conwidth, conheight, 0);
        Draw_ConsolePic (0, 0, conback, alpha, scr_conbrightness.value, conwidth, conheight);
    } else if (cl.intermission) {
        Draw_ConsolePic (0, 0, conback, alpha, scr_conbrightness.value, conwidth, conheight);
    } else {
        Draw_ConsolePic (0, lines - conheight, conback, alpha, scr_conbrightness.value, conwidth * xscale, conheight);
    }

    if (scr_hudsize.value) {
        glMatrixMode (GL_PROJECTION);
        glLoadIdentity ();
        glOrtho (0, hudwidth, hudheight, 0, NEARVAL, FARVAL);
    }

    sprintf (ver, "%s %s", ENGINE_NAME, ENGINE_VERSION);
    if (scr_hudsize.value) {

        if (host_initialized) { // lxndr: FIXME: charset fulgurance
            glMatrixMode (GL_PROJECTION);
            glLoadIdentity ();
            glOrtho (0, conwidth / 2.0, conheight / 2.0, 0, NEARVAL, FARVAL);
            if (game.legacy && ratiocorrection == 1)
                Draw_Alt_String (conwidth / 2.0 - strlen (ver) * 8 - 10, lines / 2.0 - 14, ver);
            else
                Draw_Alt_String (conwidth / 2.0 - strlen (ver) * 8 - 8, lines / 2.0 - 10, ver);
        }

        glMatrixMode (GL_PROJECTION);
        glLoadIdentity ();
        glOrtho (0, conwidth, conheight, 0, NEARVAL, FARVAL);

    } else if (key_dest != key_game) {
        Draw_Alt_String (conwidth - strlen (ver) * 8 - 8, lines - 10, ver);
    }
}

mpic_t       *screenshot = NULL;
void Draw_Screenshot (char *map) {
    int x, y;
    float f = 1;

    screenshot = Draw_CachePic (va ("%s-%s-00", ENGINE_FSNAME, map));
    if (!screenshot || !screenshot->glt)
        return;

    if (scr_hudsize.value) {
        f = 320.0 / screenshot->width;
        x = ((hudwidth - 320) >> 1) / f;
        y = ((hudheight - 200) / 2) / f;
        glScalef (f, f, 1);
    } else {
        x = (vid.width - screenshot->width) / 2;
        y = (vid.height - screenshot->height) / 2;
    }

    glColor4f (0.1, 0.1, 0.1, scr_textalpha.value);
    glLineWidth (1.0);

    glBegin (GL_LINE_LOOP);
    glVertex2i (x - 1, y - 1);
    glVertex2i (x + 1 + screenshot->width, y - 1);
    glVertex2i (x + 1 + screenshot->width, y + 1 + screenshot->height);
    glVertex2i (x - 1, y + 1 + screenshot->height);
    glEnd ();

    glColor3ubv (color_white);

    screenshot->sh = screenshot->th = 1;
    Draw_AlphaPic (x, y, screenshot, scr_textalpha.value);

    if (scr_hudsize.value)
        glScalef (1.0 / f, 1.0 / f, 1);
}

/*
 *
 * Fonts
 *
 */

static byte *Corrected (void) {
    int           i;
    byte         *src, *dest;  // lxndr: char > byte
    static byte   buf[128 * 256];

    // Convert the 128*128 conchars texture to 128*256 leaving empty space between
    // rows so that chars don't stumble on each other because of texture smoothing.
    // This hack costs us 64K of GL texture memory
    memset (buf, 255, sizeof (buf));
    src = draw_charset;
    dest = buf;
    for (i = 0; i < 16; i++) {
        memcpy (dest, src, 128 * 8);
        src += 128 * 8;
        dest += 128 * 8 * 2;
    }

    return (byte *) buf;
}

static qboolean Draw_LoadCharset (char *name, char *identifier, int texnum, int mode) {
    if (!strcmp (name, "default"))
       return false;

    if (!GL_LoadCharsetImage (va ("gfx/charsets/%s", name), identifier, texnum, mode)) {
        if (!GL_LoadCharsetImage (va ("gfx/%s", name), identifier, texnum, mode)) {
            Con_Warnf ("Couldn't load charset \"%s\"\n", name);
            return false;
        }
    }

    return true;
}

static void Draw_LoadDefaultCharset (char *identifier, int texnum, int mode) {
    if (!game.free)
        GL_LoadTextureWithTexnum (identifier, 128, 256, Corrected (), mode | TEX_NOLEVELS, 1, texnum);
}

void Draw_InitCharset (void) {
    int           i;

    if (!game.free) {
        draw_charset = W_GetLumpName ("conchars");
        for (i = 0; i < 256 * 64; i++)
            if (draw_charset[i] == 0)
                draw_charset[i] = 255;       // proper transparent color

        char_texturedefault = GL_LoadTexture ("default", 128, 256, Corrected (), TEX_ALPHA | TEX_CONSOLE | TEX_TEXT | TEX_NOLEVELS, 1);
        if (!char_texturedefault)
            Sys_Error ("Draw_InitCharset: Couldn't load default charset");
    }

    char_textureconsole = texture_extension_number++;
    if (!Draw_LoadCharset (scr_conchars.string, "pic:charsetconsole", char_textureconsole, TEX_ALPHA | TEX_CONSOLE | TEX_TEXT))
        Draw_LoadDefaultCharset ("pic:charsetconsole", char_textureconsole, TEX_ALPHA | TEX_CONSOLE | TEX_TEXT);

    char_texturehud = texture_extension_number++;
    if (!Draw_LoadCharset (scr_hudchars.string, "pic:charsethud", char_texturehud, TEX_ALPHA | TEX_TEXT))
        Draw_LoadDefaultCharset ("pic:charsethud", char_texturehud, TEX_ALPHA | TEX_TEXT);
}

/*
 *
 * Crosshair
 *
 */

byte         *StringToRGB (char *s) {
    byte         *col;
    static byte   rgb[4];

    Cmd_TokenizeString (s);
    if (Cmd_Argc () == 3) {
        rgb[0] = (byte) Q_atoi (Cmd_Argv (0));
        rgb[1] = (byte) Q_atoi (Cmd_Argv (1));
        rgb[2] = (byte) Q_atoi (Cmd_Argv (2));
    } else {
        col = (byte *) & d_8to24table[(byte) Q_atoi (s)];
        rgb[0] = col[0];
        rgb[1] = col[1];
        rgb[2] = col[2];
    }
    rgb[3] = 255;

    return rgb;
}

/*
 * Draw_Crosshair -- joe, from FuhQuake
 */
void Draw_Crosshair (void) {
    float         x, y, ofs1, ofs2, sh, th, sl, tl;
    byte         *col;
    extern vrect_t scr_vrect;

    x = (scr_vrect.x + scr_vrect.width / 2) * hudwidth / vid.width;
    y = (scr_vrect.y + scr_vrect.height / 2) * hudheight / vid.height;

    if ((crosshair.value > 1 && crosshair.value <= NUMCROSSHAIRS + 1) || crosshairimage_loaded) {
        col = StringToRGB (crosshaircolor.string);

        if (crosshairimage_loaded) {
            GL_Bind (crosshairpic.glt->texnum);
            ofs1 = 4 - 4.0 / crosshairpic.width;
            ofs2 = 4 + 4.0 / crosshairpic.width;
            sh = crosshairpic.sh;
            sl = crosshairpic.sl;
            th = crosshairpic.th;
            tl = crosshairpic.tl;
        } else {
            GL_Bind (crosshairtextures[(int) crosshair.value - 2]);
            ofs1 = 3.5;
            ofs2 = 4.5;
            tl = sl = 0;
            sh = th = 1;
        }

        glEnable (GL_BLEND);
        if (scr_crosshairalpha.value) {
            col[3] = bound (0, scr_crosshairalpha.value, 1) * 255;
            glColor3ubv (col);
        } else {
            glTexEnvf (GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);
            glColor3ubv (col);
        }

        glBegin (GL_QUADS);
        glTexCoord2f (sl, tl);
        glVertex2f (x - ofs1, y - ofs1);
        glTexCoord2f (sh, tl);
        glVertex2f (x + ofs2, y - ofs1);
        glTexCoord2f (sh, th);
        glVertex2f (x + ofs2, y + ofs2);
        glTexCoord2f (sl, th);
        glVertex2f (x - ofs1, y + ofs2);
        glEnd ();

        if (scr_crosshairalpha.value) {
        } else {
            glTexEnvf (GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_REPLACE);
        }
        glDisable (GL_BLEND);

        glColor3ubv (color_white);
    } else if (crosshair.value) {
        Draw_Character (x - 4, y - 4, '+');
    }
}

/*
 * Draw_Init
 */
void Draw_Init (void) {
    int           i;

    Cvar_Register (&gl_vbo);
    Cvar_Register (&gl_vbomapped);

    Cvar_Register (&scr_conalpha);
    Cvar_Register (&scr_conback);
    Cvar_Register (&scr_conbacksize);
    Cvar_Register (&scr_conbrightness);
    Cvar_Register (&scr_conchars);
    Cvar_Register (&scr_consmooth);
    Cvar_Register (&scr_crosshairalpha);
    Cvar_Register (&scr_crosshairimage);
    Cvar_Register (&scr_crosshairsmooth);
    Cvar_Register (&scr_hudchars);
    Cvar_Register (&scr_hudsmooth);
    Cvar_Register (&scr_menualpha);
    Cvar_Register (&scr_menusmooth);
    Cvar_Register (&scr_sbaralpha);
    Cvar_Register (&scr_sbarbrightness);
    Cvar_Register (&scr_sbarsmooth);
    Cvar_Register (&scr_textalpha);
    Cvar_Register (&scr_textbrightness);
    Cvar_Register (&scr_textsmooth);
    Cvar_Register (&scr_weaponsmooth);

    Cmd_AddCommandSyn ("loadcharset", "scr_hudchars");
    Cmd_AddCommandSyn ("loadconchars", "scr_conchars");
    Cmd_AddCommandSyn ("loadconback", "scr_conback");
    Cmd_AddCommandSyn ("loadcrossair", "crosshairimage");
    Cmd_AddCommandSyn ("loadhudchars", "scr_hudchars");

    Draw_InitCharset ();
    Draw_InitConback ();

    translate_texture = texture_extension_number++;
    fade_texture = texture_extension_number++;

    // Load the crosshair pics
    for (i = 0; i < NUMCROSSHAIRS; i++)
        crosshairtextures[i] = GL_LoadTexture (va ("crosshair%d", i), 8, 8, crosshairdata[i], TEX_ALPHA | TEX_CROSSHAIR, 1);

    if (game.free) {
        draw_null.width = draw_null.height = 8;
        GL_LoadPicTexture ("nopic", &draw_null, nulldata, 1);
    }

    // get the other pics we need
    draw_disc = Draw_PicFromWad ("disc");
    draw_backtile = Draw_PicFromWad ("backtile");
}

inline void GL_FillArray2f (float **a, float v0, float v1) {
    (*a)[0] = v0;
    (*a)[1] = v1;
    *a += 2;
}

inline void GL_FillArray3f (float **a, float v0, float v1, float v2) {
    (*a)[0] = v0;
    (*a)[1] = v1;
    (*a)[2] = v2;
    (*a) += 3;
}

inline void GL_FillArray4f (float **a, float v0, float v1, float v2, float v3) {
    (*a)[0] = v0;
    (*a)[1] = v1;
    (*a)[2] = v2;
    (*a)[3] = v3;
    *a += 4;
}

inline void GL_FillArray8f (float *a, float v0, float v1, float v2, float v3, float v4, float v5, float v6, float v7) {
    a[0] = v0;
    a[1] = v1;
    a[2] = v2;
    a[3] = v3;
    a[4] = v4;
    a[5] = v5;
    a[6] = v6;
    a[7] = v7;
}

inline void GL_FillArray12f (float *a, float v0, float v1, float v2, float v3, float v4, float v5, float v6, float v7, float v8, float v9, float v10,
                             float v11) {
    a[0] = v0;
    a[1] = v1;
    a[2] = v2;
    a[3] = v3;
    a[4] = v4;
    a[5] = v5;
    a[6] = v6;
    a[7] = v7;
    a[8] = v8;
    a[9] = v9;
    a[10] = v10;
    a[11] = v11;
}

inline void GL_FillArray8i (int *a, int v0, int v1, int v2, int v3, int v4, int v5, int v6, int v7) {
    a[0] = v0;
    a[1] = v1;
    a[2] = v2;
    a[3] = v3;
    a[4] = v4;
    a[5] = v5;
    a[6] = v6;
    a[7] = v7;
}

inline void GL_FillArray8iTof (float *a, int v0, int v1, int v2, int v3, int v4, int v5, int v6, int v7) {
    a[0] = (float) v0;
    a[1] = (float) v1;
    a[2] = (float) v2;
    a[3] = (float) v3;
    a[4] = (float) v4;
    a[5] = (float) v5;
    a[6] = (float) v6;
    a[7] = (float) v7;
}

inline float *GL_GetArrayf (void) {
    static int    index = 0;

    index++;
    if (index == MAX_VERTEXARRAY)
        index = 0;
    return vertexarrayf[index];
}

inline int   *GL_GetArrayi (void) {
    static int    index = 0;

    index++;
    if (index == MAX_VERTEXARRAY)
        index = 0;
    return vertexarrayi[index];
}

inline void GL_BindVBO (vbo_t * vbo, qboolean mapped) {
    static int    index = 0;

    index++;
    if (index == MAX_VBO)
        index = 0;
    vbo->id = vboIds[index];
    qglBindBuffer (GL_ARRAY_BUFFER_ARB, vbo->id);
    if (mapped) {
        vbo->buffer = qglMapBuffer (GL_ARRAY_BUFFER_ARB, GL_WRITE_ONLY_ARB);    // lxndr: get NULL if already mapped
        vbo->mapped = true;
    } else {
        vbo->buffer = GL_GetArrayf ();
        vbo->mapped = false;
    }
    vbo->p = vbo->buffer;
}

static inline void GL_UploadVBO (vbo_t * vbo, int size) {
    qglBindBuffer (GL_ARRAY_BUFFER_ARB, vbo->id);
    if (vbo->mapped)
        qglUnmapBuffer (GL_ARRAY_BUFFER_ARB);
    else
        qglBufferSubData (GL_ARRAY_BUFFER_ARB, 0, sizeof (float) * size, vbo->buffer);
}

static inline void GL_SetColor (vbo_t * vbo, int size) {
    qglBindBuffer (GL_ARRAY_BUFFER_ARB, vbo->id);
    glColorPointer (size, GL_FLOAT, 0, 0);
}

static inline void GL_SetTexCoord (vbo_t * vbo, int size) {
    qglBindBuffer (GL_ARRAY_BUFFER_ARB, vbo->id);
    qglClientActiveTexture (vbo->target);
    glTexCoordPointer (size, GL_FLOAT, 0, 0);
}

static inline void GL_SetVertex (vbo_t * vbo, int size) {
    qglBindBuffer (GL_ARRAY_BUFFER_ARB, vbo->id);
    glVertexPointer (size, GL_FLOAT, 0, 0);
}

inline void GL_PolyVertex3f (vbo_t * vertex, count_t * c) {
    int           i;
    GL_UploadVBO (vertex, c->sumverts * 3);
    glEnableClientState (GL_VERTEX_ARRAY);
    for (i = 0; i < c->primitives; i++) {
        GL_SetVertex (vertex, 3);
//    qglMultiDrawArrays (GL_POLYGON, c->primoffset, c->primverts, c->primitives);
        glDrawArrays (GL_POLYGON, c->primoffset[i], c->primverts[i]);
    }
    glDisableClientState (GL_VERTEX_ARRAY);
    qglBindBuffer (GL_ARRAY_BUFFER_ARB, 0);
}

inline void GL_PolyTexCoord2fVertex3f (vbo_t * texcoord, vbo_t * vertex, count_t * c) {
    int           i;
    GL_UploadVBO (texcoord, c->sumverts * 2);
    GL_UploadVBO (vertex, c->sumverts * 3);
    glEnableClientState (GL_TEXTURE_COORD_ARRAY);
    glEnableClientState (GL_VERTEX_ARRAY);
    for (i = 0; i < c->primitives; i++) {
        GL_SetTexCoord (texcoord, 2);
        GL_SetVertex (vertex, 3);
//    qglMultiDrawArrays (GL_POLYGON, c->primoffset, c->primverts, c->primitives);
        glDrawArrays (GL_POLYGON, c->primoffset[i], c->primverts[i]);
    }
    glDisableClientState (GL_TEXTURE_COORD_ARRAY);
    glDisableClientState (GL_VERTEX_ARRAY);
    qglBindBuffer (GL_ARRAY_BUFFER_ARB, 0);
}

inline void GL_Poly2TexCoord2fVertex3f (vbo_t * texcoord1, vbo_t * texcoord2, vbo_t * vertex, count_t * c) {
    int           i;
    GL_UploadVBO (texcoord1, c->sumverts * 2);
    GL_UploadVBO (texcoord2, c->sumverts * 2);
    GL_UploadVBO (vertex, c->sumverts * 3);
    glEnableClientState (GL_TEXTURE_COORD_ARRAY);
    glEnableClientState (GL_VERTEX_ARRAY);
    for (i = 0; i < c->primitives; i++) {
        GL_SetTexCoord (texcoord1, 2);
        GL_SetTexCoord (texcoord2, 2);
        GL_SetVertex (vertex, 3);
//    qglMultiDrawArrays (GL_POLYGON, c->primoffset, c->primverts, c->primitives);
        glDrawArrays (GL_POLYGON, c->primoffset[i], c->primverts[i]);
    }
    glDisableClientState (GL_TEXTURE_COORD_ARRAY);
    glDisableClientState (GL_VERTEX_ARRAY);
    qglBindBuffer (GL_ARRAY_BUFFER_ARB, 0);
}

inline void GL_Poly3TexCoord2fVertex3f (vbo_t * texcoord1, vbo_t * texcoord2, vbo_t * texcoord3, vbo_t * vertex, count_t * c) {
    int           i;
    GL_UploadVBO (texcoord1, c->sumverts * 2);
    GL_UploadVBO (texcoord2, c->sumverts * 2);
    GL_UploadVBO (texcoord3, c->sumverts * 2);
    GL_UploadVBO (vertex, c->sumverts * 3);
    glEnableClientState (GL_TEXTURE_COORD_ARRAY);
    glEnableClientState (GL_VERTEX_ARRAY);
    for (i = 0; i < c->primitives; i++) {
        GL_SetTexCoord (texcoord1, 2);
        GL_SetTexCoord (texcoord2, 2);
        GL_SetTexCoord (texcoord3, 2);
        GL_SetVertex (vertex, 3);
//    qglMultiDrawArrays (GL_POLYGON, c->primoffset, c->primverts, c->primitives);
        glDrawArrays (GL_POLYGON, c->primoffset[i], c->primverts[i]);
    }
    glDisableClientState (GL_TEXTURE_COORD_ARRAY);
    glDisableClientState (GL_VERTEX_ARRAY);
    qglBindBuffer (GL_ARRAY_BUFFER_ARB, 0);
}

inline void GL_Tri4Color4fTexCoord2fVertex3f (vbo_t * color, vbo_t * texcoord, vbo_t * vertex, count_t * c) {
    int           i;
    GL_UploadVBO (color, c->sumverts * 4);
    GL_UploadVBO (texcoord, c->sumverts * 2);
    GL_UploadVBO (vertex, c->sumverts * 3);
    glEnableClientState (GL_COLOR_ARRAY);
    glEnableClientState (GL_TEXTURE_COORD_ARRAY);
    glEnableClientState (GL_VERTEX_ARRAY);
    for (i = 0; i < c->primitives; i++) {
        GL_SetColor (color, 4);
        GL_SetTexCoord (texcoord, 2);
        GL_SetVertex (vertex, 3);
        glDrawArrays (c->primstrip[i] ? GL_TRIANGLE_STRIP : GL_TRIANGLE_FAN, c->primoffset[i], c->primverts[i]);
    }
    glDisableClientState (GL_COLOR_ARRAY);
    glDisableClientState (GL_TEXTURE_COORD_ARRAY);
    glDisableClientState (GL_VERTEX_ARRAY);
    qglBindBuffer (GL_ARRAY_BUFFER_ARB, 0);
}

inline void GL_Tri4Color4f2TexCoord2fVertex3f (vbo_t * color, vbo_t * texcoord1, vbo_t * texcoord2, vbo_t * vertex, count_t * c) {
    int           i;
    GL_UploadVBO (color, c->sumverts * 4);
    GL_UploadVBO (texcoord1, c->sumverts * 2);
    GL_UploadVBO (texcoord2, c->sumverts * 2);
    GL_UploadVBO (vertex, c->sumverts * 3);
    glEnableClientState (GL_COLOR_ARRAY);
    glEnableClientState (GL_TEXTURE_COORD_ARRAY);
    glEnableClientState (GL_VERTEX_ARRAY);
    for (i = 0; i < c->primitives; i++) {
        GL_SetColor (color, 4);
        GL_SetTexCoord (texcoord1, 2);
        GL_SetTexCoord (texcoord2, 2);
        GL_SetVertex (vertex, 3);
        glDrawArrays (c->primstrip[i] ? GL_TRIANGLE_STRIP : GL_TRIANGLE_FAN, c->primoffset[i], c->primverts[i]);
    }
    glDisableClientState (GL_COLOR_ARRAY);
    glDisableClientState (GL_TEXTURE_COORD_ARRAY);
    glDisableClientState (GL_VERTEX_ARRAY);
    qglBindBuffer (GL_ARRAY_BUFFER_ARB, 0);
}

inline void GL_QuadColor3fVertex2fTexcoord2f (float *color, float *vertex, float *texcoord, int pass) {
    int           i;

    if (gl_vertexarrays.value) {
        glEnableClientState (GL_COLOR_ARRAY);
        glEnableClientState (GL_TEXTURE_COORD_ARRAY);
        glEnableClientState (GL_VERTEX_ARRAY);
        glColorPointer (3, GL_FLOAT, 0, color);
        glTexCoordPointer (2, GL_FLOAT, 0, texcoord);
        glVertexPointer (2, GL_FLOAT, 0, vertex);
        glDrawArrays (GL_QUADS, 0, pass);
        glDisableClientState (GL_COLOR_ARRAY);
        glDisableClientState (GL_TEXTURE_COORD_ARRAY);
        glDisableClientState (GL_VERTEX_ARRAY);
    } else {
        glBegin (GL_QUADS);
        for (i = 0; i < pass; i++) {
            glColor3f (color[i * 3], color[i * 3 + 1], color[i * 3 + 2]);
            glTexCoord2f (texcoord[i * 2], texcoord[i * 2 + 1]);
            glVertex2f (vertex[i * 2], vertex[i * 2 + 1]);
        }
        glEnd ();
    }
}

inline void GL_QuadVertex2fTexcoord2f (float *vertex, float *texcoord, int pass) {
    int           i;

    if (gl_vertexarrays.value) {
        glEnableClientState (GL_TEXTURE_COORD_ARRAY);
        glEnableClientState (GL_VERTEX_ARRAY);
        glTexCoordPointer (2, GL_FLOAT, 0, texcoord);
        glVertexPointer (2, GL_FLOAT, 0, vertex);
        glDrawArrays (GL_QUADS, 0, pass);
        glDisableClientState (GL_TEXTURE_COORD_ARRAY);
        glDisableClientState (GL_VERTEX_ARRAY);
    } else {
        glBegin (GL_QUADS);
        for (i = 0; i < pass * 2; i += 2) {
            glTexCoord2f (texcoord[i], texcoord[i + 1]);
            glVertex2f (vertex[i], vertex[i + 1]);
        }
        glEnd ();
    }
}
