/*
 * Copyright (C) 1996-1997 Id Software, Inc.
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * 
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 * 
 */

#include "quakedef.h"

#  include <errno.h>
#  include <fcntl.h>
//#include <sys/types.h>
#  include <sys/ioctl.h>
#  include <sys/mman.h>
#  include <sys/shm.h>
//#include <sys/wait.h>
#  include <linux/soundcard.h>

int           audio_fd, snd_inited;

qboolean SNDDMA_InitOSS (void) {
    int           rc, fmt, tmp, i, caps;
    struct audio_buf_info info;

    snd_inited = 0;

// open /dev/dsp, confirm capability to mmap, and get size of dma buffer

    audio_fd = open (s_device.string, O_RDWR);
    if (audio_fd < 0) {
        Con_Errorf ("OSS: could not open '%s'. %s\n", s_device.string, strerror (errno));
        return 0;
    }

    rc = ioctl (audio_fd, SNDCTL_DSP_RESET, 0);
    if (rc < 0) {
        Con_Errorf ("OSS: could not reset %s. %s\n", s_device.string, strerror (errno));
        close (audio_fd);
        return 0;
    }

    if (ioctl (audio_fd, SNDCTL_DSP_GETCAPS, &caps) == -1) {
        Con_Errorf ("OSS: sound driver too old. %s\n", strerror (errno));
        close (audio_fd);
        return 0;
    }

    if (!(caps & DSP_CAP_TRIGGER) || !(caps & DSP_CAP_MMAP)) {
        Con_Errorf ("OSS: sorry but your soundcard can't do this\n");
        close (audio_fd);
        return 0;
    }

    if (ioctl (audio_fd, SNDCTL_DSP_GETOSPACE, &info) == -1) {
        Con_Errorf ("OSS: Um, can't do GETOSPACE?. %s\n", strerror (errno));
        close (audio_fd);
        return 0;
    }

    shm = &sn;
    shm->splitbuffer = 0;

    // Check for user-specified parameters...
    if (s_bits.value) {
        shm->samplebits = s_bits.value;
        if (shm->samplebits != 24 && shm->samplebits != 16 && shm->samplebits != 8) {
            Con_Errorf ("ALSA: invalid sample bits: %d\n", shm->samplebits);
            return 0;
        }
    } else {
        ioctl (audio_fd, SNDCTL_DSP_GETFMTS, &fmt);
        if (fmt & AFMT_S16_LE)
            shm->samplebits = 16;
        else if (fmt & AFMT_U8)
            shm->samplebits = 8;
    }
    if (s_rate.value) {
        shm->speed = s_rate.value;
        if (shm->speed != 96000 && shm->speed != 48000 && shm->speed != 44100 && shm->speed != 22050 && shm->speed != 11025) {
            Con_Errorf ("ALSA: invalid sample rate: %d\n", shm->speed);
            return 0;
        }
    } else {
        for (i = 0; i < sizeof (s_rates) / 4; i++)
            if (!ioctl (audio_fd, SNDCTL_DSP_SPEED, &s_rates[i]))
                break;
        shm->speed = s_rates[i];
    }
    shm->channels = s_channels.value;

    shm->samples = info.fragstotal * info.fragsize / (shm->samplebits / 8);
    shm->submission_chunk = 1;

// memory map the dma buffer
    shm->buffer = (unsigned char *) mmap (NULL, info.fragstotal * info.fragsize, PROT_WRITE, MAP_FILE | MAP_SHARED, audio_fd, 0);
    if (!shm->buffer || shm->buffer == (unsigned char *) -1) {
        Con_Errorf ("OSS: could not mmap %s. %s\n", s_device.string, strerror (errno));
        close (audio_fd);
        return 0;
    }

    tmp = 0;
    if (shm->channels == 2)
        tmp = 1;
    rc = ioctl (audio_fd, SNDCTL_DSP_STEREO, &tmp);
    if (rc < 0) {
        Con_Errorf ("OSS: could not set %s to stereo=%d. %s\n", s_device.string, shm->channels, strerror (errno));
        close (audio_fd);
        return 0;
    }

    if (tmp)
        shm->channels = 2;
    else
        shm->channels = 1;

    rc = ioctl (audio_fd, SNDCTL_DSP_SPEED, &shm->speed);
    if (rc < 0) {
        Con_Errorf ("OSS: could not set %s speed to %d. %s\n", s_device.string, shm->speed, strerror (errno));
        close (audio_fd);
        return 0;
    }

    if (shm->samplebits == 16) {
        rc = AFMT_S16_LE;
        rc = ioctl (audio_fd, SNDCTL_DSP_SETFMT, &rc);
        if (rc < 0) {
            Con_Errorf ("OSS: could not support 16-bit data. Try 8-bit. %s\n", strerror (errno));
            close (audio_fd);
            return 0;
        }
    } else if (shm->samplebits == 8) {
        rc = AFMT_U8;
        rc = ioctl (audio_fd, SNDCTL_DSP_SETFMT, &rc);
        if (rc < 0) {
            Con_Errorf ("OSS: could not support 8-bit data. %s\n", strerror (errno));
            close (audio_fd);
            return 0;
        }
    } else {
        Con_Errorf ("OSS: %d-bit sound not supported.\n", shm->samplebits);
        close (audio_fd);
        return 0;
    }

// toggle the trigger & start her up
    tmp = 0;
    rc = ioctl (audio_fd, SNDCTL_DSP_SETTRIGGER, &tmp);
    if (rc < 0) {
        Con_Errorf ("OSS: could not toggle. %s\n", strerror (errno));
        close (audio_fd);
        return 0;
    }

    tmp = PCM_ENABLE_OUTPUT;
    rc = ioctl (audio_fd, SNDCTL_DSP_SETTRIGGER, &tmp);
    if (rc < 0) {
        Con_Errorf ("OSS: ould not toggle. %s\n", strerror (errno));
        close (audio_fd);
        return 0;
    }

    shm->samplepos = 0;

    snd_inited = 1;
    return 1;
}

int SNDDMA_GetDMAPosOSS (void) {
    struct count_info count;

    if (!snd_inited)
        return 0;

    if (ioctl (audio_fd, SNDCTL_DSP_GETOPTR, &count) == -1) {
        Con_Errorf ("OSS: Uh, sound dead. %s\n", strerror (errno));
        close (audio_fd);
        snd_inited = 0;
        return 0;
    }
//      shm->samplepos = (count.bytes / (shm->samplebits / 8)) & (shm->samples-1);
//      fprintf(stderr, "%d    \r", count.ptr);
    shm->samplepos = count.ptr / (shm->samplebits / 8);

    return shm->samplepos;
}

void SNDDMA_ShutdownOSS (void) {
    if (snd_inited) {
        close (audio_fd);
        snd_inited = 0;
    }
}

/*
 * SNDDMA_SubmitOSS
 *
 * Send sound to device if buffer isn't really the dma buffer
 */
void SNDDMA_SubmitOSS (void) {
}
