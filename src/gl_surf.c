/*
 * Copyright (C) 1996-1997 Id Software, Inc.
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation; either version 2 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.
 *
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 */
// r_surf.c: surface-related refresh code

#include "quakedef.h"

#define	BLOCK_WIDTH		128
#define	BLOCK_HEIGHT		128

#define	MAX_LIGHTMAPS		256   // lxndr: was 64

static int    lightmap_textures;

unsigned      blocklights[BLOCK_WIDTH * BLOCK_HEIGHT * 3];

typedef struct glRect_s {
    unsigned char l, t, w, h;
} glRect_t;

static glpoly_t *lightmap_polys[MAX_LIGHTMAPS];
static qboolean lightmap_modified[MAX_LIGHTMAPS];
static glRect_t lightmap_rectchange[MAX_LIGHTMAPS];

static int    allocated[MAX_LIGHTMAPS][BLOCK_WIDTH];

// the lightmap texture data needs to be kept in main memory so texsubimage can update properly
byte          lightmaps[3 * MAX_LIGHTMAPS * BLOCK_WIDTH * BLOCK_HEIGHT];

msurface_t   *skychain = NULL;
msurface_t   *waterchain = NULL;
msurface_t   *alphachain = NULL;
msurface_t  **skychain_tail = &skychain;
msurface_t  **waterchain_tail = &waterchain;
msurface_t  **alphachain_tail = &alphachain;

#define CHAIN_SURF_F2B(surf, chain_tail) {	\
	*(chain_tail) = (surf);	\
	(chain_tail) = &(surf)->texturechain;\
	(surf)->texturechain = NULL;	\
}

#define CHAIN_SURF_B2F(surf, chain) {	\
	(surf)->texturechain = (chain);	\
	(chain) = (surf);	\
}

static void   R_RenderDynamicLightmaps (msurface_t * fa);
static inline void R_UploadLightMap (int lightmapnum);

qboolean      drawfullbrights = false, drawlumas = false;
glpoly_t     *fullbright_polys[MAX_GLTEXTURES];
glpoly_t     *luma_polys[MAX_GLTEXTURES];
glpoly_t     *caustics_polys = NULL;
glpoly_t     *detail_polys = NULL;

/*
 *
 * POLYS
 *
 */

static inline void R_DrawPoly (glpoly_t * p) {
    int           i;
    float        *v;

    v = p->verts[0];
    glBegin (GL_POLYGON);
    for (i = 0; i < p->numverts; i++, v += VERTEXSIZE) {
        glTexCoord2f (v[3], v[4]);
        glVertex3fv (v);
    }
    glEnd ();
}


static inline void R_DrawPolyTexname (glpoly_t * p, int texnum) {
    int           i;
    float        *v;
    gltexture_t  *glt;
    extern int    char_textureconsole;

    v = p->verts[0];
    glt = GL_FindTextureByTexnum (texnum);
    if (glt)
        for (i = 0; i < p->numverts; i++, v += VERTEXSIZE)
            Draw_StringTexture3 (v[0], v[1], v[2], glt->identifier, char_textureconsole);
}

static inline void R_DrawPolyMultitex (glpoly_t * p) {
    int           i;
    float        *v;

    v = p->verts[0];
    glBegin (GL_POLYGON);
    for (i = 0; i < p->numverts; i++, v += VERTEXSIZE) {
        qglMultiTexCoord2f (GL_TEXTURE0_ARB, v[3], v[4]);
        qglMultiTexCoord2f (GL_TEXTURE1_ARB, v[5], v[6]);
        glVertex3fv (v);
    }
    glEnd ();
}

static inline void R_DrawPolyTriangles (glpoly_t * p) {
    int           i;
    float        *v;

    v = p->verts[0];
    glBegin (GL_TRIANGLE_FAN);
    for (i = 0; i < p->numverts; i++, v += VERTEXSIZE)
        glVertex3fv (v);
    glEnd ();
}

static inline void R_FillPoly (glpoly_t * p, vbo_t * atexture, vbo_t * avertex) {
    int           i;
    float        *v;

    v = p->verts[0];
    for (i = 0; i < p->numverts; i++, v += VERTEXSIZE) {
        GL_FillArray2f (&atexture->p, v[3], v[4]);
        GL_FillArray3f (&avertex->p, v[0], v[1], v[2]);
    }
}

static inline void R_FillPolyMultitex (glpoly_t * p, vbo_t * atexture1, vbo_t * atexture2, vbo_t * avertex) {
    int           i;
    float        *v;

    v = p->verts[0];
    for (i = 0; i < p->numverts; i++, v += VERTEXSIZE) {
        GL_FillArray2f (&atexture1->p, v[3], v[4]);
        GL_FillArray2f (&atexture2->p, v[5], v[6]);
        GL_FillArray3f (&avertex->p, v[0], v[1], v[2]);
    }
}

static void R_DrawDetailPolys (void) {
    int           i;
    float        *v;
    glpoly_t     *p;

    if (!detail_polys)
        return;

    GL_Bind (detailtexture);
    glEnable (GL_BLEND);
    glBlendFunc (GL_DST_COLOR, GL_SRC_COLOR);
    glTexEnvf (GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_DECAL);

    for (p = detail_polys; p; p = p->detail_chain) {
        glBegin (GL_POLYGON);
        v = p->verts[0];
        for (i = 0; i < p->numverts; i++, v += VERTEXSIZE) {
            glTexCoord2f (v[7] * 18, v[8] * 18);
            glVertex3fv (v);
        }
        glEnd ();
    }

    glTexEnvf (GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_REPLACE);
    glBlendFunc (GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    glDisable (GL_BLEND);

    detail_polys = NULL;
}

static void R_DrawFullbrightPolys (void) {
    int           i;
    glpoly_t     *p;

    if (!drawfullbrights)
        return;

    for (i = 1; i < MAX_GLTEXTURES; i++) {
        if (!fullbright_polys[i])
            continue;
        GL_Bind (i);
        for (p = fullbright_polys[i]; p; p = p->fb_chain)
            R_DrawPoly (p);

        fullbright_polys[i] = NULL;
    }

    drawfullbrights = false;
}

static void R_DrawLightmapPolys (void) {
    int           i, j;
    float        *v;
    glpoly_t     *p;

    for (i = 0; i < MAX_LIGHTMAPS; i++) {
        if (!lightmap_polys[i])
            continue;
        GL_Bind (lightmap_textures + i);
        if (lightmap_modified[i])
            R_UploadLightMap (i);
        for (p = lightmap_polys[i]; p; p = p->chain) {
            v = p->verts[0];
            glBegin (GL_POLYGON);
            for (j = 0; j < p->numverts; j++, v += VERTEXSIZE) {
                glTexCoord2f (v[5], v[6]);
                glVertex3fv (v);
            }
            glEnd ();
        }
        lightmap_polys[i] = NULL;
    }
}

static void R_DrawLumaPolys (void) {
    int           i;
    glpoly_t     *p;

    if (!drawlumas)
        return;

    for (i = 1; i < MAX_GLTEXTURES; i++) {
        if (!luma_polys[i])
            continue;
        GL_Bind (i);
        for (p = luma_polys[i]; p; p = p->luma_chain)
            R_DrawPoly (p);
        luma_polys[i] = NULL;
    }

    drawlumas = false;
}

/*
 *
 * CHAINS
 *
 */

static void R_DrawAlphaChain (void) {
    int           k;
    msurface_t   *s;
    texture_t    *t;

    if (!alphachain)
        return;

    if (gl_ext.multitex)
        GL_EnableMultitexture ();
    glEnable (GL_ALPHA_TEST);

    for (s = alphachain; s; s = s->texturechain) {
        t = s->texinfo->texture;
        R_RenderDynamicLightmaps (s);

        GL_Bind (t->gl_texturenum);

        if (gl_ext.multitex) {
            GL_Bind (lightmap_textures + s->lightmaptexturenum);
            glTexEnvf (GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_BLEND);

            // update lightmap if its modified by dynamic lights
            k = s->lightmaptexturenum;
            if (lightmap_modified[k])
                R_UploadLightMap (k);
            R_DrawPolyMultitex (s->polys);
        } else {
            R_DrawPoly (s->polys);
        }
    }

    alphachain = NULL;

    glDisable (GL_ALPHA_TEST);
    if (gl_ext.multitex)
        GL_DisableMultitexture ();
}

/*
 * R_DrawWaterSurfaces
 */
void R_DrawWaterSurfaces (void) {
    qboolean      blend = ((r_alpha.value == 1) | (r_novis.value == 1));
    msurface_t   *s;

    if (!waterchain)
        return;

    if (blend) {
        glEnable (GL_BLEND);

//        glBlendFunc (GL_SRC_ALPHA, GL_SRC_ALPHA);
        glBlendFunc (GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
        glDepthMask (GL_FALSE);
    }

    for (s = waterchain; s; s = s->texturechain) {
        if (blend) {
            if (s->texinfo->flags & TEX_LAVA)
                glColor4f (r_colorlava.value, r_colorlava.value, r_colorlava.value, r_alphalava.value);
            else if (s->texinfo->flags & TEX_SLIME)
                glColor4f (r_colorslime.value, r_colorslime.value, r_colorslime.value, r_alphaslime.value);
            else if (s->texinfo->flags & TEX_TELEPORT)
                glColor4f (r_colorteleport.value, r_colorteleport.value, r_colorteleport.value, r_alphateleport.value);
            else
                glColor4f (r_colorwater.value, r_colorwater.value, r_colorwater.value, r_alphawater.value);
            glTexEnvf (GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);
        }

        R_DrawWaterPolys (s);

        if (blend) {
            glTexEnvf (GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_REPLACE);
            glColor3ubv (color_white);
        }
    }

    waterchain = NULL;
    waterchain_tail = &waterchain;

    if (blend) {
        glBlendFunc (GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
        glDepthMask (GL_TRUE);

        glDisable (GL_BLEND);
    }
}

// 
// Dynamic lights

typedef struct dlightinfo_s {
    int           local[2];
    int           rad;
    int           minlight;            // rad - minlight
    int           type;
} dlightinfo_t;

static dlightinfo_t dlightlist[MAX_DLIGHTS];
static int    numdlights;

/*
 * R_BuildDlightList
 */
static void R_BuildDlightList (msurface_t * surf) {
    int           lnum, i, smax, tmax, irad, iminlight, local[2], tdmin, sdmin, distmin;
    float         dist;
    vec3_t        impact;
    mtexinfo_t   *tex;
    dlightinfo_t *light;

    if (!r_dynamic.value)
        return;

    numdlights = 0;

    smax = (surf->extents[0] >> 4) + 1;
    tmax = (surf->extents[1] >> 4) + 1;
    tex = surf->texinfo;

    for (lnum = 0; lnum < MAX_DLIGHTS; lnum++) {
        if (cl_dlights[lnum].die < cl.time)
            continue;                  // dead light

        //if (!(surf->dlightbits[lnum >> 5] & (1 << (lnum & 31))))
        if (!(surf->dlightbits[(lnum >> 3)] & (1 << (lnum & 7))))
            continue;                  // not lit by this light

        dist = PlaneDiff (cl_dlights[lnum].origin, surf->plane);
        irad = (cl_dlights[lnum].radius * r_dynamicradius.value - fabs (dist)) * 256;
        iminlight = cl_dlights[lnum].minlight * 256;
//Con_Printf ("%f %d %d\n", dist, irad, iminlight);
        if (irad < iminlight)
            continue;

        iminlight = irad - iminlight;

        for (i = 0; i < 3; i++)
            impact[i] = cl_dlights[lnum].origin[i] - surf->plane->normal[i] * dist;

        local[0] = DotProduct (impact, tex->vecs[0]) + tex->vecs[0][3] - surf->texturemins[0];
        local[1] = DotProduct (impact, tex->vecs[1]) + tex->vecs[1][3] - surf->texturemins[1];

        // check if this dlight will touch the surface
        if (local[1] > 0) {
            tdmin = local[1] - (tmax << 4);
            if (tdmin < 0)
                tdmin = 0;
        } else {
            tdmin = -local[1];
        }

        if (local[0] > 0) {
            sdmin = local[0] - (smax << 4);
            if (sdmin < 0)
                sdmin = 0;
        } else {
            sdmin = -local[0];
        }

        distmin = (sdmin > tdmin) ? (sdmin << 8) + (tdmin << 7) : (tdmin << 8) + (sdmin << 7);
        if (distmin < iminlight) {
            // save dlight info
            light = &dlightlist[numdlights];
            light->minlight = iminlight;
            light->rad = irad;
            light->local[0] = local[0];
            light->local[1] = local[1];
            light->type = cl_dlights[lnum].type;
            numdlights++;
        }
    }
}

/*
 * R_AddDynamicLights
 *
 * NOTE: R_BuildDlightList must be called first!
 */
static inline void R_AddDynamicLights (msurface_t * surf) {
    int           i, j, smax, tmax, s, t, sd, td, _sd, _td, irad, idist, iminlight, color[3], tmp;
    unsigned     *dest;
    dlightinfo_t *light;

    if (!r_dynamic.value)
        return;

    smax = (surf->extents[0] >> 4) + 1;
    tmax = (surf->extents[1] >> 4) + 1;

    for (i = 0, light = dlightlist; i < numdlights; i++, light++) {
        if (light->type == lt_explosion2 || light->type == lt_explosion3) {
            for (j = 0; j < 3; j++)
                color[j] = (int) (explosioncolor[j] * 255 * r_dynamicintensity.value);
        } else {
            for (j = 0; j < 3; j++)
                color[j] = (int) (dlightcolor[light->type][j] * 255 * r_dynamicintensity.value);
        }

        irad = light->rad;
        iminlight = light->minlight;

        _td = light->local[1];
        dest = blocklights;
        for (t = 0; t < tmax; t++) {
            td = _td;
            if (td < 0)
                td = -td;
            _td -= 16;
            _sd = light->local[0];

            for (s = 0; s < smax; s++) {
                sd = _sd < 0 ? -_sd : _sd;
                _sd -= 16;
                idist = (sd > td) ? (sd << 8) + (td << 7) : (td << 8) + (sd << 7);
                if (idist < iminlight) {
                    tmp = (irad - idist) >> 7;
                    dest[0] += tmp * color[0];
                    dest[1] += tmp * color[1];
                    dest[2] += tmp * color[2];
                }
                dest += 3;
            }
        }
    }
}

/*
 * R_BuildLightMap
 *
 * Combine and scale multiple lightmaps into the 8.8 format in blocklights
 */
#define lm(b, i, s, o) ((i - ((b >> (10 - s)) + (b >> (11 + s)))) * (o / 2.0 + 0.5001))
static inline void R_BuildLightMap (msurface_t * surf, byte * dest, int stride) {
    int           smax, tmax, i, j, t, size, maps, blocksize;
    byte         *lightmap;
    unsigned      scale, *bl;
    float         shadows, shadowsmax, shadowsmin, shadowsintensity;
    int           shadowssize;

    surf->cached_dlight = !!numdlights;

    smax = (surf->extents[0] >> 4) + 1;
    tmax = (surf->extents[1] >> 4) + 1;
    size = smax * tmax;
    blocksize = size * 3;
    lightmap = surf->samples;

    if (r_fullbright.value || !cl.worldmodel->lightdata) { // set to full bright if no light data
        for (i = 0; i < blocksize; i++)
            blocklights[i] = 255 * 256;
        goto store;
    }
    memset (blocklights, 0, blocksize * sizeof (unsigned int)); // clear to no light

    if (lightmap) { // add all the lightmaps
        for (maps = 0; maps < MAXLIGHTMAPS && surf->styles[maps] != 255; maps++) {
            scale = d_lightstylevalue[surf->styles[maps]];
            surf->cached_light[maps] = scale;   // 8.8 fraction
            bl = blocklights;
            for (i = 0; i < blocksize; i++)
                *bl++ += lightmap[i] * scale;
            lightmap += blocksize;     // skip to next lightmap
        }
    }
    if (numdlights) // add all the dynamic lights
        R_AddDynamicLights (surf);

  store: // bound, invert, and shift
    bl = blocklights;
    stride -= smax * 3;
    if (r_shadows.value) {
        shadows = r_shadowsworld.value * 0.1;
        shadowsmax = 255 - r_shadowsmax.value * 255;
        shadowsmin = 255 - r_shadowsmin.value * 255;
        shadowssize = 1 + r_shadowssize.value - (gl_overbright.value ? gl_overbrightworld.value : 0);
        shadowsintensity = 255 * (1.5 + r_shadowsintensity.value);
    } else {
        shadows = 0;
        shadowsmax = 0;
        shadowsmin = 255;
        shadowssize = 2;
        shadowsintensity = 255 * 2.0;
    }
    for (i = 0; i < tmax; i++, dest += stride) {
        for (j = smax; j; j--) {
            t = lm (bl[0], shadowsintensity, shadowssize, shadows);
            dest[0] = bound (shadowsmax, t, shadowsmin);

            t = lm (bl[1], shadowsintensity, shadowssize, shadows);
            dest[1] = bound (shadowsmax, t, shadowsmin);

            t = lm (bl[2], shadowsintensity, shadowssize, shadows);
            dest[2] = bound (shadowsmax, t, shadowsmin);

            bl += 3;
            dest += 3;
        }
    }
}

static inline void R_UploadLightMap (int lightmapnum) {
    glRect_t     *theRect;

    lightmap_modified[lightmapnum] = false;
    theRect = &lightmap_rectchange[lightmapnum];
    glTexSubImage2D (GL_TEXTURE_2D, 0, 0, theRect->t, BLOCK_WIDTH, theRect->h, GL_RGB, GL_UNSIGNED_BYTE,
                     lightmaps + (lightmapnum * BLOCK_HEIGHT + theRect->t) * BLOCK_WIDTH * 3);
    theRect->l = BLOCK_WIDTH;
    theRect->t = BLOCK_HEIGHT;
    theRect->h = 0;
    theRect->w = 0;
}

void R_ResetLightmaps (void) {
    int           i, j, k;
    model_t      *m;
    qboolean      nodyn = !r_dynamic.value;

    if (nodyn)
        Cvar_SetValue (&r_dynamic, 1);

    for (i = 0; i < MAX_LIGHTMAPS; i++)
        lightmap_modified[i] = true;

    for (j = 1; j < MAX_MODELS; j++) {
        if (!(m = cl.model_precache[j]) || m->name[0] == '*')
            break;

        for (i = 0; i < m->numsurfaces; i++) {
            if (m->surfaces[i].flags & (SURF_DRAWTURB | SURF_DRAWSKY))
                continue;

            for (k = 0; k < MAXLIGHTMAPS; k++)
                m->surfaces[i].cached_light[k] = !d_lightstylevalue[m->surfaces[i].styles[k]];

            R_RenderDynamicLightmaps (m->surfaces + i);
        }
    }

    if (nodyn)
        Cvar_SetValue (&r_dynamic, 0);
}

/*
 * R_TextureAnimation
 *
 * Returns the proper texture for a given time and base texture
 */
static texture_t *R_TextureAnimation (texture_t * base) {
    int           relative, count;

    if (currententity->frame) {
        if (base->alternate_anims)
            base = base->alternate_anims;
    }

    if (!base->anim_total)
        return base;

    relative = (int) (cl.time * 10) % base->anim_total;

    count = 0;
    while (base->anim_min > relative || base->anim_max <= relative) {
        base = base->anim_next;
        if (!base)
            Sys_Error ("R_TextureAnimation: broken cycle");
        if (++count > 100)
            Sys_Error ("R_TextureAnimation: infinite cycle");
    }

    return base;
}

/*
 *
 *
 * BRUSH MODELS
 *
 *
 */

/*
 * R_RenderDynamicLightmaps
 */
static void R_RenderDynamicLightmaps (msurface_t * fa) {
    int           maps, smax, tmax;
    byte         *base;
    glRect_t     *theRect;
    qboolean      lightstyle_modified = false;

    if (!r_dynamic.value)
        return;

    c_oldbrush_polys++;

    // check for lightmap modification
    for (maps = 0; maps < MAXLIGHTMAPS && fa->styles[maps] != 255; maps++) {
        if (d_lightstylevalue[fa->styles[maps]] != fa->cached_light[maps]) {
            lightstyle_modified = true;
            break;
        }
    }

    if (fa->dlightframe == r_framecount)
        R_BuildDlightList (fa);
    else
        numdlights = 0;

    if (numdlights == 0 && !fa->cached_dlight && !lightstyle_modified)
        return;

    lightmap_modified[fa->lightmaptexturenum] = true;
    theRect = &lightmap_rectchange[fa->lightmaptexturenum];
    if (fa->light_t < theRect->t) {
        if (theRect->h)
            theRect->h += theRect->t - fa->light_t;
        theRect->t = fa->light_t;
    }
    if (fa->light_s < theRect->l) {
        if (theRect->w)
            theRect->w += theRect->l - fa->light_s;
        theRect->l = fa->light_s;
    }
    smax = (fa->extents[0] >> 4) + 1;
    tmax = (fa->extents[1] >> 4) + 1;
    if (theRect->w + theRect->l < fa->light_s + smax)
        theRect->w = fa->light_s - theRect->l + smax;
    if (theRect->h + theRect->t < fa->light_t + tmax)
        theRect->h = fa->light_t - theRect->t + tmax;
    base = lightmaps + fa->lightmaptexturenum * BLOCK_WIDTH * BLOCK_HEIGHT * 3;
    base += (fa->light_t * BLOCK_WIDTH + fa->light_s) * 3;
    R_BuildLightMap (fa, base, BLOCK_WIDTH * 3);
}

static void R_ClearTextureChains (model_t * clmodel) {
    int           i, waterline;
    texture_t    *texture;

    memset (lightmap_polys, 0, sizeof (lightmap_polys));
    memset (fullbright_polys, 0, sizeof (fullbright_polys));
    memset (luma_polys, 0, sizeof (luma_polys));

    for (i = 0; i < clmodel->numtextures; i++) {
        for (waterline = 0; waterline < 2; waterline++) {
            if ((texture = clmodel->textures[i])) {
                texture->texturechain[waterline] = NULL;
                texture->texturechain_tail[waterline] = &texture->texturechain[waterline];
            }
        }
    }

    r_notexture_mip->texturechain[0] = NULL;
    r_notexture_mip->texturechain_tail[0] = &r_notexture_mip->texturechain[0];
    r_notexture_mip->texturechain[1] = NULL;
    r_notexture_mip->texturechain_tail[1] = &r_notexture_mip->texturechain[1];

    skychain = NULL;
    skychain_tail = &skychain;
    if (clmodel == cl.worldmodel) {
        waterchain = NULL;
        waterchain_tail = &waterchain;
    }
    alphachain = NULL;
    alphachain_tail = &alphachain;
}

static void R_SetupTextureChain (model_t * model) {
    int           i, waterline;
    msurface_t   *s;
    texture_t    *t;

    for (i = 0; i < model->numtextures; i++) {
        if (!model->textures[i] || (!model->textures[i]->texturechain[0] && !model->textures[i]->texturechain[1]))
            continue;

        t = R_TextureAnimation (model->textures[i]);

        for (waterline = 0; waterline < 2; waterline++) {
            if (!(s = model->textures[i]->texturechain[waterline]))
                continue;

            for (; s; s = s->texturechain) {
                c_brush_polys++;

                if (!gl_ext.multitex) {
                    s->polys->chain = lightmap_polys[s->lightmaptexturenum];
                    lightmap_polys[s->lightmaptexturenum] = s->polys;
                }
                if (waterline) {
                    if (gl_caustics.value && underwatertexture) {
                        s->polys->caustics_chain = caustics_polys;
                        caustics_polys = s->polys;
                    }
                } else {
                    if (gl_detail.value && detailtexture) {
                        s->polys->detail_chain = detail_polys;
                        detail_polys = s->polys;
                    }
                }
                if (t->fb_texturenum) {
                    if (t->isLumaTexture) {
                        s->polys->luma_chain = luma_polys[t->fb_texturenum];
                        luma_polys[t->fb_texturenum] = s->polys;
                        drawlumas = true;
                    } else {
                        s->polys->fb_chain = fullbright_polys[t->fb_texturenum];
                        fullbright_polys[t->fb_texturenum] = s->polys;
                        drawfullbrights = true;
                    }
                }
            }
        }
    }
}

static inline void R_SortTextureBackToFront (msurface_t * s, qboolean planeback) {
    if ((s->flags & SURF_PLANEBACK) && !planeback)
        return;                        // wrong side

    if (s->flags & SURF_DRAWSKY) {
        CHAIN_SURF_B2F (s, skychain);
    } else if (s->flags & SURF_DRAWTURB) {
        CHAIN_SURF_B2F (s, waterchain);
    } else {
        CHAIN_SURF_B2F (s, s->texinfo->texture->texturechain[(s->flags & SURF_UNDERWATER) ? 1 : 0]);
    }
}

static inline void R_SortTextureFrontToBack (msurface_t * s, qboolean planeback) {
    if ((s->flags & SURF_PLANEBACK) && !planeback)
        return;                        // wrong side

    if (s->flags & SURF_DRAWSKY) {
        CHAIN_SURF_F2B (s, skychain_tail);
    } else if (s->flags & SURF_DRAWTURB) {
        CHAIN_SURF_F2B (s, waterchain_tail);
    } else {
        CHAIN_SURF_F2B (s, s->texinfo->texture->texturechain_tail[(s->flags & SURF_UNDERWATER) ? 1 : 0]);
    }
}

static void R_DrawMultitexChain (model_t * model) {
    int           i, waterline;
    msurface_t   *s;
    texture_t    *t;

    for (i = 0; i < model->numtextures; i++) {
        if (!model->textures[i] || (!model->textures[i]->texturechain[0] && !model->textures[i]->texturechain[1]))
            continue;

        t = R_TextureAnimation (model->textures[i]);

        GL_SelectTexture (GL_TEXTURE0_ARB);
        GL_Bind (t->gl_texturenum);
        GL_SelectTexture (GL_TEXTURE1_ARB);

        for (waterline = 0; waterline < 2; waterline++) {
            if (!(s = model->textures[i]->texturechain[waterline]))
                continue;

            for (; s; s = s->texturechain) {
                GL_Bind (lightmap_textures + s->lightmaptexturenum);

                // update lightmap if its modified by dynamic lights
                R_RenderDynamicLightmaps (s);
                if (lightmap_modified[s->lightmaptexturenum])
                    R_UploadLightMap (s->lightmaptexturenum);

                R_DrawPolyMultitex (s->polys);
            }
        }
    }
}

static void R_DrawTextureChain (model_t * model) {
    int           i, waterline;
    msurface_t   *s;
    texture_t    *t;
    count_t       c;
    vbo_t         atexture, avertex;

    if (gl_ext.vertex_bufferobject && gl_vbo.value) {
        c.primverts = GL_GetArrayi ();
        c.primoffset = GL_GetArrayi ();
    }

    for (i = 0; i < model->numtextures; i++) {
        if (!model->textures[i] || (!model->textures[i]->texturechain[0] && !model->textures[i]->texturechain[1]))
            continue;

        t = R_TextureAnimation (model->textures[i]);

        GL_Bind (t->gl_texturenum);

        if (gl_ext.vertex_bufferobject && gl_vbo.value) {
            GL_BindVBO (&atexture, false);
            GL_BindVBO (&avertex, false);

            atexture.target = GL_TEXTURE0_ARB;
            c.sumverts = c.primitives = 0;
        }

        for (waterline = 0; waterline < 2; waterline++) {
            if (!(s = model->textures[i]->texturechain[waterline]))
                continue;

            for (; s; s = s->texturechain) {
                if (gl_ext.vertex_bufferobject && gl_vbo.value) {
                    c.primoffset[c.primitives] = c.sumverts;
                    c.primverts[c.primitives] = s->polys->numverts;
                    c.sumverts += s->polys->numverts;
                    c.primitives++;
                    R_FillPoly (s->polys, &atexture, &avertex);
                } else {
                    R_DrawPoly (s->polys);
                }
            }
        }

        if (gl_ext.vertex_bufferobject && gl_vbo.value)
            GL_PolyTexCoord2fVertex3f (&atexture, &avertex, &c);
    }
}

static void R_DrawTriangleChain (model_t * model) {
    int           i, waterline;
    msurface_t   *s;

    if (r_drawtris.value == 2)
        glDisable (GL_DEPTH_TEST);
    glDisable (GL_TEXTURE_2D);
    glColor3f (0.1, 0.1, 0.1);
    glLineWidth (2.0);
    glPolygonMode (GL_FRONT_AND_BACK, GL_LINE);
    glPolygonOffset (-1, -1);

    for (i = 0; i < model->numtextures; i++) {
        if (!model->textures[i] || (!model->textures[i]->texturechain[0] && !model->textures[i]->texturechain[1]))
            continue;

        for (waterline = 0; waterline < 2; waterline++) {
            if (!(s = model->textures[i]->texturechain[waterline]))
                continue;

            for (; s; s = s->texturechain)
                R_DrawPolyTriangles (s->polys);
        }
    }

    glColor3f (1, 1, 1);
    glPolygonMode (GL_FRONT_AND_BACK, GL_FILL);
    if (r_drawtris.value == 2)
        glEnable (GL_DEPTH_TEST);
    glEnable (GL_TEXTURE_2D);
}

static int    GL_LIGHTMAP_TEXTURE = 0, GL_FB_TEXTURE = 0;

static inline void R_DrawImmediate (float *v, int numverts, qboolean doMtex1, qboolean mtex_lightmaps, qboolean mtex_fbs) {
    int           k;

    glBegin (GL_POLYGON);
    if (doMtex1 && mtex_lightmaps && mtex_fbs) {
        for (k = 0; k < numverts; k++, v += VERTEXSIZE) {
            qglMultiTexCoord2f (GL_TEXTURE0_ARB, v[3], v[4]);
            qglMultiTexCoord2f (GL_LIGHTMAP_TEXTURE, v[5], v[6]);
            qglMultiTexCoord2f (GL_FB_TEXTURE, v[3], v[4]);
            glVertex3fv (v);
        }
    } else if (doMtex1 && mtex_lightmaps) {
        for (k = 0; k < numverts; k++, v += VERTEXSIZE) {
            qglMultiTexCoord2f (GL_TEXTURE0_ARB, v[3], v[4]);
            qglMultiTexCoord2f (GL_LIGHTMAP_TEXTURE, v[5], v[6]);
            glVertex3fv (v);
        }
    } else if (doMtex1 && mtex_fbs) {
        for (k = 0; k < numverts; k++, v += VERTEXSIZE) {
            qglMultiTexCoord2f (GL_TEXTURE0_ARB, v[3], v[4]);
            qglMultiTexCoord2f (GL_FB_TEXTURE, v[3], v[4]);
            glVertex3fv (v);
        }
    } else if (doMtex1) {
        for (k = 0; k < numverts; k++, v += VERTEXSIZE) {
            qglMultiTexCoord2f (GL_TEXTURE0_ARB, v[3], v[4]);
            glVertex3fv (v);
        }
    } else {
        for (k = 0; k < numverts; k++, v += VERTEXSIZE) {
            glTexCoord2f (v[3], v[4]);
            glVertex3fv (v);
        }
    }
    glEnd ();
}

/*
 * R_DrawTextureChains
 */
void R_DrawTextureChainsOld (model_t * model) {
    int           i, k, waterline;
    msurface_t   *s;
    texture_t    *t;
    qboolean      mtex_lightmaps, mtex_fbs, doMtex1, doMtex2, render_lightmaps = false;
    qboolean      draw_fbs, draw_mtex_fbs, can_mtex_lightmaps, can_mtex_fbs;

    if (gl_overbright.value && gl_overbrightlights.value) {
        can_mtex_lightmaps = gl_ext.multitex;
        can_mtex_fbs = gl_textureunits >= 3;
    } else {
        can_mtex_lightmaps = gl_textureunits >= 3;
        can_mtex_fbs = gl_ext.multitex && gl_ext.tex_envadd;
    }

    GL_DisableMultitexture ();
    glTexEnvf (GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_REPLACE);

    for (i = 0; i < model->numtextures; i++) {
        if (!model->textures[i] || (!model->textures[i]->texturechain[0] && !model->textures[i]->texturechain[1]))
            continue;

        t = R_TextureAnimation (model->textures[i]);

        // bind the world texture
        GL_SelectTexture (GL_TEXTURE0_ARB);
        GL_Bind (t->gl_texturenum);

        draw_fbs = t->isLumaTexture || (gl_overbright.value && gl_overbrightlights.value);
        draw_mtex_fbs = draw_fbs && can_mtex_fbs;

        if (gl_ext.multitex) {
            if (t->isLumaTexture && !(gl_overbright.value && gl_overbrightlights.value)) {
                if (gl_ext.tex_envadd) {
                    doMtex1 = true;
                    GL_FB_TEXTURE = GL_TEXTURE1_ARB;
                    GL_EnableTMU (GL_FB_TEXTURE);
                    glTexEnvf (GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_ADD);
                    GL_Bind (t->fb_texturenum);

                    mtex_lightmaps = can_mtex_lightmaps;
                    mtex_fbs = true;

                    if (mtex_lightmaps) {
                        doMtex2 = true;
                        GL_LIGHTMAP_TEXTURE = GL_TEXTURE2_ARB;
                        GL_EnableTMU (GL_LIGHTMAP_TEXTURE);
                        glTexEnvf (GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_BLEND);
                    } else {
                        doMtex2 = false;
                        render_lightmaps = true;
                    }
                } else {
                    GL_DisableTMU (GL_TEXTURE1_ARB);
                    render_lightmaps = true;
                    doMtex1 = doMtex2 = mtex_lightmaps = mtex_fbs = false;
                }
            } else {
                doMtex1 = true;
                GL_LIGHTMAP_TEXTURE = GL_TEXTURE1_ARB;
                GL_EnableTMU (GL_LIGHTMAP_TEXTURE);
                glTexEnvf (GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_BLEND);

                mtex_lightmaps = true;
                mtex_fbs = t->fb_texturenum && draw_mtex_fbs;

                if (mtex_fbs) {
                    doMtex2 = true;
                    GL_FB_TEXTURE = GL_TEXTURE2_ARB;
                    GL_EnableTMU (GL_FB_TEXTURE);
                    GL_Bind (t->fb_texturenum);
                    glTexEnvf (GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, t->isLumaTexture ? GL_ADD : GL_DECAL);
                } else {
                    doMtex2 = false;
                }
            }
        } else {
            render_lightmaps = true;
            doMtex1 = doMtex2 = mtex_lightmaps = mtex_fbs = false;
        }

        for (waterline = 0; waterline < 2; waterline++) {
            if (!(s = model->textures[i]->texturechain[waterline]))
                continue;

            for (; s; s = s->texturechain) {
                if (mtex_lightmaps) {
                    // bind the lightmap texture
                    GL_SelectTexture (GL_LIGHTMAP_TEXTURE);
                    GL_Bind (lightmap_textures + s->lightmaptexturenum);

                    // update lightmap if its modified by dynamic lights
                    R_RenderDynamicLightmaps (s);
                    k = s->lightmaptexturenum;
                    if (lightmap_modified[k])
                        R_UploadLightMap (k);
                } else {
                    s->polys->chain = lightmap_polys[s->lightmaptexturenum];
                    lightmap_polys[s->lightmaptexturenum] = s->polys;

                    R_RenderDynamicLightmaps (s);
                }

                R_DrawImmediate (s->polys->verts[0], s->polys->numverts, doMtex1, mtex_lightmaps, mtex_fbs);

                if (waterline) {
                    if (gl_caustics.value && underwatertexture) {
                        s->polys->caustics_chain = caustics_polys;
                        caustics_polys = s->polys;
                    }
                } else {
                    if (gl_detail.value && detailtexture) {
                        s->polys->detail_chain = detail_polys;
                        detail_polys = s->polys;
                    }
                }
                if (t->fb_texturenum && draw_fbs && !mtex_fbs) {
                    if (t->isLumaTexture) {
                        s->polys->luma_chain = luma_polys[t->fb_texturenum];
                        luma_polys[t->fb_texturenum] = s->polys;
                        drawlumas = true;
                    } else {
                        s->polys->fb_chain = fullbright_polys[t->fb_texturenum];
                        fullbright_polys[t->fb_texturenum] = s->polys;
                        drawfullbrights = true;
                    }
                }
            }
        }

        if (doMtex1)
            GL_DisableTMU (GL_TEXTURE1_ARB);
        if (doMtex2)
            GL_DisableTMU (GL_TEXTURE2_ARB);
    }

    if (gl_ext.multitex)
        GL_SelectTexture (GL_TEXTURE0_ARB);

    if (gl_overbright.value && gl_overbrightlights.value) {
        if (render_lightmaps)
            R_DrawLightmapPolys ();
        if (drawfullbrights)
            R_DrawFullbrightPolys ();
        if (drawlumas)
            R_DrawLumaPolys ();
    } else {
        if (drawlumas)
            R_DrawLumaPolys ();
        if (render_lightmaps)
            R_DrawLightmapPolys ();
        if (drawfullbrights)
            R_DrawFullbrightPolys ();
    }

    R_DrawCausticsPolys ();
    R_DrawDetailPolys ();
}

void R_DrawTextureChains (model_t * model) {
    R_SetupTextureChain (model);
    if (gl_ext.multitex) {
        GL_EnableMultitexture ();
        if (gl_ext.tex_envcombine && gl_overbright.value && gl_overbrightworld.value) {
            GL_EnableCombine (gl_overbrightworld.value * 2);
            glTexEnvi (GL_TEXTURE_ENV, GL_OPERAND0_RGB_EXT, GL_ONE_MINUS_SRC_COLOR);

            R_DrawMultitexChain (model);

            GL_DisableCombine ();
        } else {
            glTexEnvf (GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_BLEND);

            R_DrawMultitexChain (model);

            glTexEnvf (GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_REPLACE);
        }
        GL_DisableMultitexture ();
    } else {
        if (gl_fog.value)
            glDisable (GL_FOG);
//        glColor3f (1, 1, 1);
        glTexEnvf (GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_REPLACE);

        R_DrawTextureChain (model);

        glEnable (GL_BLEND);
        glDepthMask (GL_FALSE);
        if (gl_fog.value) {
            glEnable (GL_FOG);
            R_ClearFogColor ();
            glBlendFunc (GL_ZERO, GL_ONE_MINUS_SRC_COLOR);
            glBlendFunc (GL_ONE_MINUS_SRC_COLOR, GL_DST_COLOR);
        } else {
            glBlendFunc (GL_ZERO, GL_ONE_MINUS_SRC_COLOR);
        }

        R_DrawLightmapPolys ();

        if (gl_fog.value && 0) {
            R_ResetFogColor ();
            glBlendFunc (GL_ONE, GL_ONE);
//        glBlendFunc (GL_ONE_MINUS_SRC_COLOR, GL_ONE_MINUS_SRC_COLOR);
            glTexEnvf (GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);
            glColor3f (0, 0, 0);

            R_DrawTextureChain (model);

            glColor3f (1, 1, 1);
            glTexEnvf (GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_REPLACE);
        }
        glBlendFunc (GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
        glDisable (GL_BLEND);
        glDepthMask (GL_TRUE);
    }
    if (gl_overbright.value && gl_overbrightlights.value) {
        glEnable (GL_ALPHA_TEST);
        glEnable (GL_BLEND);
//        if (gl_fog.value)
//            glDisable (GL_FOG);
        glAlphaFunc (GL_GREATER, 0.500);
        if (gl_overbrightlights.value == 2) {
            glBlendFunc (GL_SRC_COLOR, GL_ONE);
        } else if (gl_ext.blend_color && gl_overbrightlights.value < 1) {
            qglBlendColor (gl_overbrightlights.value, gl_overbrightlights.value, gl_overbrightlights.value, gl_overbrightlights.value);
            glBlendFunc (GL_CONSTANT_COLOR_EXT, GL_ONE);
        } else {
            glBlendFunc (GL_ONE, GL_ONE);
        }
        glDepthMask (GL_FALSE);        // don't bother writing Z

        R_DrawFullbrightPolys ();
        R_DrawLumaPolys ();

        glAlphaFunc (GL_GREATER, 0.000);
        glBlendFunc (GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
        glDepthMask (GL_TRUE);
        glDisable (GL_ALPHA_TEST);
        glDisable (GL_BLEND);
//        if (gl_fog.value)
//            glEnable (GL_FOG);
    }
    R_DrawCausticsPolys ();
    R_DrawDetailPolys ();
}

/*
 * R_DrawBrushModel
 */
void R_DrawBrushModel (entity_t * ent) {
    int           i, k;
    float         dot;
    vec3_t        mins, maxs;
    msurface_t   *surf;
    mplane_t     *plane;
    model_t      *clmodel = ent->model;
    qboolean      rotated;

    currenttexture = -1;

    if (ent->angles[0] || ent->angles[1] || ent->angles[2]) {
        rotated = true;
        if (R_CullSphere (ent->origin, clmodel->radius))
            return;
    } else {
        rotated = false;
        VectorAdd (ent->origin, clmodel->mins, mins);
        VectorAdd (ent->origin, clmodel->maxs, maxs);

        if (R_CullBox (mins, maxs))
            return;
    }

    if (!gl_ztrick.value) {
        glEnable (GL_POLYGON_OFFSET_FILL);
        glEnable (GL_POLYGON_OFFSET_LINE);
    }

    if (ISTRANSPARENT (ent)) {
        glEnable (GL_BLEND);
        glTexEnvf (GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);
        glColor4f (1, 1, 1, ent->transparency);
    }

    VectorSubtract (r_refdef.vieworg, ent->origin, modelorg);
    if (rotated) {
        vec3_t        temp, forward, right, up;

        VectorCopy (modelorg, temp);
        AngleVectors (ent->angles, forward, right, up);
        modelorg[0] = DotProduct (temp, forward);
        modelorg[1] = -DotProduct (temp, right);
        modelorg[2] = DotProduct (temp, up);
    }

    surf = &clmodel->surfaces[clmodel->firstmodelsurface];

    // calculate dynamic lighting for bmodel if it's not an instanced model
    //if (clmodel->firstmodelsurface != 0 && !gl_flashblend.value) { // lxndr: more is better
    if (clmodel->firstmodelsurface != 0) {
        for (k = 0; k < MAX_DLIGHTS; k++) {
            if (cl_dlights[k].die < cl.time || !cl_dlights[k].radius)
                continue;

            R_MarkLights (&cl_dlights[k], k, clmodel->nodes + clmodel->hulls[0].firstclipnode);
        }
    }

    if (gl_smoothmodels.value)
        glShadeModel (GL_SMOOTH);

    if (gl_affinemodels.value)
        glHint (GL_PERSPECTIVE_CORRECTION_HINT, GL_FASTEST);

    glPushMatrix ();

    glTranslatef (ent->origin[0], ent->origin[1], ent->origin[2]);
    glRotatef (ent->angles[1], 0, 0, 1);
    glRotatef (ent->angles[0], 0, 1, 0);
    glRotatef (ent->angles[2], 1, 0, 0);

    R_ClearTextureChains (clmodel);

    // draw texture
    for (i = 0; i < clmodel->nummodelsurfaces; i++, surf++) {
        // find which side of the node we are on
        plane = surf->plane;
        dot = PlaneDiff (modelorg, plane);
        R_SortTextureBackToFront (surf, dot < 0);
    }

    if (r_drawworld.value == 2)
        R_DrawTextureChains (clmodel);
    else if (r_drawworld.value)
        R_DrawTextureChainsOld (clmodel);

    if (r_drawtris.value)
        R_DrawTriangleChain (clmodel);

    R_DrawSkyChain ();
    R_DrawAlphaChain ();

    glPopMatrix ();

    glShadeModel (GL_FLAT);
    if (gl_affinemodels.value)
        glHint (GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);

    if (ISTRANSPARENT (ent)) {
        glColor3ubv (color_white);
        glDisable (GL_BLEND);
    }

    if (!gl_ztrick.value) {
        glDisable (GL_POLYGON_OFFSET_FILL);
        glDisable (GL_POLYGON_OFFSET_LINE);
    }
}

/*
 *
 *
 * WORLD MODEL
 *
 *
 */

/*
 * R_RecursiveWorldNode
 */
static void R_RecursiveWorldNode (mnode_t * node, int clipflags) {
    int           c, side, clipped;
    mplane_t     *plane, *clipplane;
    msurface_t   *surf, **mark;
    mleaf_t      *pleaf;
    double        dot;

    if (node->contents == CONTENTS_SOLID)
        return;                        // solid

    if (node->visframe != r_visframecount)
        return;

    if (R_CullBox (node->minmaxs, node->minmaxs + 3))
        return;

    for (c = 0, clipplane = frustum; c < 4; c++, clipplane++) {
        if (!(clipflags & (1 << c)))
            continue;                  // don't need to clip against it

        clipped = BOX_ON_PLANE_SIDE (node->minmaxs, node->minmaxs + 3, clipplane);
        if (clipped == 2)
            return;
        else if (clipped == 1)
            clipflags &= ~(1 << c);    // node is entirely on screen
    }

    // if a leaf node, draw stuff
    if (node->contents < 0) {
        pleaf = (mleaf_t *) node;
        mark = pleaf->firstmarksurface;
        c = pleaf->nummarksurfaces;

        if (c) {
            do {
                (*mark)->visframe = r_framecount;
                mark++;
            }
            while (--c);
        }
        // deal with model fragments in this leaf
        if (pleaf->efrags)
            R_StoreEfrags (&pleaf->efrags);

        return;
    }
    // node is just a decision point, so go down the apropriate sides

    // find which side of the node we are on
    plane = node->plane;
    dot = PlaneDiff (modelorg, plane);
    side = (dot >= 0) ? 0 : 1;

    // recurse down the children, front side first
    R_RecursiveWorldNode (node->children[side], clipflags);

    // draw stuff
    c = node->numsurfaces;
    for (surf = cl.worldmodel->surfaces + node->firstsurface; c; c--, surf++)
        if (surf->visframe == r_framecount)
            R_SortTextureFrontToBack (surf, side);

    // recurse down the back side
    R_RecursiveWorldNode (node->children[!side], clipflags);
}

/*
 * R_DrawWorld
 */
void R_DrawWorld (void) {
    entity_t      ent;

    memset (&ent, 0, sizeof (ent));
    ent.model = cl.worldmodel;

    R_ClearTextureChains (cl.worldmodel);

    VectorCopy (r_refdef.vieworg, modelorg);

    currententity = &ent;
    currenttexture = -1;

    // set up texture chains for the world
    R_RecursiveWorldNode (cl.worldmodel->nodes, 15);

    if (r_skyboxloaded)
        R_DrawSkyBox ();
    else
        R_DrawSkyChain ();

    // draw the world
    if (r_drawworld.value == 2)
        R_DrawTextureChains (cl.worldmodel);
    else if (r_drawworld.value)
        R_DrawTextureChainsOld (cl.worldmodel);

    if (r_drawtris.value)
        R_DrawTriangleChain (cl.worldmodel);

    // draw the world alpha textures
    R_DrawAlphaChain ();
}

/*
 * R_MarkLeaves
 */
void R_MarkLeaves (void) {
    int           i;
    byte         *vis, solid[MAX_MAP_LEAFS / 8];
    mnode_t      *node;

    if (!r_novis.value && r_oldviewleaf == r_viewleaf && r_oldviewleaf2 == r_viewleaf2) // watervis hack
        return;

    r_visframecount++;
    r_oldviewleaf = r_viewleaf;

    if (r_novis.value) {
        vis = solid;
        memset (solid, 0xff, (cl.worldmodel->numleafs + 7) >> 3);
    } else {
        vis = Mod_LeafPVS (r_viewleaf, cl.worldmodel);

        if (r_viewleaf2) {
            int           i, count;
            unsigned     *src, *dest;

            // merge visibility data for two leafs
            count = (cl.worldmodel->numleafs + 7) >> 3;
            memcpy (solid, vis, count);
            src = (unsigned *) Mod_LeafPVS (r_viewleaf2, cl.worldmodel);
            dest = (unsigned *) solid;
            count = (count + 3) >> 2;
            for (i = 0; i < count; i++)
                *dest++ |= *src++;
            vis = solid;
        }
    }

    for (i = 0; i < cl.worldmodel->numleafs; i++) {
        if (vis[i >> 3] & (1 << (i & 7))) {
            node = (mnode_t *) & cl.worldmodel->leafs[i + 1];
            do {
                if (node->visframe == r_visframecount)
                    break;
                node->visframe = r_visframecount;
                node = node->parent;
            }
            while (node);
        }
    }
}

/*
 *
 *
 * LIGHTMAP ALLOCATION
 *
 *
 */

// returns a texture number and the position inside it
static int AllocBlock (int w, int h, int *x, int *y) {
    int           i, j, best, best2, texnum;

    for (texnum = 0; texnum < MAX_LIGHTMAPS; texnum++) {
        best = BLOCK_HEIGHT;

        for (i = 0; i < BLOCK_WIDTH - w; i++) {
            best2 = 0;

            for (j = 0; j < w; j++) {
                if (allocated[texnum][i + j] >= best)
                    break;
                if (allocated[texnum][i + j] > best2)
                    best2 = allocated[texnum][i + j];
            }
            if (j == w) {              // this is a valid spot
                *x = i;
                *y = best = best2;
            }
        }

        if (best + h > BLOCK_HEIGHT)
            continue;

        for (i = 0; i < w; i++)
            allocated[texnum][*x + i] = best + h;

        return texnum;
    }

    Sys_Error ("AllocBlock: full");
    return 0;
}

mvertex_t    *r_pcurrentvertbase;
model_t      *currentmodel;

/*
 * BuildSurfaceDisplayList
 */
static void BuildSurfaceDisplayList (msurface_t * fa) {
    int           i, lindex, lnumverts, vertpage;
    float        *vec, s, t;
    medge_t      *pedges, *r_pedge;
    glpoly_t     *poly;

    // reconstruct the polygon
    pedges = currentmodel->edges;
    lnumverts = fa->numedges;
    vertpage = 0;

    // draw texture
    poly = Hunk_AllocName (sizeof (glpoly_t) + (lnumverts - 4) * VERTEXSIZE * sizeof (float), "polys");
    poly->next = fa->polys;
    fa->polys = poly;
    poly->numverts = lnumverts;

    for (i = 0; i < lnumverts; i++) {
        lindex = currentmodel->surfedges[fa->firstedge + i];

        if (lindex > 0) {
            r_pedge = &pedges[lindex];
            vec = r_pcurrentvertbase[r_pedge->v[0]].position;
        } else {
            r_pedge = &pedges[-lindex];
            vec = r_pcurrentvertbase[r_pedge->v[1]].position;
        }

        s = DotProduct (vec, fa->texinfo->vecs[0]) + fa->texinfo->vecs[0][3];
        s /= fa->texinfo->texture->width;

        t = DotProduct (vec, fa->texinfo->vecs[1]) + fa->texinfo->vecs[1][3];
        t /= fa->texinfo->texture->height;

        VectorCopy (vec, poly->verts[i]);
        poly->verts[i][3] = s;
        poly->verts[i][4] = t;

        // lightmap texture coordinates
        s = DotProduct (vec, fa->texinfo->vecs[0]) + fa->texinfo->vecs[0][3];
        s -= fa->texturemins[0];
        s += fa->light_s * 16;
        s += 8;
        s /= BLOCK_WIDTH * 16;         // fa->texinfo->texture->width;

        t = DotProduct (vec, fa->texinfo->vecs[1]) + fa->texinfo->vecs[1][3];
        t -= fa->texturemins[1];
        t += fa->light_t * 16;
        t += 8;
        t /= BLOCK_HEIGHT * 16;        // fa->texinfo->texture->height;

        poly->verts[i][5] = s;
        poly->verts[i][6] = t;

        s = DotProduct (vec, fa->texinfo->vecs[0]) + fa->texinfo->vecs[0][3];
        s /= 128;

        t = DotProduct (vec, fa->texinfo->vecs[1]) + fa->texinfo->vecs[1][3];
        t /= 128;

        VectorCopy (vec, poly->verts[i]);
        poly->verts[i][7] = s;
        poly->verts[i][8] = t;
    }

    poly->numverts = lnumverts;
}

/*
 * CreateSurfaceLightmap
 */
static void CreateSurfaceLightmap (msurface_t * surf) {
    int           smax, tmax;
    byte         *base;

    smax = (surf->extents[0] >> 4) + 1;
    tmax = (surf->extents[1] >> 4) + 1;

    if (smax > BLOCK_WIDTH)
        Host_Error ("GL_CreateSurfaceLightmap: smax = %d > BLOCK_WIDTH", smax);
    if (tmax > BLOCK_HEIGHT)
        Host_Error ("GL_CreateSurfaceLightmap: tmax = %d > BLOCK_HEIGHT", tmax);
    if (smax * tmax > BLOCK_WIDTH * BLOCK_HEIGHT)
        Host_Error ("GL_CreateSurfaceLightmap: smax * tmax = %d > BLOCK_WIDTH * BLOCK_HEIGHT", smax * tmax);

    surf->lightmaptexturenum = AllocBlock (smax, tmax, &surf->light_s, &surf->light_t);
    base = lightmaps + surf->lightmaptexturenum * BLOCK_WIDTH * BLOCK_HEIGHT * 3;
    base += (surf->light_t * BLOCK_WIDTH + surf->light_s) * 3;
    numdlights = 0;
    R_BuildLightMap (surf, base, BLOCK_WIDTH * 3);
}

/*
 * GL_BuildLightmaps
 *
 * Builds the lightmap texture with all the surfaces from all brush models
 *
 */
void GL_BuildLightmaps (void) {
    int           i, j;
    model_t      *m;

    memset (allocated, 0, sizeof (allocated));

    r_framecount = 1;                  // no dlightcache

    if (!lightmap_textures) {
        lightmap_textures = texture_extension_number;
        texture_extension_number += MAX_LIGHTMAPS;
    }

    for (j = 1; j < MAX_MODELS; j++) {
        if (!(m = cl.model_precache[j]))
            break;
        if (m->name[0] == '*')
            continue;
        r_pcurrentvertbase = m->vertexes;
        currentmodel = m;
        for (i = 0; i < m->numsurfaces; i++) {
            if (m->surfaces[i].flags & (SURF_DRAWTURB | SURF_DRAWSKY))
                continue;
            CreateSurfaceLightmap (m->surfaces + i);
            BuildSurfaceDisplayList (m->surfaces + i);
        }
    }

    if (gl_ext.multitex)
        GL_EnableMultitexture ();

    // upload all lightmaps that were filled
    for (i = 0; i < MAX_LIGHTMAPS; i++) {
        if (!allocated[i][0])
            break;                     // no more used
        lightmap_modified[i] = false;
        lightmap_rectchange[i].l = BLOCK_WIDTH;
        lightmap_rectchange[i].t = BLOCK_HEIGHT;
        lightmap_rectchange[i].w = 0;
        lightmap_rectchange[i].h = 0;
        GL_Bind (lightmap_textures + i);
        glTexParameterf (GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
        glTexParameterf (GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
        glTexImage2D (GL_TEXTURE_2D, 0, gl_lightmap_format, BLOCK_WIDTH, BLOCK_HEIGHT, 0, GL_RGB, GL_UNSIGNED_BYTE,
                      lightmaps + i * BLOCK_WIDTH * BLOCK_HEIGHT * 3);
    }

    if (gl_ext.multitex)
        GL_DisableMultitexture ();
}
