/*
 * Copyright (C) 1996-1997 Id Software, Inc.
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation; either version 2 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.
 *
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 */
// gl_screen.c -- master for refresh, status bar, console, chat, notify, etc

#include "quakedef.h"
#ifdef _WIN32
#  include "movie.h"
#endif

/*
 *
 * background clear rendering turtle/net/ram icons sbar centerprint / slow
 * centerprint notify lines intermission / finale overlay loading plaque
 * console menu
 *
 * required background clears required update regions
 *
 *
 * syncronous draw mode or async One off screen buffer, with updates either
 * copied or xblited Need to double buffer?
 *
 *
 * async draw will require the refresh area to be cleared, because it will be
 * xblited, but sync draw can just ignore it.
 *
 * sync draw
 *
 * CenterPrint () SlowPrint () Screen_Update (); Con_Printf ();
 *
 * net turn off messages option
 *
 * the refresh is always rendered, unless the console is full screen
 *
 *
 * console is: notify lines half full
 *
 */

int           glx, gly, glwidth, glheight;

int           hudwidth, hudheight;

// only the refresh window will be updated unless these variables are flagged
int           scr_copytop;
int           scr_copyeverything;

float         scr_con_current;
float         scr_conlines;            // lines of console to display

float         oldscreensize, oldfov, oldsbar;
qboolean      oldzoomstate;

cvar_t        scr_autoid = { "scr_autoid", "0" };
cvar_t        scr_centerlog = { "scr_centerlog", "1" };
cvar_t        scr_centertime = { "scr_centertime", "4" };
cvar_t        scr_consize = { "scr_consize", "0.2" };
cvar_t        scr_conspeed = { "scr_conspeed", "1000" };
cvar_t        scr_conswitch = { "scr_conswitch", "1" };
cvar_t        scr_contextsize = { "scr_contextsize", "1" };
qboolean      OnChange_scr_fov (cvar_t * var, char *string);
cvar_t        scr_fov = { "fov", "90", CVAR_NONE, OnChange_scr_fov };   // 10 - 170
qboolean      OnChange_scr_hudsize (cvar_t * var, char *string);
cvar_t        scr_hudsize = { "scr_hudsize", "0.7", CVAR_NONE, OnChange_scr_hudsize };
cvar_t        scr_printspeed = { "scr_printspeed", "8" };
cvar_t        scr_sbarsize = { "scr_sbarsize", "1" };
cvar_t        scr_shotauto = { "scr_shotauto", "1" };
qboolean      OnChange_scr_shotformat (cvar_t * var, char *string);
cvar_t        scr_shotformat = { "scr_shotformat", "png", CVAR_NONE, OnChange_scr_shotformat };
qboolean      OnChange_scr_viewsize (cvar_t * var, char *string);
cvar_t        scr_viewsize = { "viewsize", "110", CVAR_NONE, OnChange_scr_viewsize };
cvar_t        gl_triplebuffer = { "gl_triplebuffer", "1" };

extern cvar_t crosshair;

qboolean      scr_initialized;         // ready to draw

int           scr_fullupdate;

int           clearconsole;
int           clearnotify;

int           sb_lines;

viddef_t      vid;                     // global video state

vrect_t       scr_vrect;

char         *scr_notifystring;

qboolean      scr_drawdialog;
qboolean      scr_drawloading;
qboolean      scr_drawnothing;
qboolean      scr_drawtransition;

qboolean      scr_disabled_for_loading;

void          SCR_ScreenShot_f (void);

qboolean OnChange_scr_fov (cvar_t * var, char *string) {
    float         value = Q_atof (string);

    if (value < 25 || value > 155) {
        if (key_dest == key_console)
            Con_Printf ("Valid values for %s are between 25 and 155\n", var->name);
        return true;
    }

    vid.recalc_refdef = true;
    return false;
}

qboolean OnChange_scr_hudsize (cvar_t * var, char *string) {
    float         value = Q_atof (string);

    if (value < 0 || value > 1.5001) { // lxndr: float hazard
        if (key_dest == key_console)
            Con_Printf ("Valid values for %s are between 0 and 1.5\n", var->name);
        return true;
    }

    vid.recalc_refdef = true;
    return false;
}

qboolean OnChange_scr_shotformat (cvar_t * var, char *string) {
    if (!Q_strcasecmp (string, "jpg") || !Q_strcasecmp (string, "jpeg")) {
        strcpy (string, "jpg");
    } else if (Q_strcasecmp (string, "png") && Q_strcasecmp (string, "tga")) {
        Con_Printf ("Unsupported image format\n");
        return true;
    }

    return false;
}

qboolean OnChange_scr_viewsize (cvar_t * var, char *string) {
    float         value = Q_atof (string);

    if (cl.intermission)
        return true;

    if (value < 20 || value > 120) {
        if (key_dest == key_console)
            Con_Printf ("Valid values for %s are between 20 and 120\n", var->name);
        return true;
    }

    vid.recalc_refdef = true;
    return false;
}

/*
 *
 *
 * CENTER PRINTING
 *
 *
 */

char          scr_centerstring[1024];
float         scr_centertime_start;    // for slow victory printing
float         scr_centertime_off;
int           scr_center_lines;
int           scr_erase_lines;
int           scr_erase_center;

/*
 * SCR_CenterPrint
 *
 * Called for important messages that should stay in the center of the screen
 * for a few moments
 */
void SCR_CenterPrint (char *str) {
    int           l;
    char          out[48];
    char         *p = str;

    if (!str[0])
        return;

    Q_strncpyz (scr_centerstring, str, sizeof (scr_centerstring));
    scr_centertime_off = scr_centertime.value;
    scr_centertime_start = cl.time;

    while (*p) {
        for (l = 0; l < 40; l++) {
            if (p[l] == '\n' || !p[l])
                break;
            out[l] = p[l];
        }
        p += l;
        if (*p == '\n')
            p++;
        while (l < 40)
            out[l++] = ' ';
        out[l] = '\0';
        if (scr_centerlog.value)
            Con_Printf ("\x1d\x1e %s \x1e\x1f\n", out);
    }
    if (scr_centerlog.value)
        Con_Printf ("\n");

    // count the number of lines for centering
    scr_center_lines = 1;
    while (*str) {
        if (*str == '\n')
            scr_center_lines++;
        str++;
    }
}

void SCR_DrawCenterString (void) {
    char         *start;
    int           l, j, x, y, remaining;

    scr_copytop = 1;
    if (scr_center_lines > scr_erase_lines)
        scr_erase_lines = scr_center_lines;

    scr_centertime_off -= host_frametime;

    if (scr_centertime_off <= 0 && !cl.intermission)
        return;

    if (key_dest != key_game)
        return;

    // the finale prints the characters one at a time
    if (cl.intermission)
        remaining = scr_printspeed.value * (cl.time - scr_centertime_start);
    else
        remaining = 9999;

    scr_erase_center = 0;
    start = scr_centerstring;

    if (scr_center_lines <= 4)
        y = hudheight * 0.35;
    else
        y = 48;

    do {
        // scan the width of the line

        for (l = 0; l < 40; l++)
            if (start[l] == '\n' || !start[l])
                break;

        x = (hudwidth - l * 8) / 2;
        for (j = 0; j < l; j++, x += 8) {
            Draw_Character (x, y, start[j]);
            if (!remaining--)
                return;
        }

        y += 8;

        while (*start && *start != '\n')
            start++;

        if (!*start)
            break;
        start++;                       // skip the \n
    }
    while (1);
}

// 
/*
 * SCR_CalcRefdef
 *
 * Must be called whenever vid changes Internal use only
 */
static void SCR_CalcRefdef (void) {
    int           h;
    float         size;
    qboolean      full = false;

    scr_fullupdate = 0;                // force a background redraw
    vid.recalc_refdef = false;

    // force the status bar to redraw
    //Sbar_Changed ();

    if (cl.intermission || scr_drawloading)     // lxndr: drawloading requires an external recalc request (cl_parse.c)
        full = true;
    if (scr_viewsize.value >= 100.0 && !(scr_hudsize.value == 1.5 && scr_sbarinvpos.value == 1))
        full = true;

    size = scr_viewsize.value;
    if (size >= 120)
        sb_lines = 0;                  // no status bar at all
    else if (size >= 110)
        sb_lines = 24;                 // no inventory
    else
        sb_lines = 24 + 16 + 8;

    if (size < 120)
        Sbar_Changed ();
    if (size >= 100 || full)
        size = 100;
    size /= 100.0;

    h = full ? vid.height : vid.height - sb_lines * vid.height / hudheight;

    r_refdef.vrect.width = vid.width * size;
    if (r_refdef.vrect.width < 96) {
        size = 96.0 / r_refdef.vrect.width;
        r_refdef.vrect.width = 96;     // min for icons
    }

    r_refdef.vrect.height = vid.height * size;

    if (!full) {
        if (r_refdef.vrect.height > vid.height - sb_lines * vid.height / hudheight)
            r_refdef.vrect.height = vid.height - sb_lines * vid.height / hudheight;
    } else if (r_refdef.vrect.height > vid.height) {
        r_refdef.vrect.height = vid.height;
    }

    r_refdef.vrect.x = (vid.width - r_refdef.vrect.width) / 2;

    if (full)
        r_refdef.vrect.y = 0;
    else
        r_refdef.vrect.y = (h - r_refdef.vrect.height) / 2;

    V_CalcZoom ();                     // lxndr: new method

    scr_vrect = r_refdef.vrect;
}

/*
 * SCR_SizeUp_f
 *
 * Keybinding command
 */
void SCR_SizeUp_f (void) {
    Cvar_SetValue (&scr_viewsize, scr_viewsize.value + 10);
    vid.recalc_refdef = true;
}

/*
 * SCR_SizeDown_f
 *
 * Keybinding command
 */
void SCR_SizeDown_f (void) {
    Cvar_SetValue (&scr_viewsize, scr_viewsize.value - 10);
    vid.recalc_refdef = true;
}

// 

/*
 * SCR_Init
 */
void SCR_Init (void) {
    Cvar_Register (&scr_autoid);
    Cvar_Register (&scr_centerlog);
    Cvar_Register (&scr_centertime);
    Cvar_Register (&scr_consize);
    Cvar_Register (&scr_conspeed);
    Cvar_Register (&scr_conswitch);
    Cvar_Register (&scr_contextsize);
    Cvar_Register (&scr_fov);
    Cvar_Register (&scr_hudsize);
    Cvar_Register (&scr_printspeed);
    Cvar_Register (&scr_sbarsize);
    Cvar_Register (&scr_shotauto);
    Cvar_Register (&scr_shotformat);
    Cvar_Register (&scr_viewsize);
    Cvar_Register (&gl_triplebuffer);

    // register our commands
    Cmd_AddCommand ("screenshot", SCR_ScreenShot_f);
    Cmd_AddCommand ("sizeup", SCR_SizeUp_f);
    Cmd_AddCommand ("sizedown", SCR_SizeDown_f);

#ifdef _WIN32
    Movie_Init ();
#endif

    scr_initialized = true;
}

// 

/*
 * SCR_SetupConsole
 */
void SCR_SetupConsole (void) {
    // decide on the height of the console
    con_forcedup = (!cl.worldmodel || cls.signon != SIGNONS);

//    if (con_forcedup && cls.state == ca_disconnected && key_dest == key_game)
//        key_dest = key_console; // lxndr: something went wrong

    if (scr_hudsize.value) {
        conwidth = vid.conwidth / (1.0 + scr_hudsize.value / 2.0) / (1.0 + scr_contextsize.value);
        conheight = conwidth * vid.conheight / vid.conwidth;
        if (conheight > vid.conheight)
            conheight = vid.conheight;
    } else {
        conwidth = vid.conwidth > vid.width ? vid.width : vid.conwidth;
        conheight = vid.conheight > vid.height ? vid.height : vid.conheight;
    }

    if (host_initialized)              // lxndr: quick hack to prevent console clearing
        Con_CheckResize ();

    if (con_forcedup) {
        scr_conlines = conheight;      // full screen
        scr_con_current = scr_conlines;
    } else if (scr_con_current > conheight) {
        scr_con_current = 0;
    } else if (key_dest == key_console && !con_message) {
        if (scr_conswitch.value && conwidth > 480)
            scr_conlines = conheight;
        else
            scr_conlines = conheight * scr_consize.value;
        if (scr_conlines < 20)
            scr_conlines = 20;
        if (scr_conlines > conheight)
            scr_conlines = conheight;
    } else {
        scr_conlines = 0;              // none visible
    }

    if (scr_conlines < scr_con_current) {
        scr_con_current -= scr_conspeed.value * (host_frametime + 0.01) * conheight / 320;      // lxndr: host_frametime can be equal to zero when playing demo
        if (scr_conlines > scr_con_current)
            scr_con_current = scr_conlines;

    } else if (scr_conlines > scr_con_current) {
        scr_con_current += scr_conspeed.value * (host_frametime + 0.01) * conheight / 320;      // lxndr: host_frametime can be equal to zero when playing demo
        if (scr_conlines < scr_con_current)
            scr_con_current = scr_conlines;
    }

    if (clearconsole++ < vid.numpages) {
        Sbar_Changed ();
    } else if (clearnotify++ < vid.numpages) {
    } else {
        con_clearnotifylines = 0;
    }
}

/*
 * SCR_DrawConsole
 */
void SCR_DrawConsole (void) {
//    if (scr_drawloading)
//        return;                        // never a console with loading plaque

    if (scr_con_current) {
        scr_copyeverything = 1;
        Con_DrawConsole (scr_con_current, true);
        clearconsole = 0;
        if (scr_hudsize.value) {
            glMatrixMode (GL_PROJECTION);
            glLoadIdentity ();
            glOrtho (0, hudwidth, hudheight, 0, NEARVAL, FARVAL);
        }
    }
}

/*
 * SCR_DrawMenu
 */
void SCR_DrawMenu (void) {
//    if (scr_drawloading)
//        return;                        // never a menu with loading plaque

    M_Draw ();
}

void SCR_DrawSbar (void) {
    float oldwidth, oldheight, f;

    oldwidth = hudwidth;
    oldheight = hudheight;
    if (scr_hudsize.value) {
        f = scr_sbarsize.value / 2 + 0.5;
        hudwidth /= f;
        hudheight /= f;
        glMatrixMode (GL_PROJECTION);
        glLoadIdentity ();
        glOrtho (0, hudwidth, hudheight, 0, NEARVAL, FARVAL);
    }
    Sbar_Draw ();
    if (scr_hudsize.value) {
        hudwidth = oldwidth;
        hudheight = oldheight;
        glMatrixMode (GL_PROJECTION);
        glLoadIdentity ();
        glOrtho (0, hudwidth, hudheight, 0, NEARVAL, FARVAL);
    }
}

/*
 * SCR_DrawNotify
 */
void SCR_DrawNotify (void) {
    if (!scr_con_current && (key_dest != key_menu))
        Con_DrawNotify ();
}

// 

int qglProject (float objx, float objy, float objz, float *model, float *proj, int *view, float *winx, float *winy, float *winz) {
    int           i;
    float         in[4], out[4];

    in[0] = objx, in[1] = objy, in[2] = objz, in[3] = 1.0;

    for (i = 0; i < 4; i++)
        out[i] = in[0] * model[0 * 4 + i] + in[1] * model[1 * 4 + i] + in[2] * model[2 * 4 + i] + in[3] * model[3 * 4 + i];

    for (i = 0; i < 4; i++)
        in[i] = out[0] * proj[0 * 4 + i] + out[1] * proj[1 * 4 + i] + out[2] * proj[2 * 4 + i] + out[3] * proj[3 * 4 + i];

    if (!in[3])
        return 0;

    VectorScale (in, 1 / in[3], in);

    *winx = view[0] + (1 + in[0]) * view[2] / 2;
    *winy = view[1] + (1 + in[1]) * view[3] / 2;
    *winz = (1 + in[2]) / 2;

    return 1;
}

typedef struct player_autoid_s {
    float         x, y;
    scoreboard_t *player;
} autoid_player_t;

static autoid_player_t autoids[MAX_SCOREBOARDNAME];
static int    autoid_count;

#define ISDEAD(i) ((i) >= 41 && (i) <= 102)

void SCR_SetupAutoID (void) {
    int           i, view[4];
    float         model[16], project[16], winz, *origin;
    entity_t     *state;
    autoid_player_t *id;

    autoid_count = 0;

    if (!scr_autoid.value || cls.state != ca_connected || !cls.demoplayback)
        return;

    glGetFloatv (GL_MODELVIEW_MATRIX, model);
    glGetFloatv (GL_PROJECTION_MATRIX, project);
    glGetIntegerv (GL_VIEWPORT, view);

    for (i = 0; i < cl.maxclients; i++) {
        state = &cl_entities[1 + i];

        if ((state->modelindex == cl_modelindex[mi_player] && ISDEAD (state->frame))
            || state->modelindex == cl_modelindex[mi_h_player])
            continue;

        if (R_CullSphere (state->origin, 0))
            continue;

        id = &autoids[autoid_count];
        id->player = &cl.scores[i];
        origin = state->origin;
        if (qglProject (origin[0], origin[1], origin[2] + 28, model, project, view, &id->x, &id->y, &winz))
            autoid_count++;
    }
}

void SCR_DrawAutoID (void) {
    int           i, x, y;

    if (!scr_autoid.value || !cls.demoplayback)
        return;

    for (i = 0; i < autoid_count; i++) {
        x = autoids[i].x * vid.width / glwidth;
        y = (glheight - autoids[i].y) * vid.height / glheight;
        Draw_String (x - strlen (autoids[i].player->name) * 4, y - 8, autoids[i].player->name);
    }
}

/*
 *
 * SCREEN SHOTS
 *
 *
 */

extern unsigned short ramps[3][256];

void ApplyGamma (byte * buffer, int size) {
    int           i;

    if (!vid_hwgamma_enabled)
        return;

    for (i = 0; i < size; i += 3) {
        buffer[i + 0] = ramps[0][buffer[i + 0]] >> 8;
        buffer[i + 1] = ramps[1][buffer[i + 1]] >> 8;
        buffer[i + 2] = ramps[2][buffer[i + 2]] >> 8;
    }
}

qboolean SCR_ScreenShot (char *name) {
    qboolean      ok = false;
    int           buffersize;
    byte         *buffer;
    char         *ext;

    ext = Com_FileExtension (name);
    buffersize = glwidth * glheight * 3;
    buffer = Q_malloc (buffersize);

    glReadPixels (glx, gly, glwidth, glheight, GL_RGB, GL_UNSIGNED_BYTE, buffer);

    ApplyGamma (buffer, buffersize);

    if (!Q_strcasecmp (ext, "jpg"))
        ok = Image_WriteJPEG (name, jpeg_compression_level.value, buffer + buffersize - 3 * glwidth, -glwidth, glheight);
    else if (!Q_strcasecmp (ext, "png"))
        ok = Image_WritePNG (name, png_compression_level.value, buffer + buffersize - 3 * glwidth, -glwidth, glheight);
    else
        ok = Image_WriteTGA (name, buffer, glwidth, glheight);

    Q_free (buffer);

    return ok;
}

qboolean SCR_FindScreenShot (char *name) {
    if (Com_FindFile (va ("%s.jpg", name)) || Com_FindFile (va ("%s.png", name)) || Com_FindFile (va ("%s.tga", name)))
        return true;
    return false;
}

void SCR_ScreenShotAuto (void) {
    char          name[MAX_OSPATH];
    qboolean      success;

    if (!scr_drawnothing)
        return;

    Q_snprintfz (name, sizeof (name), "%s-%s-00", ENGINE_FSNAME, cl_mapname.string);
    if (!SCR_FindScreenShot (name)) {
        success = SCR_ScreenShot (va ("%s/%s.%s", com_screenshotdir, name, scr_shotformat.string));
        Con_Printf ("%s %s.%s\n", success ? "Wrote" : "Couldn't write", name, scr_shotformat.string);
    }

    scr_drawnothing = false;
}

/*
 * SCR_ScreenShot_f
 */
void SCR_ScreenShot_f (void) {
    int           i;
    qboolean      success;
    char          name[MAX_OSPATH];

    if (Cmd_Argc () == 2) {
        Q_strncpyz (name, Cmd_Argv (1), sizeof (name));
    } else if (Cmd_Argc () == 1) {
        // find a file name to save it to
        for (i = 0; i < 99; i++) {
            if (cl.worldmodel)
                Q_snprintfz (name, sizeof (name), "%s-%s-%02i.%s", ENGINE_FSNAME, cl_mapname.string, i, scr_shotformat.string);
            else
                Q_snprintfz (name, sizeof (name), "%s-%02i.%s", ENGINE_FSNAME, i, scr_shotformat.string);
            if (Sys_FileTime (va ("%s/%s", com_screenshotdir, name)) == -1)
                break;                 // file doesn't exist
        }

        if (i == 100) {
            Con_Printf ("ERROR: Cannot create more screenshots\n");
            return;
        }
    } else {
        Con_Printf ("Usage: %s [<filename>]", Cmd_Argv (0));
        return;
    }

    success = SCR_ScreenShot (va ("%s/%s", com_screenshotdir, name));
    Con_Printf ("%s %s\n", success ? "Wrote" : "Couldn't write", name);
}

// 

void SCR_DrawNotifyString (void) {
    char         *start;
    int           l, j, x, y;

    scr_copyeverything = true;

//    Draw_FadeScreen ();

    start = scr_notifystring;

    y = vid.height * 0.35;
    do {
        // scan the width of the line
        for (l = 0; l < 40; l++)
            if (start[l] == '\n' || !start[l])
                break;
        x = (vid.width - l * 8) / 2;
        for (j = 0; j < l; j++, x += 8)
            Draw_Character (x, y, start[j]);

        y += 8;

        while (*start && *start != '\n')
            start++;

        if (!*start)
            break;
        start++;                       // skip the \n
    }
    while (1);
}

// 

void SCR_TileClear (void) {
    if (con_forcedup || scr_drawloading || cl.intermission)
        return;

    if (r_refdef.vrect.x > 0) {
        // left
        Draw_TileClear (0, 0, r_refdef.vrect.x, vid.height - sb_lines * vid.height / hudheight);
        // right
        Draw_TileClear (r_refdef.vrect.x + r_refdef.vrect.width, 0, vid.width - r_refdef.vrect.x + r_refdef.vrect.width,
                        vid.height - sb_lines * vid.height / hudheight);
    }
    if (r_refdef.vrect.y > 0) {
        // top
        Draw_TileClear (r_refdef.vrect.x, 0, r_refdef.vrect.x + r_refdef.vrect.width, r_refdef.vrect.y);
        // bottom
        Draw_TileClear (r_refdef.vrect.x, r_refdef.vrect.y + r_refdef.vrect.height, r_refdef.vrect.width,
                        vid.height - sb_lines * vid.height / hudheight - (r_refdef.vrect.height + r_refdef.vrect.y));
    }
    // lxndr: sbar area
    if ((scr_hudsize.value == 1.5 && scr_sbarinvpos.value == 1) || scr_viewsize.value < 100.0)
        Draw_TileClear (0, vid.height - sb_lines * vid.height / hudheight, vid.width, sb_lines * vid.height / hudheight);
}

void SCR_Init2D (void) {
    if (con_forcedup)
        GL_Set2D (glx, gly, glwidth, glheight, NEARVAL, FARVAL);

//Con_Printf ("%d\n", con_forcedup);
    glColor3ubv (color_white);
    glAlphaFunc (GL_GREATER, 0.000);
    glTexEnvf (GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);

    glEnable (GL_ALPHA_TEST);          // lxndr: was enable
    glDisable (GL_BLEND);
    glDisable (GL_DEPTH_TEST);
    glEnable (GL_TEXTURE_2D);

}

void SCR_UpdatePalette (void) {
    int           i, c;
    qboolean      new;
    float         gamma, contrast;
    static float  old_gamma, old_contrast;
    extern float  vid_gamma;

    new = false;

    gamma = bound (0.5, v_gamma.value, 4.5);
    if (gamma != old_gamma) {
        old_gamma = gamma;
        new = true;
    }

    contrast = bound (1, v_contrast.value, 5);
    if (contrast != old_contrast) {
        old_contrast = contrast;
        new = true;
    }

    if (!new)
        return;

    if (vid_gamma != 1.0) {
        contrast = pow (contrast, vid_gamma);
        gamma /= vid_gamma;
    }

    for (i = 0; i < 256; i++) {
        // apply contrast
        c = i * contrast;
        if (c > 255)
            c = 255;
        // apply gamma
        c = 255 * pow ((c + 0.5) / 255.5, gamma) + 0.5;
        c = bound (0, c, 255);
        ramps[0][i] = ramps[1][i] = ramps[2][i] = c << 8;
    }

    VID_SetDeviceGammaRamp ((unsigned short *) ramps);
}

float sss;
/*
 * SCR_UpdateScreen
 *
 * This is called every frame, and can also be called explicitly to flush text
 * to the screen.
 *
 * WARNING: be very careful calling this from elsewhere, because the refresh
 * needs almost the entire 256k of stack space!
 */
void SCR_UpdateScreen (void) {
    extern float  zoom;

    if (cls.state == ca_dedicated)
        return;                        // stdout only
    if (!scr_initialized || !con_initialized)
        return;                        // not initialized yet
    if (vid.nodisplay)
        return;
    if (scr_disabled_for_loading)
        return;

    if (scr_hudsize.value) {
        hudwidth = 320 / (scr_hudsize.value / 2 + 0.25);
        hudheight = hudwidth * vid.height / vid.width;
    } else {
        hudwidth = vid.width;
        hudheight = vid.height;
    }

    vid.numpages = 2 + gl_triplebuffer.value;

    scr_copytop = 0;
    scr_copyeverything = 0;

    // lxndr: cvars should use an OnChange structure
    if (oldfov != scr_fov.value) {
        oldfov = scr_fov.value;
        vid.recalc_refdef = true;
    }
    if (oldscreensize != scr_viewsize.value) {
        oldscreensize = scr_viewsize.value;
        vid.recalc_refdef = true;
    }
    if ((in_zoom.state & 1) != oldzoomstate || zoom > 0 || (zoom > 0 && cl.stats[STAT_HEALTH] <= 0)) {
        oldzoomstate = (in_zoom.state & 1);
        vid.recalc_refdef = true;
    }

    if (vid.recalc_refdef || scr_drawloading || cl.time < 2.5)
        SCR_CalcRefdef ();

    if ((v_contrast.value > 1 && !vid_hwgamma_enabled) || gl_clear.value)
        Sbar_Changed ();

    SCR_SetupConsole ();

    Ambience_Update ();

    // do 3D refresh drawing, and then update the screen
    GL_BeginRendering (&glx, &gly, &glwidth, &glheight);

    V_RenderView ();

    SCR_SetupAutoID ();
    SCR_Init2D ();

    // draw any areas not covered by the refresh
    SCR_TileClear ();
    if (scr_hudsize.value) {
        glMatrixMode (GL_PROJECTION);
        glLoadIdentity ();
        glOrtho (0, hudwidth, hudheight, 0, NEARVAL, FARVAL);
    }
    if (scr_drawloading) {
        SCR_DrawLoading ();
    } else if (scr_drawtransition) {
        if (cls.state == ca_disconnected)
            SCR_DrawConsole ();
        Draw_FadeScreen ();
    } else if (scr_drawnothing) {
    } else if (scr_drawdialog) {
        SCR_DrawSbar ();
        SCR_DrawNotifyString ();
    } else if (cl.intermission == 1 && key_dest == key_game) {
        SCR_DrawIntermissionOverlay ();
        SCR_DrawNotify ();
        SCR_DrawVolume ();
    } else if (cl.intermission == 2 && key_dest == key_game) {
        SCR_DrawFinaleOverlay ();
        SCR_DrawCenterString ();
        SCR_DrawNotify ();
        SCR_DrawVolume ();
    } else {
        Draw_Crosshair ();
        if (nehahra)
            SHOWLMP_drawall ();
        SCR_DrawAutoID ();
        SCR_DrawCenterString ();
        SCR_DrawClock ();
#ifdef DEBUG
        SCR_DrawDebug ();
#endif
        SCR_DrawFPS ();
        SCR_DrawLocation ();
        SCR_DrawNotify ();
        SCR_DrawPlaybackStats ();
        SCR_DrawSpeed ();
        SCR_DrawStats ();
        SCR_DrawDeathmatchOverlay ();
        SCR_DrawSbar ();
        SCR_DrawPause ();
        SCR_DrawConsole ();
        SCR_DrawVolume ();
        if (!game.free) {
            SCR_DrawNet ();
            SCR_DrawRam ();
            SCR_DrawTurtle ();
        }
        SCR_DrawMenu ();
    }
    if (scr_hudsize.value) {
        glMatrixMode (GL_PROJECTION);
        glLoadIdentity ();
        glOrtho (0, vid.width, vid.height, 0, NEARVAL, FARVAL);
    }

    SCR_UpdatePalette ();

#ifdef _WIN32
    Movie_UpdateScreen ();
#endif

    GL_Set3D ();
    GL_EndRendering ();
}
