/*
 * Copyright (C) 1996-1997 Id Software, Inc.
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * 
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 * 
 */
// glquake.h

// disable data conversion warnings

#ifdef _WIN32
#  pragma warning (disable : 4244)     // MIPS
#  pragma warning (disable : 4136)     // X86
#  pragma warning (disable : 4051)     // ALPHA

#  include <windows.h>
#endif

#include <GL/gl.h>

#ifndef APIENTRY
#define APIENTRY
#endif

void          GL_BeginRendering (int *x, int *y, int *width, int *height);
void          GL_EndRendering (void);

extern int    texture_extension_number;
extern float  gldepthmin, gldepthmax;
extern byte   color_white[4], color_black[4];

#define TEX_EXTERNAL		(1<<0)
#define TEX_MIPMAP		(1<<1)
#define TEX_ALPHA		(1<<2)
#define TEX_LUMA		(1<<3)
#define TEX_FULLBRIGHT		(1<<4)
#define TEX_LINEAR		(1<<5)
#define TEX_NEAREST		(1<<6)
#define TEX_BILINEAR		(1<<7)
#define TEX_TRILINEAR		(1<<8)
#define TEX_NOLEVELS		(1<<9)

#define TEX_LAVA		(1<<10)
#define TEX_SLIME		(1<<11)
#define TEX_TELEPORT		(1<<12)
#define TEX_WATER		(1<<13)

#define TEX_CONSOLE		(1<<15)
#define TEX_CROSSHAIR		(1<<16)
#define TEX_MENU		(1<<17)
#define TEX_SBAR		(1<<18)
#define TEX_TEXT		(1<<19)

#define TEX_ALIAS		(1<<20)
#define TEX_BRUSH		(1<<21)
#define TEX_DECAL		(1<<22)
#define TEX_ITEM		(1<<23)
#define TEX_KEY			(1<<24)
#define TEX_SKY			(1<<25)
#define TEX_TRIGGER		(1<<26)
#define TEX_WEAPON		(1<<27)


#define	MAX_GLTEXTURES	4096	// lxndr: was 1024 (2048 reached)

#define NEARVAL		-10
#define FARVAL		100

#define MAX_PBO		5
#define MAX_VBO		5
extern unsigned int pboIds[MAX_PBO];
extern unsigned int vboIds[MAX_VBO];

#define MAX_VERTEXARRAY	MAX_VBO
#define MAX_VERTEXTUPLE 4 // lxndr: RGBA
#define MAX_VERTEXSIZE	MAX_MAP_VERTS * MAX_VERTEXTUPLE // lxndr: watch models limit
extern float *vertexarrayf[MAX_VERTEXARRAY];
extern int   *vertexarrayi[MAX_VERTEXARRAY];

#define SAT_WEIGHT_RED 0.212671
#define SAT_WEIGHT_GREEN 0.715160
#define SAT_WEIGHT_BLUE 0.072169

typedef struct {
    float         x, y, z;
    float         s, t;
    float         r, g, b;
} glvert_t;

extern glvert_t glv;

extern int    glx, gly, glwidth, glheight;

// normalizing factor so player model works out to about 1 pixel per triangle
#define ALIAS_BASE_SIZE_RATIO	(1.0 / 11.0)

#define	MAX_LBM_HEIGHT		480

#define MAX_ANISOTROPY_EXP 5      // lxndr: 2 ^ MAX_ANISOTROPY_EXP

#define NUMVERTEXNORMALS	162

#define SKYSHIFT		7
#define	SKYSIZE			(1 << SKYSHIFT)
#define SKYMASK			(SKYSIZE - 1)

#define BACKFACE_EPSILON	0.01

void          R_TimeRefresh_f (void);
void          R_ReadPointFile_f (void);

#define ISTRANSPARENT(ent)	((ent)->istransparent && (ent)->transparency > 0 && (ent)->transparency < 1)

void          QMB_InitParticles (void);
void          QMB_ClearParticles (void);
void          QMB_DrawParticles (void);

void          QMB_RunParticleEffect (vec3_t org, vec3_t dir, int color, int count);
void          QMB_RocketTrail (vec3_t start, vec3_t end, vec3_t * trail_origin, trail_type_t type);
void          QMB_BlobExplosion (vec3_t org);
void          QMB_ParticleExplosion (vec3_t org);
void          QMB_LavaSplash (vec3_t org);
void          QMB_TeleportSplash (vec3_t org);
void          QMB_StaticBubble (entity_t * ent);
void          QMB_ColorMappedExplosion (vec3_t org, int colorStart, int colorLength);
void          QMB_TorchFlame (vec3_t org, float size, float time);
void          QMB_Q3TorchFlame (vec3_t org, float size);
void          QMB_MissileFire (vec3_t org);
void          QMB_ShamblerCharge (vec3_t org);
void          QMB_LightningBeam (vec3_t start, vec3_t end);
void          QMB_GenSparks (vec3_t org, byte col[3], float count, float size, float life);
void          QMB_MuzzleFlash (vec3_t org);

extern qboolean qmb_initialized;

void          R_GetParticleMode (void);

extern entity_t r_worldentity;
extern qboolean r_cache_thrash;        // compatability
extern vec3_t modelorg, r_entorigin;
extern entity_t *currententity;
extern int    r_visframecount;         // ??? what difs?
extern int    r_framecount;
extern mplane_t frustum[4];
extern int    c_oldbrush_polys, c_brush_polys, c_md1_polys, c_md3_polys;

// view origin
extern vec3_t vup;
extern vec3_t vpn;
extern vec3_t vright;
extern vec3_t r_origin;

// screen size info
extern refdef_t r_refdef;
extern mleaf_t *r_viewleaf, *r_oldviewleaf;
extern mleaf_t *r_viewleaf2, *r_oldviewleaf2;   // for watervis hack
extern texture_t *r_notexture_mip;
extern int    d_lightstylevalue[256];  // 8.8 fraction of base light value

extern int    currenttexture;
extern int    particletexture;
extern int    playertextures;
extern int    skyboxtextures;
extern int    underwatertexture, detailtexture;
extern int    damagetexture;

extern int    gl_max_size_default;

extern cvar_t r_farclip;
extern cvar_t r_fullbright;
extern cvar_t r_fullbrightskins;
extern cvar_t r_novis;
extern cvar_t r_skybox;
extern cvar_t r_skycolor;
extern cvar_t r_speeds;
extern cvar_t r_viewmodelsize;
extern cvar_t r_waterwarp;

extern cvar_t r_alpha;
extern cvar_t r_alphalava;
extern cvar_t r_alphaslime;
extern cvar_t r_alphateleport;
extern cvar_t r_alphawater;

extern cvar_t r_colorlava;
extern cvar_t r_colorslime;
extern cvar_t r_colorteleport;
extern cvar_t r_colorwater;

extern cvar_t r_drawflames;
extern cvar_t r_drawsky;
extern cvar_t r_drawtris;
extern cvar_t r_drawworld;

extern cvar_t r_dynamic;
extern cvar_t r_dynamicintensity;
extern cvar_t r_dynamicradius;

extern cvar_t r_levels;
extern cvar_t r_levelsmodels;
extern cvar_t r_levelsworld;
extern cvar_t r_levelsweapon;
extern cvar_t r_levelsblood;
extern cvar_t r_levelsgrey;
extern cvar_t r_levelsmax;
extern cvar_t r_levelsthresholdhigh;
extern cvar_t r_levelsthresholdlow;

extern cvar_t r_quality;

extern cvar_t r_shadows;
extern cvar_t r_shadowsintensity;
extern cvar_t r_shadowsmax;
extern cvar_t r_shadowsmin;
extern cvar_t r_shadowsmodels;
extern cvar_t r_shadowssize;
extern cvar_t r_shadowsworld;

extern cvar_t gl_affinemodels;
extern cvar_t gl_bounceparticles;
extern cvar_t gl_caustics;
extern cvar_t gl_clear;
extern cvar_t gl_cull;
extern cvar_t gl_detail;
extern cvar_t gl_doubleeyes;
extern cvar_t gl_interdist;
extern cvar_t gl_lerptextures;
extern cvar_t gl_max_size;
extern cvar_t gl_nocolors;
extern cvar_t gl_playermip;
extern cvar_t gl_picmip;
extern cvar_t gl_polyblend;
extern cvar_t gl_smoothmodels;
extern cvar_t gl_solidparticles;
extern cvar_t gl_subdivide_size;
extern cvar_t gl_vertexarrays;
extern cvar_t gl_vertexlights;
extern cvar_t gl_ztrick;

extern cvar_t gl_contrast;
extern cvar_t gl_gamma;

extern cvar_t gl_anisotropy;
extern cvar_t gl_antialiasing;

extern cvar_t gl_bloom;
extern cvar_t gl_bloomalpha;
extern cvar_t gl_bloomcolor;
extern cvar_t gl_bloomdarken;
extern cvar_t gl_bloomdiamond;
extern cvar_t gl_bloomdiamondsize;
extern cvar_t gl_bloomintensity;
extern cvar_t gl_bloomsamplesize;
extern cvar_t gl_bloomfastsample;

extern cvar_t gl_blur;
extern cvar_t gl_bluralpha;
extern cvar_t gl_blurblood;
extern cvar_t gl_blurbrightness;
extern cvar_t gl_blurscale;

extern cvar_t gl_decalsmax;
extern cvar_t gl_decalstime;
extern cvar_t gl_decals_blood;
extern cvar_t gl_decals_bullets;
extern cvar_t gl_decals_sparks;
extern cvar_t gl_decals_explosions;

extern cvar_t gl_dcontrast;
extern cvar_t gl_dcontrastcolor;
extern cvar_t gl_dcontrastfastsample;
extern cvar_t gl_dcontrastscale;
extern cvar_t gl_dcontrastspeed;

extern cvar_t gl_blendintensity;
extern cvar_t gl_blendradius;

extern cvar_t gl_fog;
extern cvar_t gl_fogbrightness;
extern cvar_t gl_fogcolor;
extern cvar_t gl_fogdensity;
extern cvar_t gl_fogdistance;

extern cvar_t gl_loadlits;
extern cvar_t gl_loadq3models;
extern cvar_t gl_loadtextures;

extern cvar_t gl_overbright;
extern cvar_t gl_overbrightlights;
extern cvar_t gl_overbrightmodels;
extern cvar_t gl_overbrightworld;

extern cvar_t gl_part_explosions;
extern cvar_t gl_part_trails;
extern cvar_t gl_part_spikes;
extern cvar_t gl_part_gunshots;
extern cvar_t gl_part_blood;
extern cvar_t gl_part_telesplash;
extern cvar_t gl_part_blobs;
extern cvar_t gl_part_lavasplash;
extern cvar_t gl_part_flames;
extern cvar_t gl_part_lightning;
extern cvar_t gl_part_damagesplash;
extern cvar_t gl_part_muzzleflash;

extern cvar_t gl_pbo;
extern cvar_t gl_vbo;
extern cvar_t gl_vbomapped;

extern cvar_t gl_smokes;
extern cvar_t gl_smokesalpha;
extern cvar_t gl_smokesbrightness;
extern cvar_t gl_smokescolor;
extern cvar_t gl_smokesentropy;
extern cvar_t gl_smokesradius;
extern cvar_t gl_smokesvelocity;

extern cvar_t gl_smokesdensity;

extern cvar_t gl_texturechecksum;
extern cvar_t gl_texturecompression;
extern cvar_t gl_texturefilter;

extern cvar_t gl_warpcaustics;
extern cvar_t gl_warpwater;
extern cvar_t gl_warpalphaskyspeed;
extern cvar_t gl_warpsolidskyspeed;

extern cvar_t scr_conalpha;
extern cvar_t scr_menualpha;
extern cvar_t scr_sbaralpha;
extern cvar_t scr_textalpha;

extern cvar_t scr_conbrightness;
extern cvar_t scr_sbarbrightness;
extern cvar_t scr_textbrightness;

extern cvar_t scr_confont;

extern cvar_t scr_consmooth;
extern cvar_t scr_crosshairsmooth;
extern cvar_t scr_hudsmooth;
extern cvar_t scr_hudsize;
extern cvar_t scr_menusmooth;
extern cvar_t scr_sbarsmooth;
extern cvar_t scr_textsmooth;
extern cvar_t scr_weaponsmooth;

extern cvar_t vid_vsync;

qboolean OnChange_vid_vsync (cvar_t * var, char *string);

extern int    lightmode;

extern int    gl_lightmap_format;
extern int    gl_solid_format;
extern int    gl_alpha_format;

extern int    mirrortexturenum;        // quake texturenum, not gltexturenum
extern qboolean mirror;
extern mplane_t *mirror_plane;

extern float  r_world_matrix[16];

extern const char *gl_vendor;
extern const char *gl_renderer;
extern const char *gl_version;
extern const char *gl_extensions;

// lxndr: should already be defined in GL/glext.h
// Multitexture
#define      GL_TEXTURE0_ARB                 0x84C0
#define      GL_TEXTURE1_ARB                 0x84C1
#define      GL_TEXTURE2_ARB                 0x84C2
#define      GL_TEXTURE3_ARB                 0x84C3
#define      GL_MAX_TEXTURE_UNITS_ARB        0x84E2

extern int    gl_textureunits;

// ambience.c
void          Ambience_Next_f (void);
void          Ambience_Prev_f (void);

// gl_bloom.c
void          R_BloomClear (void);
void          R_InitBloom (void);
void          R_RenderBloom (void);

// gl_decalss.c
void          R_InitDecals (void);
void          R_ClearDecals (void);
void          R_DrawDecals (void);
void          R_SetupDecals (void);
void          R_SpawnDecal (vec3_t center, vec3_t normal, vec3_t tangent, int tex, int size);
void          R_SpawnDecalStatic (vec3_t org, int tex, int size);
extern int    decal_blood1, decal_blood2, decal_blood3, decal_q3blood, decal_burn, decal_mark, decal_glow;

// gl_draw.c
typedef struct vbo_s {
    int id;
    float *buffer;
    float *p;
    qboolean mapped;
    GLenum target;
} vbo_t;
/*typedef struct count_s {
    int sumverts;
    int primitives;
    int *primverts;
    int *primoffset;
    qboolean *primstrip;
} count_t;*/
void          Draw_AlphaPic (int x, int y, mpic_t * pic, float alpha);
void          Draw_AlphaPicColor (int x, int y, mpic_t * pic, float alpha, float color);
void          Draw_AlphaSubPic (int x, int y, mpic_t * pic, float alpha, int srcx, int srcy, int width, int height);
void          Draw_AlphaSubPicColor (int x, int y, mpic_t * pic, float alpha, float color, int srcx, int srcy, int width, int height);
void          Draw_CharacterConsole (int x, int y, int num);
void          Draw_Screenshot (char *map);
void          Draw_StringConsole (int x, int y, char *str);
void          Draw_TextPic (int x, int y, mpic_t * pic);
inline void   GL_BindVBO (vbo_t *vbo, qboolean mapped);
inline void   GL_FillArray12f (float *a, float v0, float v1, float v2, float v3, float v4, float v5, float v6, float v7, float v8, float v9, float v10, float v11);
inline void   GL_FillArray2f (float **a, float v0, float v1);
inline void   GL_FillArray3f (float **a, float v0, float v1, float v2);
inline void   GL_FillArray4f (float **a, float v0, float v1, float v2, float v3);
inline void   GL_FillArray8f (float *a, float v0, float v1, float v2, float v3, float v4, float v5, float v6, float v7);
inline void   GL_FillArray8i (int *a, int v0, int v1, int v2, int v3, int v4, int v5, int v6, int v7);
inline void   GL_FillArray8iTof (float *a, int v0, int v1, int v2, int v3, int v4, int v5, int v6, int v7);
inline float *GL_GetArrayf (void);
inline int   *GL_GetArrayi (void);
inline void   GL_PolyVertex3f (vbo_t *vertex, count_t *c);
inline void   GL_PolyTexCoord2fVertex3f (vbo_t *texcoord, vbo_t *vertex, count_t *c);
inline void   GL_Poly2TexCoord2fVertex3f (vbo_t *texcoord1, vbo_t *texcoord2, vbo_t *vertex, count_t *c);
inline void   GL_Poly3TexCoord2fVertex3f (vbo_t *texcoord1, vbo_t *texcoord2, vbo_t *texcoord3, vbo_t *vertex, count_t *c);
inline void   GL_Tri4Color4fTexCoord2fVertex3f (vbo_t *color, vbo_t *texcoord, vbo_t *vertex, count_t *c);
inline void   GL_Tri4Color4f2TexCoord2fVertex3f (vbo_t *color, vbo_t *texcoord1, vbo_t *texcoord2, vbo_t *vertex, count_t *c);
inline void   GL_QuadColor3fVertex2fTexcoord2f (float *color, float *vertex, float *texcoord, int pass);
inline void   GL_QuadVertex2fTexcoord2f (float *vertex, float *texcoord, int pass);
extern mpic_t *conback;

// gl_levels.c
void          R_ClearLevels (void);
void          R_InitLevels (void);
void          R_Levels (void);
inline void   R_LevelsBind (int texnum);
void          R_ResetLevels (void);
void          R_SetupLevels (void);
extern qboolean refresh_levels;

// gl_light.c
void          R_AnimateLight (void);
//float         R_GetVertexLightValue (byte ppitch, byte pyaw, float apitch, float ayaw);
void          R_InitVertexLights (void);
float         R_LerpVertexLight (byte ppitch1, byte pyaw1, byte ppitch2, byte pyaw2, float ilerp, float apitch, float ayaw);
int           R_LightPoint (vec3_t p);
void          R_MarkLights (dlight_t * light, int lnum, mnode_t * node);
void          R_RenderDlights (void);
extern vec3_t lightspot, lightcolor;
extern float  vlight_pitch, vlight_yaw;
extern byte   anorm_pitch[NUMVERTEXNORMALS], anorm_yaw[NUMVERTEXNORMALS];

// gl_main.c
void          R_BrightenScreen (void);
void          R_ClearLuminance (float clear);
void          R_CalcLuminance (void);
void          R_CalcRGB (float color, float intensity, float *rgb);
void          R_CalcScreenTextureSize (int *width, int *height);
inline void   R_ClearFogColor (void);
inline void   R_ResetFogColor (void);
inline qboolean R_CullBox (vec3_t mins, vec3_t maxs);
inline qboolean R_CullSphere (vec3_t centre, float radius);
void          R_RotateForEntity (entity_t * ent, qboolean shadow);
void          R_RotateForViewEntity (entity_t * ent);
void          R_SetupLighting (entity_t * ent);
void          R_DynamicContrast (void);
void          R_PolyBlend (void);
void          R_ResetLightmaps (void);

// gl_md*.c
void          R_DrawQ1Model (entity_t * ent);
void          R_DrawQ3Model (entity_t * ent);

// gl_mesh.c
void          GL_MakeQ1ModelDisplayLists (model_t * m, aliashdr_t * hdr);

// gl_misc.c
void          R_InitOtherTextures (void);

// gl_part.c
typedef enum {
    pm_classic, pm_qmb, pm_quake3, pm_mixed
} part_mode_t;
char         *R_NameForParticleMode (void);
void          R_SetParticleMode (part_mode_t val);
void          R_ToggleParticles_f (void);
extern part_mode_t particle_mode;

// gl_refrag.c
void          R_StoreEfrags (efrag_t ** ppefrag);

// gl_sandbox.c
void          R_InitSandbox (void);
void          R_RenderSandbox (void);

// gl_surf.c
void          GL_BuildLightmaps (void);
void          R_DrawBrushModel (entity_t * e);
void          R_DrawWorld (void);

// gl_smokess.c
void          R_InitSmokes (void);
void          R_RenderSmokes (void);

// gl_tex.c
inline void   GL_Bind (int texnum);
inline void   GL_CheckForError (void);
inline void   GL_DisableMultitexture (void);
inline void   GL_EnableMultitexture (void);
inline void   GL_DisableCombine (void);
inline void   GL_EnableCombine (GLfloat scale);
inline void   GL_DisableTMU (GLenum target);
inline void   GL_EnableTMU (GLenum target);
inline void   GL_SelectTexture (GLenum target);
inline gltexture_t *GL_FindTextureByTexnum (int texnum);
const char   *GL_GetFilterString (void);
void          GL_NextFilter (void);
void          GL_PrevFilter (void);
void          GL_ToggleFilter (qboolean linear, int texmode);
inline void   GL_Set2D (int x, int y, int width, int height, int nearval, int farval);
inline void   GL_Set3D (void);
inline unsigned *GL_BindPBO (int size, qboolean block);
inline void   GL_UploadPBO (unsigned int *scaled, int width, int height, int mode);
inline void   GL_UploadSlow (unsigned int *scaled, int width, int height, int mode);
int           GL_Upload32 (unsigned *data, int width, int height, int mode);
int           GL_Upload8 (byte * data, int width, int height, int mode);
inline void   GL_TryUploadFast (unsigned int *scaled, gltexture_t *glt);
int           GL_LoadCharsetImage (char *filename, char *identifier, int texnum, int mode);
byte         *GL_LoadImagePixels (char *filename, char *identifier, int *matchwidth, int *matchheight, int mode);
mpic_t       *GL_LoadPicImage (char *filename, char *id, int matchwidth, int matchheight, int mode);
int           GL_LoadPicTexture (char *name, mpic_t * pic, byte * data, int mode);
int           GL_LoadTexture (char *identifier, int width, int height, byte * data, int mode, int bytesperpixel);
int           GL_LoadTextureWithTexnum (char *identifier, int width, int height, byte * data, int mode, int bpp, int texnum);
int           GL_LoadTextureImage (char *filename, char *identifier, int matchwidth, int matchheight, int mode);
void          R_Textures (void);
void          R_InitTexture (void);
extern gltexture_t gltextures[MAX_GLTEXTURES];
extern int    numgltextures;

// gl_warp.c
void          R_DrawCausticsPolys (void);
void          R_DrawWaterPolys (msurface_t * fa);
void          GL_SubdivideSurface (msurface_t * fa);
void          R_AddSkyBoxSurface (msurface_t * fa);
void          R_ClearSkyBox (void);
void          R_DrawSkyBox (void);
void          R_DrawSkyChain (void);
int           R_SetSky (char *skyname);
extern qboolean r_skyboxloaded;
extern int    solidskytexture, alphaskytexture;

// location.c
void          R_DrawLocations (void);

// vid_common_gl.c
typedef struct extension_s {
    const char *extension;
    qboolean *found;
} extension_t;
struct gl_ext_s {
    qboolean blend_color;
    qboolean blend_minmax;
    qboolean blend_substract;
    qboolean frame_bufferobject;
    qboolean generate_mipmap;
    qboolean multisample;
    qboolean multitex;
    qboolean pixel_bufferobject;
    qboolean tex_compression;
    qboolean tex_envadd;
    qboolean tex_envcombine;
    qboolean tex_filteranisotropic;
    qboolean tex_nonpoweroftwo;
    qboolean swap_control;
    qboolean vertex_bufferobject;
    qboolean video_sync;
} gl_ext;
void          Check_Gamma (unsigned char *pal);
qboolean      CheckExtension (const char *extension);
void          GL_Init (void);
void          GL_Shutdown (void);
void          (APIENTRY *qglActiveTexture) (GLenum target);
void          (APIENTRY *qglBlendEquation) (GLenum mode);
void          (APIENTRY *qglBlendColor) (GLclampf red, GLclampf green, GLclampf blue, GLclampf alpha);
void          (APIENTRY *qglBindBuffer) (GLenum target, GLuint buffer);
void          (APIENTRY *qglBufferData) (GLenum target, GLsizeiptrARB size, const GLvoid *data, GLenum usage);
void          (APIENTRY *qglBufferSubData) (GLenum target, GLintptrARB offset, GLsizeiptrARB size, const GLvoid *data);
void          (APIENTRY *qglClientActiveTexture) (GLenum target);
void          (APIENTRY *qglDeleteBuffers) (GLsizei n, const GLuint *buffers);
void          (APIENTRY *qglGenBuffers) (GLsizei n, GLuint *buffers);
void          (APIENTRY *qglGetBufferParameteriv) (GLenum target, GLenum value, GLint *data);
void          (APIENTRY *qglGenerateMipmap) (GLenum target);
void          *(APIENTRY *qglMapBuffer) (GLenum target, GLenum access);
void          (APIENTRY *qglMultiDrawArrays) (GLenum mode, GLint *first, GLsizei *count, GLsizei primcount);
void          (APIENTRY *qglMultiTexCoord2f) (GLenum target, GLfloat s, GLfloat t);
qboolean      (APIENTRY *qglUnmapBuffer) (GLenum target);
void          *qglGetProcAddress (const char *proc);
inline void   VID_ThreadLock (void);
inline void   VID_ThreadUnlock (void);
inline void   VID_ThreadLock2 (void);
inline void   VID_ThreadUnlock2 (void);
void          *VID_ThreadCallback (void (* callback) (void));
void          VID_ThreadStart (void (* callback) (void), int count);
extern qboolean gl_allow_ztrick;
extern qboolean gl_nopolysmooth;
extern qboolean gl_softwarerenderer;
