/*
 * Copyright (C) 1996-1997 Id Software, Inc.
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * 
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 * 
 */

// draw.h -- these are the only functions outside the refresh allowed
// to touch the vid buffer

#ifdef GLQUAKE

typedef enum {
    TEX_ACTION_NONE = 0, TEX_ACTION_RETRIEVE, TEX_ACTION_PROCESS, TEX_ACTION_UPLOAD
} texaction_t;

typedef enum {
    TEX_STATE_NONE = 0, TEX_STATE_LOADED, TEX_STATE_USED
} texstate_t;

typedef enum {
    TEX_TRANS_NONE = 0, TEX_TRANS_RETRIEVING, TEX_TRANS_PROCESSING, TEX_TRANS_UPLOADING
} textrans_t;

typedef struct {
    int           texnum;
    char          identifier[MAX_QPATH];
    char         *filename;
    int           width, height;
    int           scaled_width, scaled_height;
    int           texmode;
    unsigned      crc;
    int           bpp;
    unsigned     *backup;
    unsigned     *process;
    texaction_t   action;
//    texstate_t    state;
    textrans_t    transition;
    qboolean      loaded;
    qboolean      used;
    qboolean      locked;
    int           allocated;
    int           count;
} gltexture_t;

typedef struct {
    int           width, height;
    gltexture_t  *glt;
    float         sl, tl, sh, th;
} mpic_t;

#else
typedef struct {
    int           width;
    short         height;
    byte          alpha;
    byte          pad;
    byte          data[4];             // variable sized
} mpic_t;
#endif

extern mpic_t *draw_disc;              // also used on sbar

void          Draw_Init (void);
void          Draw_Character (int x, int y, int num);
void          Draw_DebugChar (char num);
void          Draw_SubPic (int x, int y, mpic_t * pic, int srcx, int srcy, int width, int height);
void          Draw_Pic (int x, int y, mpic_t * pic);
void          Draw_TransPic (int x, int y, mpic_t * pic);
void          Draw_TransPicTranslate (int x, int y, mpic_t * pic, byte * translation);
void          Draw_ConsoleBackground (int lines, float xscale);
void          Draw_BeginDisc (void);
void          Draw_EndDisc (void);
void          Draw_TileClear (int x, int y, int w, int h);
void          Draw_Fill (int x, int y, int w, int h, int c);
void          Draw_FadeScreen (void);
void          Draw_String (int x, int y, char *str);
void          Draw_Alt_String (int x, int y, char *str);
mpic_t       *Draw_PicFromWad (char *name);
mpic_t       *Draw_CachePic (char *path);
void          Draw_Crosshair (void);
void          Draw_TextBox (int x, int y, int width, int lines);
