//
// location.h
//
// JPG
// 
// This entire file is new in proquake.  It is used to translate map areas
// to names for the %l formatting specifier
//

typedef struct location_s {
    struct location_s *next_loc;
    vec3_t mins, maxs;		
    char name[32];
    vec_t sum;
} location_t;

void Location_Init (void);

// Load the locations for the current level from the location file
void Loc_LoadLocations (void);

// Get the name of the location of a point
char *Loc_GetLocation (vec3_t p);

void Loc_Delete_f (void);
void Loc_Clear_f (void);
void Loc_StartPoint_f (void);
void Loc_EndPoint_f (void);
void Loc_Load_f (void);
void Loc_Save_f (void);
