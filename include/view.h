/*
 * Copyright (C) 1996-1997 Id Software, Inc.
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * 
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 * 
 */
// view.h
extern cvar_t crosshair;
extern cvar_t v_contrast;
extern cvar_t v_fovcorrection;
extern cvar_t v_gamma;
extern cvar_t v_gunbob;
extern cvar_t v_gunyoffset;
extern cvar_t v_idlescale;
extern cvar_t v_noisescale;

extern cvar_t show_stats;
extern cvar_t show_clock;
extern cvar_t show_fps;

#define FPS_HIST_SIZE 100
extern float  v_fpshist[FPS_HIST_SIZE];
extern float  v_fpsmean;

extern float  healthscale;
extern float  zoom;

#define FovScale() v_fovcorrection.value ? 1.0 - (float) (scr_fov.value - 90) / 90.0 * v_fovcorrection.value : 1
#ifdef GLQUAKE
#define LUM_HIST_SIZE 100
extern float  v_lumhist[LUM_HIST_SIZE];
extern float  v_lummean;

extern float  v_blend[4];

void          V_AddLightBlend (float r, float g, float b, float a2);
#endif

extern byte   gammatable[256];         // palette is sent through this
extern byte   current_pal[768];

#ifndef GLQUAKE
extern cvar_t lcd_x, lcd_yaw;
#endif

void          V_Init (void);
void          V_RenderView (void);

void          V_CalcBlend (void);
void          V_CalcPowerupCshift (void);
void          V_CalcZoom (void);
float         V_CalcRoll (vec3_t angles, vec3_t velocity);
void          V_UpdatePalette (void);
