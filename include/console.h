/*
 * Copyright (C) 1996-1997 Id Software, Inc.
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * 
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 * 
 */

// console
extern int    conwidth, conheight;

#define CON_TEXTSIZE    65536*4

#define CHAT_PROMPT	'@'
#define CONSOLE_PROMPT	']'
#define SHELL_PROMPT	'>'

#define LINESEP "\35\36\36\36\36\36\36\36\36\36\36\36\36\36\36\36\36\36\36\36\36\36\36\36\36\36\36\36\36\36\36\36\36\36\37\n"

extern int    con_totallines;
extern int    con_numlines;
extern int    con_backscroll;
extern qboolean con_forcedup;          // because no entities to refresh
extern qboolean con_initialized;
extern qboolean con_message;
extern byte  *con_chars;
extern int    con_clearnotifylines;    // scan lines to clear for notify lines

void          Con_DrawCharacter (int cx, int line, int num);

void          Con_CheckResize (void);
void          Con_Init (void);
void          Con_Shutdown (void);
void          Con_DrawConsole (int lines, qboolean drawinput);
void          Con_Printf (char *fmt, ...);
void          Con_DPrintf (char *fmt, ...);
void          Con_FPrintf (char *fmt, ...);
void          Con_VPrintf (char *fmt, ...);
void          Con_VPrintf (char *fmt, ...);
void          Con_Warnf (char *fmt, ...);
void          Con_DWarnf (char *fmt, ...);
void          Con_Errorf (char *fmt, ...);
void          Con_Clear (void);
void          Con_DrawNotify (void);
void          Con_ClearNotify (void);
void          Con_ToggleConsole_f (void);

void          Con_NotifyBox (char *text);       // during startup for sound / cd warnings

int           debugcount;
#define       here Con_Printf("\x02%s\n", __FUNCTION__);
#ifdef DEBUG
#define       Con_Debug(level, string) level <= developer.value ? Con_Printf ("%s: %s", __FUNCTION__, string) : 0
#define       Con_Debugf(level, format, args...) level <= developer.value ? Con_Printf ("%s: " format, __FUNCTION__, args) : 0
#define       incstart() (debugcount = 0)
#define       inc() Con_Printf("|%d", debugcount++)
#else
#define       Con_Debug(level, string)
#define       Con_Debugf(level, format, args...)
#endif
