struct ambience_s {
    char name[16];
    void (*apply) (void);
    qboolean select;
    struct ambience_s *next;
    struct ambience_s *prev;
};

typedef struct ambience_s ambience_t;

extern cvar_t ambience;
extern cvar_t ambience_auto;
extern cvar_t ambience_hd;
extern cvar_t ambience_switchtime;
extern cvar_t ambience_switchsteps;

void Ambiance_Next_f (void);
void Ambiance_Prev_f (void);
void Ambience_Reset_f (void);

ambience_t *Ambience_Add (char *name, void (* apply) (void), int select);
qboolean Ambience_Delete (char *name, const char **ret);
ambience_t *Ambience_Get (char *name);
qboolean Ambience_IsInternal (void);
qboolean Ambience_Save (char *name, const char **ret);
void Ambience_Set (ambience_t *a);
void Ambience_Write (FILE * f);

void Ambience_Init (void);
