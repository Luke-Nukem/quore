/*
 * Copyright (C) 1996-1997 Id Software, Inc.
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * 
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 * 
 */
// screen.h

#define STAT_MINUS      10             // num frame for '-' stats digit

void          Show_Init (void);
void          SCR_Init (void);

void          SCR_UpdateScreen (void);
void          SCR_UpdateWholeScreen (void);

int           SCR_itoa (int num, char *buf);
int           SCR_ColorForMap (int m);
void          SCR_SortFrags (void);

void          SCR_DrawDeathmatchOverlay (void);
void          SCR_DrawMiniDeathmatchOverlay (void);
void          SCR_DrawFinaleOverlay (void);
void          SCR_DrawIntermissionOverlay (void);

void          SCR_DrawLoading (void);
void          SCR_DrawNet (void);
void          SCR_DrawPause (void);
void          SCR_DrawRam (void);
void          SCR_DrawTurtle (void);

void          SCR_DrawClock (void);
void          SCR_DrawDebug (void);
void          SCR_DrawFPS (void);
void          SCR_DrawLocation (void);
void          SCR_DrawSpeed (void);
void          SCR_DrawStats (void);
void          SCR_DrawVolume (void);
void          SCR_DrawPlaybackStats (void);

void          SCR_CenterPrint (char *str);

void          ApplyGamma (byte * buffer, int size);

void          SCR_BeginLoadingPlaque (void);
void          SCR_EndLoadingPlaque (void);

void          SCR_ScreenShotAuto (void);
qboolean      SCR_FindScreenShot (char *name);

extern float  scr_con_current;
extern float  scr_conlines;            // lines of console to display

extern int    scr_fullupdate;          // set to 0 to force full redraw
extern int    sb_lines;

extern int    clearnotify;             // set to 0 whenever notify text is drawn
extern qboolean scr_disabled_for_loading;
extern qboolean scr_skipupdate;

extern cvar_t scr_consize;
extern cvar_t scr_conswitch;
extern cvar_t scr_fov;
extern cvar_t scr_sbarinvpos;
extern cvar_t scr_sbarpos;
extern cvar_t scr_shotauto;
extern cvar_t scr_viewsize;

extern int    hudwidth, hudheight;

// only the refresh window will be updated unless these variables are flagged
extern int    scr_copytop;
extern int    scr_copyeverything;

extern qboolean block_drawing;
