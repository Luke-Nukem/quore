# QUORE
A mirror of the Quore source. Last udated by lxndr on 2013-05-22

### Website
http://quore.free.fr/
    
### About
Quore is an atmospheric Quake engine running on GNU/Linux systems with enhanced graphics, increased limits, configurable HUD and ambiences, and different modes for changing the gameplay. It is based on JoeQuake with additional effects from Qrack, ezQuake and engine's limits tweaking from Fitzquake.

My main goal in this project was to enhance what was my favorite game some years ago, making it looking more like a modern game while staying close to the original spirit. As I am quite happy with the current version, it will most likely not be updated very often.

The name 'Quore' comes from some keywords: Quake, Gore, Ogre, Core. It means 'heart' in italian, I pronounce it like 'Core', not 'Cure' which was my favorite band when I was younger, as you always wanted to know :-)

Life is short.
lxndr
