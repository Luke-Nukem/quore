2009-11-22     quore-0.3.1
	- added r_levelsblood effect, cvar and menu option
	- reduced time needed to process r_levels
	- changed r_levels and texture memory management
	- added world and models shadow tuning, cvars and menu options
	- use stencil buffer for shadows
	- changed decals alpha
	- reduced decals memory footprint
	- added flame transparency
	- added sv_rationalspeed cvar
	- added heratic noise movements following health value
	- fixed flickering status bar
	- fixed inventory alpha
	- arranged texture filtering code
	- made texture checksum optional
	- added texture compression support
	- added scr_*smooth cvars and menu option
	- added pbo and mipmap OpenGL extensions
	- use vertex arrays for the bloom effect
	- added blur when zooming
	- improved weapon view
	- added mouse mouvement inertia
	- fixed player speed when non-DGA
	- added v_ratiocorrection cvar for widescreens
	- added slider for selecting gun position
	- added more menus for games and maps selection
	- removed network type selection from the multiplayer menu navigation
	- automaticaly add entry to the server list when joining a server
	- fixed demo menu
	- added player location feature
	- made ALSA support optional defaulting to OSS
	- merged code shared by X11 and GLX
	- fixed keyboard autorepeat
	- added 64 bit support
 	- added multithreading support
	- added vsync support
	- increased limits
	- changed build structure
	- added cvarlock, cvarunlock and resetambience commands
	- added CVAR_VOLATILE and CVAR_LOWFP flag
	- renamed several cvars
	- save non-default cvars by default
	- standard output disabled by default
	- new command line arguments to control output verbosity
	- updated ambiences
	- lots of cosmetic changes

2009-06-22	quore-0.3.0
	- limited r_levels processing to hardware renderer
	- only process textures that require it
	- added sky textures within r_levels processing
	- added r_levelslowmemory and r_levelsmax cvars
	- fixed r_levels update when disabling effect
	- fixed opengl state within r_renderblur
	- fixed framerate compensation on blur effect
	- removed gl_hwblend code
	- cleaned up color shift code
	- made damage blending last longer following the skill value
	- made monsters act faster following the skill value
	- show health board instead of score board when player dies
	- added gl_bloomdiamond cvar for selecting the bluring method
	- added gl_bloomcolor cvar
	- added ambienceauto cvar
	- added and updated ambiences
	- added ambiencelist command
	- added health impact on idle behaviour
	- added zfighting fix from ezquake
	- added ambient light to the vertex lighting
	- gamma and contrast ranges less restrictive
	- added dynamic contrast effect, cvars and menu
	- partially fixed surface flags for brush models when underwater
	- made hud elements scalable and transparent
	- changed scr_menuscale and crosshairsize cvars for scr_hudsize
	- added hud menu
	- log centerprint messages in console
	- prevented console from using external charset
	- prevented console from displaying content when menu is active
	- fixed the way conwidth and conheight arguments are used
	- cleaned up console switch code
	- draw full screen console or loading plaque before loading map
	- rewrote main option menu
	- disabled scrap allocation
	- partially fixed screen clearing in X11
	- removed weapon from texture filtering
	- changed weapon movement formulas
	- added zoom effect
	- added keycodes for controlling audio volume from a multimedia keyboard
	- fixed mouse pointer position when only -nomdga option is given
	- increased limits thanks to fitzquake
	- use 64MB hunk memory and fullscreen by default
	- use dynamic memory for decals
        - made some particles optional within QMB
	- added mode command and menu option
	- start with the main menu in quore mode

2009-02-22	quore-0.2.9
	- prevented screensaver from reseting gamma during game
	- changed model hints, added new ones and new texture flags
	- added some cvars to control more accurately how r_levels apply
	- fixed the way r_levels is called to remove useless processing
	- removed processing of fullbright textures within r_levels
	- prevented console from showing up when map start && scr_conswitch 
	- various bug fixes and clean up

2009-02-16	quore-0.2.8
	- ensure console size is minimum 50 char wide when scr_conswitch is on
	- grab keyboard while in windowed_mouse mode with X11
	- fixed fullscreen console bug when scr_conswitch was enabled with X11
	- added -current to the X11 command line options
	- added cvar gl_smokedensity
	- gl_smokeradius impacts lavasmoke as well
	- added smoke effect submenu
	- skill value increases health impact on player speed
	- removed some resolution limitations
	- keep aspect of the menu constant over different screen ratios
	- compensate gun position over different screen ratios
	- added slider to select menu size
	- added resetcvars and resetbinds commands
	- various bug fixes, improvements and clean up

2009-02-10	quore-0.2.7
	- added health impact on player speed
	- added cvar sv_healthimpact
	- added configure script to ease install
	- changed demo default location
	- added experimental mp3 support from Robert Heffernan

2009-02-06	quore-0.2.6
	- replaced enginebinds cvar by -legacy command line parameter
	- choice between engine's default settings and call to the quake.rc file
	- tristate sbar position
	- increased number of files limit in pak files
	- load external files dynamically
	- many atomics and not atomics changes
	- new predefined styles for HD textures selectable with ambiencehd cvar
	- added -quore command line to enable HD engine

2009-02-04	quore-0.2.5
	- added blend equation extension
	- fixed pointer bug from the previous release within q3 vertex lighting
	- splitted CL_RelinkEntities into several functions

2009-02-01	quore-0.2.4
	- removed quake.rc call
	- added cvar enginebinds to set engine's default bindings
	- scr_scalemenu now really scales up to full screen size
	- scr_conswitch applies only if width > 640
	- fixed a bloom rendering issue when DrawViewModel was turned off
	- added ambiencenext and ambienceprev commands
	- reduced processing of levels to a 0-127 range

2009-01-28	quore-0.2.3
	- renamed cl_movespeedkey to cl_movespeedscale
	- renamed cl_anglespeedkey to cl_anglespeedscale
	- applied cl_movespeedscale to switch between walking and running speed
	- removed some image size limitations
	- fixed a console bug linked with scr_conswitch modification
	- fixed a rendering conflict between fog and other effects
	- reduced EF_DIMLIGHT light radius
	- added cvars gl_dlightintensity and gl_dlightradius
	- added cvars r_dynamicintensity and r_dynamicradius
	- joined EF_BRIGHTLIGHT and EF_MUZZLEFLASH dynamic lighting effect
	- added EF_BRIGHTFIELD case to the dynamic lighting list
	- developer value must be increased for more verbose output
	- added command menu_singlemaps
	- set sv_aim to 1 by default
	- fixed cvar aggregate preventing from changing individual cvars
	- added modified greyscale effect from qrack
	- added r_levels* cvars
	- added cvar flag CVAR_AFTER_CHANGE
	- apply r_levels* modifications dynamically
	- removed some warnings
	- video menu totally rewritten
	- predefined styles moved to a separete file, cvars renamed
	- cleaned up several cvars, r_wateralpha moved to r_alphawater

2009-01-10	quore-0.2.2
	- added cvar v_style
	- replaced predefined styles behaviour in the video menu
	- added new prefined styles
	- made DGA extension optional
	- reduced speed when both moving and strafing in air or liquid
	- reduced speed in liquids
	- changed command history location
	- replaced texture quality option by anisotropic filter
	- added modified blur effect from ezquake
	- added cvars gl_blur_alpha, gl_blur_health, gl_blur_scale
	- compensate framerate variation on motion blur rendering
	- enforce framerate limit
	- screenshots now in HOME gamedir
	- added cvar scr_conswitch to choose console position
	- added several cvars to the r_quality aggregate
	- filter dead bodies only if cl_deadbodyfilter > 0
	- fixed gun position and removed gunposition cvar
	- running speed is walking speed x 2

2008-12-29	quore-0.2.1
	- added CVAR_AGGREGATE flag
	- added cvar r_quality
	- added rendering quality option in the video menu

2008-12-20	quore-0.2
	- added linear texture filter
	- added fog menu
	- changed gun behaviour while idle
	- added ${HOME} in the gamedir searchpath
	- removed check on empty param when doing map completion
	- added anisotropic filter from qrack
	- removed demo and map menus from the main menu
	- gamma and contrast ranges less restrictive
	- added cvar v_gunposition
	- added bloom effect from ezquake
	- added smoke effect from qrack
	- removed signedness warnings
	- keep constant light in liquids

2008-12-12	quore-0.1
	- removed some warnings
	- added quoth mod support
	- dropped w32 support
	- changed water fog for global fog
	- changed code style

2008-12-11	quore-0.0
	- project renaming
	- new build structure
	- changed OSS for ALSA
